﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StackExchange.Redis;

namespace ResolverRisk.TreeServer
{
    public struct MembershipSet2
    {
        public MembershipSet2(SqlHierarchyId hid, Dictionary<int, ulong> memberships)
        {
            this.Hid = hid;
            this.Memberships = memberships;
        }

        public SqlHierarchyId Hid;
        public Dictionary<int, ulong> Memberships;
    }

    [Serializable]
    public struct Membership
    {
        public Membership(ulong rank, int user)
        {
            this.Rank = rank;
            this.User = user;
            this.Hid = null;
        }

        public Membership(string hid, ulong rank, int user)
        {
            this.Rank = rank;
            this.User = user;
            this.Hid = hid;
        }

        public ulong Rank;
        public int User;
        public string Hid;
    }

    public struct MemberKey : IEquatable<MemberKey>
    {
        public readonly int User;
        public readonly SqlHierarchyId Hid;

        public MemberKey(int user, SqlHierarchyId hid)
        {
            this.User = user;
            this.Hid = hid;
        }

        public override bool Equals(object obj)
        {
            if (obj is MemberKey)
            {
                return this.Equals((MemberKey)obj);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return User.GetHashCode() ^ Hid.GetHashCode();
        }

        public bool Equals(MemberKey other)
        {
            return this.User == other.User && (this.Hid == other.Hid).IsTrue;
        }
    }

    public struct MembershipSet
    {
        public MembershipSet(SqlHierarchyId hid, List<Membership> memberships)
        {
            this.Hid = hid;
            this.Memberships = memberships;
        }

        public SqlHierarchyId Hid;
        public List<Membership> Memberships;
    }

    public struct NodeRoles : IComparable, IComparable<NodeRoles>
    {
        public NodeRoles(SqlHierarchyId hid, int sid, ulong mask)
        {
            this.Sid = sid;
            this.Hid = hid;
            this.Mask = mask;
        }

        public int Sid;
        public SqlHierarchyId Hid;
        public ulong Mask;

        public int CompareTo(object obj)
        {
            return this.CompareTo((NodeRoles)obj);
        }

        public int CompareTo(NodeRoles other)
        {
            return this.Hid.CompareTo(other.Hid);
        }
    }

    public struct PermissionRow
    {
        public PermissionRow(int userId, SqlHierarchyId hid, ulong mask)
        {
            this.UserId = userId;
            this.Hid = hid;
            this.Mask = mask;
        }

        public int UserId;
        public SqlHierarchyId Hid;
        public ulong Mask;
    }

    public struct NodePayload
    {
        public NodePayload(string hid, ulong mask)
        {
            this.Hid = hid;
            this.Mask = mask;
        }

        public NodePayload(NodeRoles nr)
        {
            this.Hid = nr.Hid.ToString();
            this.Mask = nr.Mask;
        }

        public string Hid;
        public ulong Mask;
    }

    public enum PermissionRequestType
    {
        Self,
        Child,
        Descendant
    }

    public class NodePermission
    {
        public NodePermission(string hid, ulong mask, bool descendantExists)
        {
            this.Hid = hid;
            this.Mask = mask;
            this.DescendantExists = descendantExists;
        }

        public string Hid;
        public ulong Mask;
        public bool DescendantExists;
    }

    public struct UserKey
    {
        public readonly int User;
        public readonly ulong Rank;

        public UserKey(int user, ulong rank)
        {
            this.User = user;
            this.Rank = rank;
        }
    }

    /// <summary>
    /// TODO
    /// TAKEN OUT GLOBAL PERM
    /// 
    /// BinarySearch for nodeRoles IMPORTANT: FIND FIRST INSTANCE OF HID USING BIN SEARCH
    /// 
    /// NO MUTABLE STRUCTS
    /// 
    /// Dependent tables
    /// tree structure/roles/rolemembers/rolepermissions/itemsubtypes/itemstate
    ///  might need extra summary for lst iteration of this loop
    /// 
    /// 
    /// 
    /// remember : workflowinstance.status = 0 assumed do not exist / items table state
    /// </summary>
    public static class RedisAggregator2
    {
        private static List<Membership> Memberships = new List<Membership>();
        private static SqlConnection Connection;
        private static ConnectionMultiplexer RedisConnection;
        private static IDatabase Redis;
        private static object Key = new object();

        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> RealMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        public static Dictionary<int, ulong> GlobalMemberships = new Dictionary<int, ulong>();

        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> AncestorMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        public static Dictionary<SqlHierarchyId, List<UserKey>> AncestorMembershipsByHid = new Dictionary<SqlHierarchyId, List<UserKey>>();

        public static Dictionary<string, ulong> Roles = new Dictionary<string, ulong>();
        public static Dictionary<int, Dictionary<int, ulong>> Items = new Dictionary<int, Dictionary<int, ulong>>();

        public static void Init()
        {
            RedisConnection = ConnectionMultiplexer.Connect("localhost,allowAdmin=true,SyncTimeout=5000");
            Redis = RedisConnection.GetDatabase();
        }

        public static void FlushRedis()
        {
            RedisConnection.GetServer("localhost", 6379).FlushDatabase();
        }

        public static void GenerateRoles(SqlConnection conn)
        {
            GlobalMemberships.Clear();
            AncestorMemberships.Clear();
            AncestorMembershipsByHid.Clear();
            RealMemberships.Clear();

            var realMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();

            #region Load Number of users to initialize

            using (SqlCommand cmd = new SqlCommand(String.Format(@"SELECT COUNT(*) FROM tblUserGroupObjects WHERE Status = 1"), conn))
            {
                int count = (int)cmd.ExecuteScalar();

                GlobalMemberships = new Dictionary<int, ulong>(count);
                for (int i = 0; i <= count; i++)
                {
                    GlobalMemberships.Add(i, 0);
                }

                Items = new Dictionary<int, Dictionary<int, ulong>>(count);
                for (int i = 0; i <= count; i++)
                {
                    Items.Add(i, new Dictionary<int, ulong>());
                }
            }

            #endregion

            #region Get memberships

            using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
                    SELECT rm.hid, u.rank AS userid, rr.rank, 2 AS isreal
                    FROM tblRoleMembership AS rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid = '/'
                    UNION ALL
                    SELECT rm.hid, u.rank AS userid, rr.rank, 1 AS isreal
                    FROM tblRoleMembership AS rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid <> '/'
                    UNION ALL
                    SELECT rm.hid, u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
                    FROM tblRoleMembership as rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.IsReal = 0 and rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                    GROUP BY rm.hid, u.rank"), conn))
            {
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var userid = (int)reader.GetInt64(1);
                        ulong rank = (ulong)reader.GetInt64(2);
                        int isReal = reader.GetInt32(3);

                        //Global permissions ('/')
                        if (isReal == 2)
                        {
                            GlobalMemberships[userid] |= rank;
                        }
                        else if (isReal == 1)
                        {
                            if (RealMemberships.ContainsKey(hid))
                            {
                                if (RealMemberships[hid].ContainsKey(userid))
                                {
                                    RealMemberships[hid][userid] |= rank;
                                }
                                else
                                {
                                    RealMemberships[hid].Add(userid, rank);
                                }
                            }
                            else
                            {
                                var member = new Dictionary<int, ulong>();
                                member.Add(userid, rank);

                                RealMemberships.Add(hid, member);
                            }
                        }
                        else
                        {
                            Dictionary<int, ulong> rolesAt = null;
                            if (AncestorMemberships.TryGetValue(hid, out rolesAt))
                            {
                                rolesAt.Add(userid, rank);
                            }
                            else
                            {
                                rolesAt = new Dictionary<int, ulong>();
                                rolesAt.Add(userid, rank);
                                AncestorMemberships.Add(hid, rolesAt);
                            }

                            List<UserKey> usersAt = null;
                            if (AncestorMembershipsByHid.TryGetValue(hid, out usersAt))
                            {
                                usersAt.Add(new UserKey(userid, rank));
                            }
                            else
                            {
                                AncestorMembershipsByHid.Add(hid, new List<UserKey>() { new UserKey(userid, rank) });
                            }

                        }
                    }
                }
            }

            #endregion

            #region Load roles

            using (SqlCommand cmd = new SqlCommand(String.Format(@"
                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
                    SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
                    FROM (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr
	                INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
                    ORDER BY rr.rank"), conn))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var rank = (ulong)reader.GetInt64(0);
                        var itemtype = reader.GetInt32(1);
                        var state = reader.GetGuid(2);

                        var key = itemtype.ToString() + state.ToString();

                        if (Roles.ContainsKey(key))
                        {
                            Roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
                        }
                        else
                        {
                            Roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
                        }
                    }
                }
            }

            #endregion

        }

        public static IEnumerable<NodeRoles> GenerateTree(SqlConnection conn)
        {
            SqlHierarchyId currentHid = SqlHierarchyId.Null;
            bool firstRun = true;
            Guid currentState = Guid.Empty;
            int currentSid = 0;

            using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT li.hid, wi.state, ist.ItemTypeId, i.itemsid
                        FROM tblLinkedItems4_5 AS li 
                              INNER JOIN tblItems AS i ON li.SubItemID = i.ItemId
			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
                        WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                        ORDER BY li.hid"), conn))
            {
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    var typeList = new List<int>();
                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                        var type = reader.GetInt32(2);
                        var sid = reader.GetInt32(3);

                        if (firstRun)
                        {
                            currentHid = hid;
                            currentSid = sid;
                            firstRun = false;
                        }

                        //Summarize hid
                        if (currentHid != hid)
                        {
                            //summarize currentHid
                            yield return Summarize(currentHid, currentSid, currentState, typeList);

                            currentHid = hid;
                            currentSid = sid;
                            currentState = state;
                            typeList.Clear();
                            typeList.Add(type);
                        }
                        else
                        {
                            currentState = state;
                            typeList.Add(type);
                        }
                    }

                    yield return Summarize(currentHid, currentSid, currentState, typeList);
                }
            }
        }

        public static void Aggregate(SqlConnection conn)
        {
            var NodeMemberships = new Stack<MembershipSet2>();

            //Initialize empty root memberships
            NodeMemberships.Push(new MembershipSet2(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

            //Traverse the tree depth first
            foreach (var node in GenerateTree(conn))
            {
                //Pop until we hit the parent of current node
                while (NodeMemberships.Peek().Hid != node.Hid.GetAncestor(1)) NodeMemberships.Pop();

                //Get parent permissions
                var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

                //Combine current node applicable permission with parent applicable permissions for each user
                Combine(node.Hid, nodeMemberships);

                //Cache for descendants
                NodeMemberships.Push(new MembershipSet2(node.Hid, nodeMemberships));

                //roleMask contains applicable roles at this node, roleMembers the user roles
                Apply(node.Hid, node.Sid, node.Mask, nodeMemberships);
            }

            AncestorMemberships.Clear();
            AncestorMembershipsByHid.Clear();
            RealMemberships.Clear();
        }

        public static void AddToRedis()
        {
            Init();
            FlushRedis();

            foreach (var keyValue in Items)
            {
                foreach (var item in keyValue.Value)
                {
                    byte[] buser = BitConverter.GetBytes(keyValue.Key);
                    byte[] bsid = BitConverter.GetBytes(item.Key);

                    //byte[] bmask = BitConverter.GetBytes(item.Value);
                    byte[] result = new byte[8];

                    buser.CopyTo(result, 0);
                    bsid.CopyTo(result, 4);

                    Redis.HashSetAsync("Items", result, item.Value);
                }
            }
        }

        public static ulong RedisCount(SqlConnection conn)
        {
            ulong max = 0;
            using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT TOP(1000) i.itemid, i.itemsid
                        FROM tblItems AS i"), conn))
            {
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var sid = reader.GetInt32(1);
                        int user = 965;

                        byte[] buser = BitConverter.GetBytes(user);
                        byte[] bsid = BitConverter.GetBytes(sid);

                        byte[] result = new byte[8];
                        buser.CopyTo(result, 0);
                        bsid.CopyTo(result, 4);

                        ulong mask = (ulong)Redis.HashGet("Items", result);
                        max |= mask;
                    }
                }
            }
            return max;
        }

        private static NodeRoles Summarize(SqlHierarchyId hid, int sid, Guid state, List<int> types)
        {
            ulong nodeRoles = 0;
            foreach (var type in types)
            {
                var key = type.ToString() + state.ToString();
                ulong rolesForType = 0;

                if (Roles.TryGetValue(key, out rolesForType))
                {
                    nodeRoles |= rolesForType;
                }
            }

            return new NodeRoles(hid, sid, nodeRoles);
        }

        private static void Combine(SqlHierarchyId hid, Dictionary<int, ulong> parentMemberships)
        {
            Dictionary<int, ulong> members = null;
            if (RealMemberships.TryGetValue(hid, out members))
            {
                foreach (var member in members)
                {
                    if (parentMemberships.ContainsKey(member.Key))
                    {
                        parentMemberships[member.Key] |= member.Value;
                    }
                    else
                    {
                        parentMemberships.Add(member.Key, member.Value);
                    }
                }
            }
        }

        private static void Apply(SqlHierarchyId hid, int itemSid, ulong roleMask, Dictionary<int, ulong> parentMemberships)
        {
            bool anyRealMembers = false;

            Dictionary<int, ulong> ancestorRoles = null;
            bool hasAncestors = AncestorMemberships.TryGetValue(hid, out ancestorRoles);

            foreach (var keyValue in parentMemberships)
            {
                anyRealMembers = true;

                //Combine with any ancestor permissions
                ulong ancestorPermission = 0;
                if (hasAncestors) ancestorRoles.TryGetValue(keyValue.Key, out ancestorPermission);

                ulong appliedRoles = (keyValue.Value | ancestorPermission) & roleMask;

                if (appliedRoles > 0)
                {
                    if (Items[keyValue.Key].ContainsKey(itemSid))
                    {
                        Items[keyValue.Key][itemSid] |= appliedRoles;
                    }
                    else Items[keyValue.Key].Add(itemSid, appliedRoles);
                }
            }

            //Add any ancestor permissions
            if (!anyRealMembers)
            {
                List<UserKey> userKeys = null;

                if (AncestorMembershipsByHid.TryGetValue(hid, out userKeys))
                {
                    foreach (var keyValue in userKeys)
                    {
                        ulong appliedRoles = keyValue.Rank & roleMask;
                        if (appliedRoles > 0)
                        {
                            if (Items[keyValue.User].ContainsKey(itemSid)) Items[keyValue.User][itemSid] |= appliedRoles;
                            else Items[keyValue.User].Add(itemSid, appliedRoles);
                        }
                    }
                }
            }
        }
    }
}
