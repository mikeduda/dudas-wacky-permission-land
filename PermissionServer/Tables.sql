﻿/****** Object:  Table [dbo].[tblAppliedPermissionsStatus]    Script Date: 2/26/2015 11:14:06 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsStatus]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsStatus]    Script Date: 2/26/2015 11:14:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsStatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[stateid] [int] NOT NULL,
	[state]  AS (case when [stateid]=(0) then 'idle' else 'generating' end),
	[lastran] [datetime] NOT NULL,
	[rmdate] [datetime] NOT NULL,
	[rpdate] [datetime] NOT NULL,
	[lidate] [datetime] NOT NULL,
	[istdate] [datetime] NOT NULL,
	[widate] [datetime] NOT NULL,
	[rmcount] [int] NOT NULL,
	[rpcount] [int] NOT NULL,
	[licount] [int] NOT NULL,
	[istcount] [int] NOT NULL,
	[wicount] [int] NOT NULL,
 CONSTRAINT [PK_AppliedPermissionsStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsStaging]    Script Date: 2/26/2015 11:14:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsStaging]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsStaging]
GO


/****** Object:  Table [dbo].[tblAppliedPermissionsStaging]    Script Date: 2/26/2015 11:14:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsStaging](
	[userid] [int] NOT NULL,
	[treeid] [int] NOT NULL,
	[hid] [hierarchyid] NOT NULL,
	[rank] [bigint] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[tblAppliedPermissionsHierarchical]    Script Date: 2/26/2015 11:13:58 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsHierarchical]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsHierarchical]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsHierarchical]    Script Date: 2/26/2015 11:13:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsHierarchical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[treeid] [int] NULL,
	[hid] [hierarchyid] NULL,
	[rank] [bigint] NULL,
 CONSTRAINT [PK_tblAppliedHierarchicalPermissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsHierarchical]') AND name = N'IX_Hierarchical')
BEGIN
	DROP INDEX [IX_Hierarchical] ON [dbo].[tblAppliedPermissionsHierarchical]
END
GO

/****** Object:  Index [IX_Hierarchical]    Script Date: 3/3/2015 1:50:27 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Hierarchical] ON [dbo].[tblAppliedPermissionsHierarchical]
(
	[userid] ASC,
	[treeid] ASC,
	[hid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsGlobal]    Script Date: 2/26/2015 11:13:53 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsGlobal]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsGlobal]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsGlobal]    Script Date: 2/26/2015 11:13:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsGlobal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [uniqueidentifier] NOT NULL,
	[treeid] [int] NOT NULL,
	[itemtypeid] [int] NOT NULL,
	[stateid] [uniqueidentifier] NOT NULL,
	[rank] [bigint] NOT NULL,
 CONSTRAINT [PK_AppliedPermissionsGlobal] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsGlobal]') AND name = N'IX_User')
BEGIN
	DROP INDEX [IX_User] ON [dbo].[tblAppliedPermissionsGlobal]
END
GO

/****** Object:  Index [IX_Hierarchical]    Script Date: 3/3/2015 1:50:27 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_User] ON [dbo].[tblAppliedPermissionsGlobal]
(
	[userid] ASC,
	[treeid] ASC,
	[itemtypeid] ASC,
	[stateid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[tblAppliedPermissionsDurations]    Script Date: 2/26/2015 11:13:48 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsDurations]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsDurations]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsDurations]    Script Date: 2/26/2015 11:13:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsDurations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[duration] [nvarchar](250) NULL,
	[lastran] [datetime] NOT NULL
 CONSTRAINT [PK_AppliedPermissionsDurations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF NOT EXISTS(SELECT 1 FROM [tblAppliedPermissionsStatus])
BEGIN
	INSERT INTO [tblAppliedPermissionsStatus]
		SELECT 0, GETDATE(), GETDATE(), GETDATE(), GETDATE(), GETDATE(), GETDATE(), 0, 0, 0, 0, 0
END

/****** Object:  Table [dbo].[tblAppliedPermissionsRoleMaps]    Script Date: 3/13/2015 3:39:15 PM ******/
DROP TABLE [dbo].[tblAppliedPermissionsRoleMaps]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsRoleMaps]    Script Date: 3/13/2015 3:39:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsRoleMaps](
	[Id] [int] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
 CONSTRAINT [PK_tblAppliedPermissionsRoleMaps] PRIMARY KEY NONCLUSTERED 
(
	[IsCurrent] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[tblAppliedPermissionsUserMaps]    Script Date: 3/13/2015 3:39:19 PM ******/
DROP TABLE [dbo].[tblAppliedPermissionsUserMaps]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsUserMaps]    Script Date: 3/13/2015 3:39:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsUserMaps](
	[Id] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
 CONSTRAINT [PK_tblAppliedPermissionsUserMaps] PRIMARY KEY NONCLUSTERED 
(
	[IsCurrent] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
