﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTreeViewGetNodesWithPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTreeViewGetNodesWithPermission]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spTreeViewGetNodesWithPermission] 
	@TREEID						uniqueidentifier,	
	@FROMHID					hierarchyid,
	@USERID						uniqueidentifier
AS 
BEGIN
	IF(@USERID = '00000000-0000-0000-0000-000000000000')
	BEGIN
			--Select starting point node and children nodes
			SELECT li.treeid, li.hid.ToString(), li.SubItemID, li.ParentItemID, i.Name, i.ItemNumber,i.ItemTypeId, i.DefaultIconItemTypeID, i.IsMultiParent, i.LockSetId, i.SingleInstance, 1 AS nodePermission,	
					(SELECT 1 WHERE EXISTS (SELECT 1 FROM tblLinkedItems4_5 AS li2 WHERE li2.treeid = @TreeID AND li2.hid.IsDescendantOf(li.hid) = 1 AND li2.hid <> li.hid)) AS leafPermission
			FROM	tblLinkedItems4_5 AS li INNER JOIN tblItems AS i ON li.SubItemID = i.ItemID
			WHERE	li.treeid = @TreeID and (li.ParentHID = @FROMHID)
	END
	ELSE
	BEGIN
			DECLARE @TREE INT = (SELECT CASE WHEN @TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' THEN 0 ELSE 1 END)
			DECLARE @USER INT = (SELECT Id FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 1 AND userId = @USERID)
			DECLARE @USERGROUPS IntTable 
			INSERT  @USERGROUPS SELECT [rank] FROM fnGetUserGroupMembership(@USERID) INNER JOIN (SELECT UserId, Id AS rank FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 1) AS u ON ugo = u.UserId
	
			--Select starting point node and children nodes
			SELECT li.treeid, li.hid.ToString(), li.SubItemID, li.ParentItemID, i.Name, i.ItemNumber,i.ItemTypeId, i.DefaultIconItemTypeID, i.IsMultiParent, i.LockSetId, i.SingleInstance,
				(SELECT [dbo].[BinaryOR](p.rank) AS rank FROM (	SELECT agp.rank	
																FROM tblAppliedPermissionsGlobal AS agp 
																INNER JOIN tblItemSubTypes AS ist ON ist.ItemId = li.SubItemID AND agp.itemtypeid = ist.itemtypeid
																LEFT JOIN tblWorkflowInstances AS wi ON wi.itemid = li.SubItemID AND ISNULL(wi.currentstate, '00000000-0000-0000-0000-000000000000') = agp.stateid 
																WHERE agp.userid = @USERID AND agp.treeid = @TREE
																UNION
																SELECT app.rank
																FROM DUDADB5_PERMISSIONS.dbo.tblAppliedPermissionsHierarchical as app 
																		INNER JOIN @USERGROUPS AS ug ON app.userid = ug.value
																WHERE app.treeid = @TREE AND app.hid = li.hid) AS p) AS nodePermission,	
				(SELECT 1 WHERE (EXISTS (SELECT 1 FROM DUDADB5_PERMISSIONS.dbo.tblAppliedPermissionsHierarchical as app INNER JOIN @USERGROUPS AS ug ON app.userid = ug.value
											WHERE app.treeid = @TREE AND app.hid.IsDescendantOf(li.hid) = 1 AND app.hid <> li.hid)
											OR EXISTS (SELECT 1 FROM tblLinkedItems4_5 AS li INNER JOIN tblItemSubTypes AS ist ON ist.itemid = li.SubItemID
															INNER JOIN tblAppliedPermissionsGlobal AS agp ON agp.itemtypeid = ist.itemtypeid
															LEFT JOIN tblWorkflowInstances AS wi ON wi.itemid = li.SubItemID AND ISNULL(wi.currentstate, '00000000-0000-0000-0000-000000000000') = agp.stateid 
														WHERE li.TreeId = @TreeId and li.hid.IsDescendantOf(li.hid) = 1 AND agp.userid = @USERID))) AS leafPermission
			FROM	tblLinkedItems4_5 AS li
						INNER JOIN tblItems AS i ON li.SubItemID = i.ItemID
			WHERE	li.treeid = @TreeID and (li.ParentHID = @FROMHID)
	END
END