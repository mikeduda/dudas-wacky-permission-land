﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsSetGenerationParameters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsSetGenerationParameters]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsSetGenerationParameters
AS
BEGIN
	
	UPDATE tblAppliedPermissionsStatus
	SET stateid = 1,
		rmdate =  (SELECT MAX(updatedon) AS date FROM tblRoleMembership),
		rmcount = (SELECT COUNT(*) FROM tblRoleMembership),
		rpdate =  (SELECT MAX(updatedon) AS date FROM tblRolePermissions),
		rpcount = (SELECT COUNT(*) FROM tblRolePermissions),
		lidate =  (SELECT MAX(updatedon) AS date FROM tblLinkedItems4_5),
		licount = (SELECT COUNT(*) FROM tblLinkedItems4_5),
		istdate = (SELECT MAX(updatedon) AS date FROM tblItemSubTypes),
		istcount = (SELECT COUNT(*) FROM tblItemSubTypes),
		widate =  (SELECT MAX(updatedon) AS date FROM tblWorkflowInstances),
		wicount = (SELECT COUNT(*) FROM tblWorkflowInstances)

END

GO