﻿
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsStaging]    Script Date: 2/26/2015 11:14:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsStaging]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsStaging]
GO


/****** Object:  Table [dbo].[tblAppliedPermissionsStaging]    Script Date: 2/26/2015 11:14:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsStaging](
	[userid] [int] NOT NULL,
	[treeid] [int] NOT NULL,
	[hid] [hierarchyid] NOT NULL,
	[rank] [bigint] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[tblAppliedPermissionsHierarchical]    Script Date: 2/26/2015 11:13:58 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsHierarchical]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsHierarchical]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsHierarchical]    Script Date: 2/26/2015 11:13:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsHierarchical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[treeid] [int] NULL,
	[hid] [hierarchyid] NULL,
	[rank] [bigint] NULL,
 CONSTRAINT [PK_tblAppliedHierarchicalPermissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsHierarchical]') AND name = N'IX_Hierarchical')
BEGIN
	DROP INDEX [IX_Hierarchical] ON [dbo].[tblAppliedPermissionsHierarchical]
END
GO

/****** Object:  Index [IX_Hierarchical]    Script Date: 3/3/2015 1:50:27 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Hierarchical] ON [dbo].[tblAppliedPermissionsHierarchical]
(
	[userid] ASC,
	[treeid] ASC,
	[hid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsClearStaging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsClearStaging]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsClearStaging
AS
BEGIN
	TRUNCATE TABLE [dbo].[tblAppliedPermissionsStaging]
END

GO


/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMergeHierarchicalPermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMergeHierarchicalPermissions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spMergeHierarchicalPermissions
AS
BEGIN

	MERGE [dbo].[tblAppliedPermissionsHierarchical] AS aph
	USING [dbo].[tblAppliedPermissionsStaging] AS aps
		ON aph.userid = aps.userid AND aph.treeid = aps.treeid AND aph.hid = aps.hid

	WHEN MATCHED AND aps.[rank] != aph.[rank] THEN
	  UPDATE SET aph.[rank] = aps.[rank]

	WHEN NOT MATCHED BY TARGET THEN
	  INSERT ([userid],[treeid],[hid],[rank])
	  VALUES (aps.[userid], aps.[treeid], aps.[hid], aps.[rank])

	WHEN NOT MATCHED BY SOURCE THEN
	  DELETE;

	TRUNCATE TABLE [dbo].[tblAppliedPermissionsStaging]

END

GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'bpsr_webuser' AND type = 'R')
	CREATE ROLE [bpsr_webuser] AUTHORIZATION [dbo]
GO

GRANT SELECT ON  [dbo].[tblAppliedPermissionsHierarchical] TO [bpsr_webuser]

GO