﻿//using System;
//using System.Linq;
//using System.Data;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Data.SqlTypes;
//using System.Collections;
//using Microsoft.SqlServer.Server;
//using Microsoft.SqlServer.Types;
//using System.Threading.Tasks;
//using System.Collections.Concurrent;
//using StackExchange.Redis;
//using System.Text;
//using System.IO;
//using System.Runtime.Serialization.Formatters.Binary;

//namespace PermissionAggregator
//{
//    public struct MembershipSet2
//    {
//        public MembershipSet2(SqlHierarchyId hid, Dictionary<int, ulong> memberships)
//        {
//            this.Hid = hid;
//            this.Memberships = memberships;
//        }

//        public SqlHierarchyId Hid;
//        public Dictionary<int, ulong> Memberships;
//    }

//    public struct ApplicableRole
//    {
//        public ApplicableRole(SqlHierarchyId hid, ulong rank, int userId)
//        {
//            this.Rank = rank;
//            this.Hid = hid;
//            this.UserId = userId;
//            this.Treeid = 0;
//        }

//        public ulong Rank;
//        public SqlHierarchyId Hid;
//        public int UserId;
//        public int Treeid;
//        public override string ToString()
//        {
//            StringBuilder b = new StringBuilder();
//            b.Append(Hid);
//            b.Append("|");
//            b.Append(UserId);
//            b.Append("|");
//            b.Append(Rank);
//            return b.ToString();
//        }
//    }

//    [Serializable]
//    public struct Membership
//    {
//        public Membership(ulong rank, int user)
//        {
//            this.Rank = rank;
//            this.User = user;
//            this.Hid = null;
//        }

//        public Membership(string hid, ulong rank, int user)
//        {
//            this.Rank = rank;
//            this.User = user;
//            this.Hid = hid;
//        }

//        public ulong Rank;
//        public int User;
//        public string Hid;
//    }

//    public struct MemberKey : IEquatable<MemberKey>
//    {
//        public readonly int User;
//        public readonly SqlHierarchyId Hid;

//        public MemberKey(int user, SqlHierarchyId hid)
//        {
//            this.User = user;
//            this.Hid = hid;
//        }

//        public override bool Equals(object obj)
//        {
//            if (obj is MemberKey)
//            {
//                return this.Equals((MemberKey)obj);
//            }
//            else
//            {
//                return false;
//            }
//        }

//        public override int GetHashCode()
//        {
//            return User.GetHashCode() ^ Hid.GetHashCode();
//        }

//        public bool Equals(MemberKey other)
//        {
//            return this.User == other.User && (this.Hid == other.Hid).IsTrue;
//        }
//    }

//    public struct MembershipSet
//    {
//        public MembershipSet(SqlHierarchyId hid, List<Membership> memberships)
//        {
//            this.Hid = hid;
//            this.Memberships = memberships;
//        }

//        public SqlHierarchyId Hid;
//        public List<Membership> Memberships;
//    }

//    public struct NodeRoles : IComparable, IComparable<NodeRoles>
//    {
//        public NodeRoles(SqlHierarchyId hid, ulong mask)
//        {
//            this.Hid = hid;
//            this.Mask = mask;
//        }

//        public SqlHierarchyId Hid;
//        public ulong Mask;

//        public int CompareTo(object obj)
//        {
//            return this.CompareTo((NodeRoles)obj);
//        }

//        public int CompareTo(NodeRoles other)
//        {
//            return this.Hid.CompareTo(other.Hid);
//        }
//    }

//    public struct NodeRolesJSON
//    {
//        public NodeRolesJSON(string hid, ulong mask)
//        {
//            this.H = hid;
//            this.M = mask;
//        }

//        public NodeRolesJSON(NodeRoles nr)
//        {
//            this.H = nr.Hid.ToString();
//            this.M = nr.Mask;
//        }

//        public string H;
//        public ulong M;
//    }

//    public struct UserKey
//    {
//        public readonly int User;
//        public readonly ulong Rank;

//        public UserKey(int user, ulong rank)
//        {
//            this.User = user;
//            this.Rank = rank;
//        }
//    }

//    /// <summary>
//    /// TODO
//    /// 4. groups
//    /// 
//    /// BinarySearch for nodeRoles IMPORTANT: FIND FIRST INSTANCE OF HID USING BIN SEARCH
//    /// 
//    /// NO MUTABLE STRUCTS
//    /// 
//    /// Dependent tables
//    /// tree structure/roles/rolemembers/rolepermissions/itemsubtypes/itemstate
//    ///  might need extra summary for lst iteration of this loop
//    /// 
//    /// 
//    /// 
//    /// remember : workflowinstance.status = 0 assumed do not exist / items table state
//    /// </summary>
//    public static class Aggregator
//    {
//        private static List<Membership> Memberships = new List<Membership>();
//        private static SqlConnection Connection;
//        private static object Key = new object();

//        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> RealMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
//        public static Dictionary<int, ulong> GlobalMemberships = new Dictionary<int, ulong>();
//        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> AncestorMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
//        public static Dictionary<SqlHierarchyId, List<UserKey>> AncestorMembershipsByHid = new Dictionary<SqlHierarchyId, List<UserKey>>();

//        public static Dictionary<int, List<NodeRoles>> HierarchicalMemberships = new Dictionary<int, List<NodeRoles>>();

//        public static Dictionary<string, ulong> Roles = new Dictionary<string, ulong>();
//        public static List<NodeRoles> Tree = new List<NodeRoles>(10000);

//        private static void Summarize2(SqlHierarchyId hid, Guid state, List<int> types)
//        {
//            ulong nodeRoles = 0;

//            foreach (var type in types)
//            {
//                var key = type.ToString() + state.ToString();
//                ulong rolesForType = 0;

//                if (Roles.TryGetValue(key, out rolesForType))
//                {
//                    nodeRoles |= rolesForType;
//                }
//            }

//            Tree.Add(new NodeRoles(hid, nodeRoles));
//        }

//        public static IEnumerable<NodeRolesJSON> CheckDescendantPermission(string shid = "/1/1/", int user = 965)
//        {
//            var hid = SqlHierarchyId.Parse(shid);
//            var result = new List<NodeRoles>();

//            var global = GlobalMemberships[user];

//            NodeRoles root = new NodeRoles(hid, 0);

//            int treeCount = HierarchicalMemberships[user].Count;
//            int startIndex = BinarySearchTree(user, hid);

//            StringBuilder b = new StringBuilder();

//            if (startIndex != -1)
//            {
//                for (int i = startIndex; i < treeCount; i++)
//                {
//                    var node = HierarchicalMemberships[user][i];
//                    if (node.Hid.IsDescendantOf(hid))
//                    {
//                        yield return new NodeRolesJSON(node);
//                    }
//                    else break;
//                }
//            }
//        }

//        public static int BinarySearchTree(int user, SqlHierarchyId hid)
//        {
//            int count = HierarchicalMemberships[user].Count;

//            //TODO Implement binary search here
//            for (int i = 0; i < count; i++)
//            {
//                if (HierarchicalMemberships[user][i].Hid.IsDescendantOf(hid))
//                {
//                    return i;
//                }
//            }
//            return -1;
//        }

//        public static void GenerateRoles(string connString)
//        {
//            lock (Key)
//            {
//                GlobalMemberships.Clear();
//                AncestorMemberships.Clear();
//                AncestorMembershipsByHid.Clear();
//                RealMemberships.Clear();

//                var realMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();

//                using (Connection = new SqlConnection(connString))
//                {
//                    Connection.Open();

//                    #region Load Number of users to initialize

//                    using (SqlCommand cmd = new SqlCommand(String.Format(@"SELECT COUNT(*) FROM tblUserGroupObjects WHERE Status = 1"), Connection))
//                    {
//                        int count = (int)cmd.ExecuteScalar();

//                        GlobalMemberships = new Dictionary<int, ulong>(count);
//                        for (int i = 0; i <= count; i++)
//                        {
//                            GlobalMemberships.Add(i, 0);
//                        }

//                        HierarchicalMemberships = new Dictionary<int, List<NodeRoles>>(count);
//                        for (int i = 0; i <= count; i++)
//                        {
//                            HierarchicalMemberships.Add(i, new List<NodeRoles>());
//                        }
//                    }

//                    #endregion


//                    #region Get memberships

//                    using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
//                    SELECT rm.hid, u.rank AS userid, rr.rank, 2 AS isreal
//                    FROM tblRoleMembership AS rm 
//	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
//	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
//                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid = '/'
//                    UNION ALL
//                    SELECT rm.hid, u.rank AS userid, rr.rank, 1 AS isreal
//                    FROM tblRoleMembership AS rm 
//	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
//	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
//                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid <> '/'
//                    UNION ALL
//                    SELECT rm.hid, u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
//                    FROM tblRoleMembership as rm 
//	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
//	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
//                    WHERE rm.IsReal = 0 and rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
//                    GROUP BY rm.hid, u.rank"), Connection))
//                    {
//                        using (SqlDataReader reader = cmd3.ExecuteReader())
//                        {
//                            while (reader.Read())
//                            {
//                                var hid = (SqlHierarchyId)reader.GetValue(0);
//                                var userid = (int)reader.GetInt64(1);
//                                ulong rank = (ulong)reader.GetInt64(2);
//                                int isReal = reader.GetInt32(3);

//                                //Global permissions ('/')
//                                if (isReal == 2)
//                                {
//                                    GlobalMemberships[userid] |= rank;
//                                }
//                                else if (isReal == 1)
//                                {
//                                    if (RealMemberships.ContainsKey(hid))
//                                    {
//                                        if (RealMemberships[hid].ContainsKey(userid))
//                                        {
//                                            RealMemberships[hid][userid] |= rank;
//                                        }
//                                        else
//                                        {
//                                            RealMemberships[hid].Add(userid, rank);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        var member = new Dictionary<int, ulong>();
//                                        member.Add(userid, rank);

//                                        RealMemberships.Add(hid, member);
//                                    }
//                                }
//                                else
//                                {
//                                    Dictionary<int, ulong> rolesAt = null;
//                                    if (AncestorMemberships.TryGetValue(hid, out rolesAt))
//                                    {
//                                        rolesAt.Add(userid, rank);
//                                    }
//                                    else
//                                    {
//                                        rolesAt = new Dictionary<int, ulong>();
//                                        rolesAt.Add(userid, rank);
//                                        AncestorMemberships.Add(hid, rolesAt);
//                                    }

//                                    List<UserKey> usersAt = null;
//                                    if (AncestorMembershipsByHid.TryGetValue(hid, out usersAt))
//                                    {
//                                        usersAt.Add(new UserKey(userid, rank));
//                                    }
//                                    else
//                                    {
//                                        AncestorMembershipsByHid.Add(hid, new List<UserKey>() { new UserKey(userid, rank) });
//                                    }

//                                }
//                            }
//                        }
//                    }

//                    #endregion

//                    #region Load roles

//                    using (SqlCommand cmd = new SqlCommand(String.Format(@"
//                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
//                    SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
//                    FROM (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr
//	                INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
//                    ORDER BY rr.rank"), Connection))
//                    {
//                        using (SqlDataReader reader = cmd.ExecuteReader())
//                        {
//                            while (reader.Read())
//                            {
//                                var rank = (ulong)reader.GetInt64(0);
//                                var itemtype = reader.GetInt32(1);
//                                var state = reader.GetGuid(2);

//                                var key = itemtype.ToString() + state.ToString();

//                                if (Roles.ContainsKey(key))
//                                {
//                                    Roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
//                                }
//                                else
//                                {
//                                    Roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
//                                }
//                            }
//                        }
//                    }

//                    #endregion
//                }
//            }
//        }

//        public static void GenerateTree(string connString)
//        {
//            lock (Key)
//            {
//                #region Load tree

//                Tree.Clear();

//                using (Connection = new SqlConnection(connString))
//                {
//                    Connection.Open();

//                    SqlHierarchyId currentHid = SqlHierarchyId.Null;
//                    bool firstRun = true;
//                    Guid currentState = Guid.Empty;

//                    using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                    	SELECT li.hid, wi.state, ist.ItemTypeId
//                        FROM tblLinkedItems4_5 AS li 
//			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
//			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
//                        WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
//                        ORDER BY li.hid"), Connection))
//                    {
//                        using (SqlDataReader reader = cmd2.ExecuteReader())
//                        {
//                            var typeList = new List<int>();
//                            while (reader.Read())
//                            {
//                                var hid = (SqlHierarchyId)reader.GetValue(0);
//                                var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
//                                var type = reader.GetInt32(2);

//                                if (firstRun)
//                                {
//                                    currentHid = hid;
//                                    firstRun = false;
//                                }

//                                //Summarize hid
//                                if (currentHid != hid)
//                                {
//                                    //summarize currentHid
//                                    Summarize2(currentHid, currentState, typeList);

//                                    currentHid = hid;
//                                    currentState = state;
//                                    typeList.Clear();
//                                    typeList.Add(type);
//                                }
//                                else
//                                {
//                                    currentState = state;
//                                    typeList.Add(type);
//                                }
//                            }

//                            Summarize2(currentHid, currentState, typeList);
//                        }
//                    }
//                }

//                #endregion
//            }
//        }

//        public static void Aggregate2()
//        {
//            SqlHierarchyId hid, parentHid;
//            var NodeMemberships = new Stack<MembershipSet2>();

//            //Initialize empty root memberships
//            NodeMemberships.Push(new MembershipSet2(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

//            //Traverse the tree depth first - might need extra summary for lst iteration of this loop
//            foreach (var node in Tree)
//            {
//                ulong roleMask = node.Mask;
//                hid = node.Hid;
//                parentHid = hid.GetAncestor(1);

//                //Pop until we hit the parent of current node
//                while (NodeMemberships.Peek().Hid != parentHid) NodeMemberships.Pop();

//                //Get parent permissions
//                var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

//                //Combine current node applicable permission with parent applicable permissions for each user
//                Add2(hid, nodeMemberships);

//                //Cache for descendants
//                NodeMemberships.Push(new MembershipSet2(hid, nodeMemberships));

//                //roleMask contains applicable roles at this node, roleMembers the user roles
//                Apply(hid, roleMask, nodeMemberships);
//            }

//            AncestorMemberships.Clear();
//            AncestorMembershipsByHid.Clear();
//            Tree.Clear();
//        }

//        private static void Add2(SqlHierarchyId hid, Dictionary<int, ulong> parentMemberships)
//        {
//            Dictionary<int, ulong> members = null;
//            if (RealMemberships.TryGetValue(hid, out members))
//            {
//                foreach (var member in members)
//                {
//                    if (parentMemberships.ContainsKey(member.Key))
//                    {
//                        parentMemberships[member.Key] |= member.Value;
//                    }
//                    else
//                    {
//                        parentMemberships.Add(member.Key, member.Value);
//                    }
//                }
//            }
//        }

//        private static void Apply(SqlHierarchyId hid, ulong roleMask, Dictionary<int, ulong> parentMemberships)
//        {
//            bool anyRealMembers = false;

//            Dictionary<int, ulong> ancestorRoles = null;
//            bool hasAncestors = AncestorMemberships.TryGetValue(hid, out ancestorRoles);

//            foreach (var keyValue in parentMemberships)
//            {
//                anyRealMembers = true;

//                //Combine with any ancestor permissions
//                ulong ancestorPermission = 0;
//                if (hasAncestors) ancestorRoles.TryGetValue(keyValue.Key, out ancestorPermission);

//                ulong appliedRoles = (keyValue.Value | ancestorPermission) & roleMask;

//                if (appliedRoles > 0)
//                {
//                    HierarchicalMemberships[keyValue.Key].Add(new NodeRoles(hid, appliedRoles));
//                }
//            }

//            //Add any ancestor permissions
//            if (!anyRealMembers)
//            {
//                List<UserKey> userKeys = null;

//                if (AncestorMembershipsByHid.TryGetValue(hid, out userKeys))
//                {
//                    foreach (var keyValue in userKeys)
//                    {
//                        ulong appliedRoles = keyValue.Rank & roleMask;
//                        if (appliedRoles > 0)
//                        {
//                            HierarchicalMemberships[keyValue.User].Add(new NodeRoles(hid, keyValue.Rank));
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
