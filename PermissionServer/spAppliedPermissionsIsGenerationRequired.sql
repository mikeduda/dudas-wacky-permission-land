﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsIsGenerationRequired]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsIsGenerationRequired]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsIsGenerationRequired
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM [dbo].[tblAppliedPermissionsStatus] WHERE stateid = 1) SELECT 0
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblRoleMembership) AS t
							INNER JOIN (SELECT rmdate AS date, rmcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblRolePermissions) AS t
							INNER JOIN (SELECT rpdate AS date, rpcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblLinkedItems4_5) AS t
							INNER JOIN (SELECT lidate AS date, licount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1				
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblItemSubTypes) AS t
							INNER JOIN (SELECT istdate AS date, istcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1				
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblWorkflowInstances) AS t
							INNER JOIN (SELECT widate AS date, wicount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1	
	ELSE SELECT 0

END

GO