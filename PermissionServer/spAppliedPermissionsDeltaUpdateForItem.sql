﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsDeltaUpdateForItem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsDeltaUpdateForItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spAppliedPermissionsDeltaUpdateForItem]
(
	@Items GuidTable READONLY
)
AS
BEGIN
	DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
	DECLARE @LOCATIONTREEID UNIQUEIDENTIFIER = '2E3D1C02-C154-47EE-A391-2C5480C5148A'	
	IF OBJECT_ID('tempdb..#ROLEMAPS') IS NOT NULL BEGIN DROP TABLE #ROLEMAPS END	
	IF OBJECT_ID('tempdb..#LINKS') IS NOT NULL BEGIN DROP TABLE #LINKS END	
	
	CREATE TABLE #ROLEMAPS (ID INT, ITEMTYPE INT, STATE UNIQUEIDENTIFIER)
	INSERT #ROLEMAPS
	SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END AS state
					 FROM (SELECT RoleId, Id AS rank FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 1) AS rr
							INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId

	CREATE TABLE #LINKS(TREEID UNIQUEIDENTIFIER,TREEINT INT, HID HIERARCHYID, RANK INT PRIMARY KEY(TREEID, HID, RANK))
	INSERT #LINKS

	SELECT DISTINCT li.TreeId,CASE WHEN(li.TreeId = @LOCATIONTREEID) THEN 0 ELSE 1 END, li.hid, rm.ID
	FROM tblLinkedItems4_5 AS li
		INNER JOIN @Items AS i ON li.SubItemID = i.value
		INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = li.SubItemId
		LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemID = li.SubItemId AND wi.Status = 1
		INNER JOIN #ROLEMAPS AS rm ON ist.ItemTypeID = rm.ITEMTYPE AND (wi.CurrentState = rm.STATE OR (wi.CurrentState IS NULL AND rm.STATE = @EMPTY)) 

	DELETE FROM DUDADB5.[dbo].[tblAppliedPermissionsHierarchical]
	FROM DUDADB5.[dbo].[tblAppliedPermissionsHierarchical] AS h
				INNER JOIN #LINKS AS l ON l.TREEINT = h.treeid AND l.Hid = h.hid

	INSERT DUDADB5.[dbo].[tblAppliedPermissionsHierarchical]
	SELECT i.UserId, CASE WHEN (i.TreeId = @LOCATIONTREEID) THEN 0 ELSE 1 END, i.hid, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), i.rank))  
	FROM	(SELECT li.TreeId, li.hid, um.Id AS UserId, m.Id AS rank 
			 FROM #LINKS AS li 
					CROSS APPLY fnGetAncestors(li.hid)
					INNER JOIN tblRoleMembership AS rm ON rm.TreeId = li.TreeId AND rm.hid = ancestor AND rm.IsReal = 1 AND rm.Status = 1
					INNER JOIN tblAppliedPermissionsRoleMaps AS m ON m.RoleId = rm.RoleId AND m.IsCurrent = 1  AND m.Id = li.RANK
					INNER JOIN tblAppliedPermissionsUserMaps AS um ON um.UserId = rm.UserGroupId AND um.IsCurrent = 1
			 UNION
			 SELECT li.TreeId, li.hid, um.Id AS UserId, m.Id AS rank
			 FROM #LINKS AS li 
					INNER JOIN tblRoleMembership AS rm ON rm.TreeId = li.TreeId AND rm.hid = li.hid AND rm.IsReal = 0 AND rm.Status = 1
					INNER JOIN tblAppliedPermissionsRoleMaps AS m ON m.RoleId = rm.RoleId AND m.IsCurrent = 1 AND m.Id = li.RANK
					INNER JOIN tblAppliedPermissionsUserMaps AS um ON um.UserId = rm.UserGroupId AND um.IsCurrent = 1) AS i
	GROUP BY i.TreeId, i.hid, i.UserId
END
