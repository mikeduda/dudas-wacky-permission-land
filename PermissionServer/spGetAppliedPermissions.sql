﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAppliedPermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAppliedPermissions]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spGetAppliedPermissions
(
	@UserId UNIQUEIDENTIFIER,
	@TreeId UNIQUEIDENTIFIER,
	@Hid HIERARCHYID
)
AS
BEGIN

	DECLARE @TREE INT = (SELECT CASE WHEN @TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' THEN 0 ELSE 1 END)

	DECLARE @ROOT HIERARCHYID = @Hid
	DECLARE @USER INT = (SELECT Id FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 1 AND userId = @USERID)
	DECLARE @USERGROUPS IntTable 
	INSERT  @USERGROUPS SELECT [rank] FROM fnGetUserGroupMembership(@USERID) INNER JOIN (SELECT UserId, Id AS rank FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 1) AS u ON ugo = u.UserId

	SELECT p.hid, [dbo].[BinaryOR](p.rank) AS rank
	FROM
		(SELECT li.hid, agp.rank
		FROM tblLinkedItems4_5 AS li		
				INNER JOIN tblItemSubTypes AS ist ON ist.itemid = li.SubItemID
				INNER JOIN tblAppliedPermissionsGlobal AS agp ON agp.itemtypeid = ist.itemtypeid
				LEFT JOIN tblWorkflowInstances AS wi ON wi.itemid = li.SubItemID AND ISNULL(wi.currentstate, '00000000-0000-0000-0000-000000000000') = agp.stateid 
		WHERE agp.userid = @USERID AND agp.treeid = @TREE AND li.TreeId = @TreeId AND li.hid.IsDescendantOf(@ROOT) = 1
		UNION
		SELECT app.hid, app.rank
		FROM tblAppliedPermissionsHierarchical as app 
				INNER JOIN @USERGROUPS AS ug ON app.userid = ug.value
		WHERE app.treeid = @TREE AND app.hid.IsDescendantOf(@ROOT) = 1) AS p 
	GROUP BY p.hid
END

GO