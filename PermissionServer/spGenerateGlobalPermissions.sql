﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateGlobalPermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateGlobalPermissions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spGenerateGlobalPermissions
AS
BEGIN

DELETE FROM tblAppliedPermissionsGlobal

INSERT INTO tblAppliedPermissionsGlobal
	SELECT userGroupId, treeid, ItemTypeId, StateId, [dbo].[BinaryOR](rank) AS rank
	FROM	(SELECT ugo.UserGroupId, CASE WHEN(rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, rp.ItemTypeId, rp.StateId, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank
			FROM tblRoleMembership AS rm
						INNER JOIN (SELECT Id AS rank, RoleId FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 0) AS rr ON rm.RoleId = rr.RoleId
						INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupId = rm.UserGroupId
						INNER JOIN (SELECT DISTINCT RoleId, ItemTypeId, ISNULL(StateId, '00000000-0000-0000-0000-000000000000') AS StateId FROM tblRolePermissions) AS rp ON rm.RoleId = rp.RoleId
			WHERE rm.IsReal = 1 AND rm.hid = '/' AND ugo.Type = 1

			GROUP BY ugo.UserGroupId, rm.TreeId, rp.ItemTypeId, rp.StateId
			UNION
			SELECT  ugm.MemberId AS UserGroupId, CASE WHEN(rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, rp.ItemTypeId, rp.StateId, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank
			FROM tblRoleMembership AS rm
						INNER JOIN (SELECT Id AS rank, RoleId FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 0) AS rr ON rm.RoleId = rr.RoleId
						INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupId = rm.UserGroupId
						INNER JOIN tblUserGroupMembership AS ugm ON ugo.UserGroupId = ugm.UserGroupId
						INNER JOIN (SELECT DISTINCT RoleId, ItemTypeId, ISNULL(StateId, '00000000-0000-0000-0000-000000000000') AS StateId FROM tblRolePermissions) AS rp ON rm.RoleId = rp.RoleId
			WHERE rm.IsReal = 1 AND rm.hid = '/' AND ugo.Type = 2
			GROUP BY ugm.MemberId, rm.TreeId, rp.ItemTypeId, rp.StateId) AS perm
	GROUP BY userGroupId, treeid, ItemTypeId, StateId
END

GO