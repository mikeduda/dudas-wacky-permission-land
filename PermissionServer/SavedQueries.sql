﻿
ALTER DATABASE DUDADB5 SET RECOVERY BULK_LOGGED;
ALTER DATABASE DUDADB5 SET RECOVERY FULL;

select r.usergroupid,  u.rank, count(r.usergroupid)
from tblRolemembership as r
	inner join (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) as u on r.usergroupid = u.usergroupid
group by  r.usergroupid,u.rank
order by count(r.usergroupid) desc



UPDATE  DUDADB4.[dbo].[tblAppliedPermissionsStatus] SET rmcount = 0, stateid = 0
UPDATE  DUDADB5.[dbo].[tblAppliedPermissionsStatus] SET rmcount = 0, stateid = 0
UPDATE  CLD_82Starwood.[dbo].[tblAppliedPermissionsStatus] SET rmcount = 0, stateid = 0


select * from DUDADB4.[dbo].[tblAppliedPermissionsStatus]
select top(1) * from DUDADB4.[dbo].[tblAppliedPermissionsDurations] order by lastran desc
select * from DUDADB5.[dbo].[tblAppliedPermissionsStatus]
select top(1) * from DUDADB5.[dbo].[tblAppliedPermissionsDurations] order by ranon desc
select * from CLD_82Starwood.[dbo].[tblAppliedPermissionsStatus]
select top(1) * from CLD_82Starwood.[dbo].[tblAppliedPermissionsDurations] order by lastran desc



select count(*) from  DUDADB5.dbo.tblAppliedPermissionsGlobal
select count(*) from  DUDADB5.dbo.tblAppliedPermissionsHierarchical
select count(*) from  DUDADB5.dbo.tblAppliedPermissionsStaging
select count(*) from  DUDADB4.dbo.tblAppliedPermissionsGlobal
select count(*) from  DUDADB4.dbo.tblAppliedPermissionsHierarchical
select count(*) from  DUDADB4.dbo.tblAppliedPermissionsStaging
select count(*) from  CLD_82Starwood.dbo.tblAppliedPermissionsGlobal
select count(*) from  CLD_82Starwood.dbo.tblAppliedPermissionsHierarchical
select count(*) from  CLD_82Starwood.dbo.tblAppliedPermissionsStaging

delete  from tblAppliedPermissionsStaging
delete from tblAppliedPermissionsHierarchical

select * from tblRolemembership where hid = '/' and isreal = 1


DECLARE @DB NVARCHAR(MAX)
DECLARE @COMMAND NVARCHAR(MAX) = 'DECLARE @DURATIONS TABLE(db NVARCHAR(MAX), duration NVARCHAR(MAX), status NVARCHAR(MAX))
';
DECLARE @DBS TABLE(name NVARCHAR(MAX))
INSERT @DBS SELECT '[CLD_82Demo]'
INSERT @DBS SELECT '[CLD_82Fox]'
INSERT @DBS SELECT '[CLD_82Jamie]'
INSERT @DBS SELECT '[CLD_82Jay]'
INSERT @DBS SELECT '[CLD_82ManualNG]'
INSERT @DBS SELECT '[CLD_82ProductSSO]'
INSERT @DBS SELECT '[CLD_82SmokeTestNGTest]'
INSERT @DBS SELECT '[CLD_90Knobel]'
INSERT @DBS SELECT '[CLD_90MarkDemoTemplate]'
INSERT @DBS SELECT '[CLD_AirCanada]'
INSERT @DBS SELECT '[CLD_Alpha82]'
INSERT @DBS SELECT '[CLD_AltaLink]'
INSERT @DBS SELECT '[CLD_Canmanie]'
INSERT @DBS SELECT '[CLD_Fox81]'
INSERT @DBS SELECT '[CLD_Fox811]'
INSERT @DBS SELECT '[CLD_SmokePermissions]'
INSERT @DBS SELECT '[CLD_SmokePermissions811]'
INSERT @DBS SELECT '[CLD_StreetCapital]'
INSERT @DBS SELECT '[CLD_TestingC]'
INSERT @DBS SELECT '[DUDADB5]'
INSERT @DBS SELECT '[DUDADB4]'
INSERT @DBS SELECT '[CLD_82Starwood]'

DECLARE DBLOOP CURSOR FOR (SELECT name FROM @DBS )
OPEN DBLOOP
FETCH NEXT FROM DBLOOP INTO @DB
WHILE @@FETCH_STATUS = 0
BEGIN	
	SET @COMMAND = @COMMAND + 'INSERT @DURATIONS SELECT ''' + @DB + ''', (SELECT TOP(1) duration FROM ' + @DB + '.[dbo].[tblAppliedPermissionsDurations] ORDER BY lastran DESC), (SELECT state FROM ' + @DB + '.[dbo].[tblAppliedPermissionsStatus])
	';
FETCH NEXT FROM DBLOOP INTO @DB
END
CLOSE DBLOOP
DEALLOCATE DBLOOP

SET @COMMAND = @COMMAND + '	SELECT * FROM @DURATIONS '
EXEC (@COMMAND)

--1C288BCA-CA3B-4A43-B0FA-B72B92E82874
select * from tblUsers where fullname like '%lisa%'
select * from tblRolemembership where usergroupid  = '1C288BCA-CA3B-4A43-B0FA-B72B92E82874' and isreal = 1

DECLARE @TREEID UNIQUEIDENTIFIER = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
DECLARE @TREE INT = 0
DECLARE @USERID UNIQUEIDENTIFIER = '1C288BCA-CA3B-4A43-B0FA-B72B92E82874'
DECLARE @ROOT HIERARCHYID = '/'
DECLARE @USER INT = (SELECT u.rank FROM
						(SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u 
							WHERE u.userGroupId = @USERID)
DECLARE @USERGROUPS IntTable 
INSERT  @USERGROUPS 
	SELECT [rank] 
	FROM  fnGetUserGroupMembership(@USERID)
			INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON ugo = u.UserGroupId

SELECT *
FROM	(SELECT RoleId, POWER(CAST(2 AS BIGINT), RANK() OVER (ORDER BY RoleId)) AS rank FROM tblRoles) AS r
		INNER JOIN (SELECT p.hid, [dbo].[BinaryOR](p.rank) AS rank
					FROM (SELECT li.hid, agp.rank
							FROM tblLinkedItems4_5 AS li		
									INNER JOIN tblItemSubTypes AS ist ON ist.itemid = li.SubItemID
									INNER JOIN tblAppliedPermissionsGlobal AS agp ON agp.itemtypeid = ist.itemtypeid
									LEFT JOIN tblWorkflowInstances AS wi ON wi.itemid = li.SubItemID AND ISNULL(wi.currentstate, '00000000-0000-0000-0000-000000000000') = agp.stateid 
							WHERE agp.userid = @USERID AND agp.treeid = @TREE AND li.TreeId = @TreeId AND li.hid.IsDescendantOf(@ROOT) = 1
							UNION
							SELECT app.hid, app.rank
							FROM DUDADB5_PERMISSIONS.dbo.tblAppliedPermissionsHierarchical as app 
									INNER JOIN @USERGROUPS AS ug ON app.userid = ug.value
							WHERE app.treeid = @TREE AND app.hid.IsDescendantOf(@ROOT) = 1) AS p 
							GROUP BY p.hid) AS p ON r.rank & p.rank > 0