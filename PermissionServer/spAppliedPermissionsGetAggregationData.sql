﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[spAppliedPermissionsGetAggregationData]
(
	@TREEID UNIQUEIDENTIFIER
)
AS
BEGIN
	DELETE FROM [dbo].[tblAppliedPermissionsRoleMaps] WHERE IsCurrent = 0

	INSERT INTO [dbo].[tblAppliedPermissionsRoleMaps]
		SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId, 0 FROM tblRoles AS rr WHERE Status = 1

	DELETE FROM [dbo].[tblAppliedPermissionsUserMaps] WHERE IsCurrent = 0

	INSERT INTO [dbo].[tblAppliedPermissionsUserMaps]
		SELECT RANK() OVER (ORDER BY UserGroupID) AS rank, UserGroupID, 0 FROM tblUserGroupObjects WHERE Status = 1

	SELECT rm.hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 1 AS isreal
	FROM tblRoleMembership AS rm 
		INNER JOIN (SELECT UserId, Id AS rank FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 0) AS u ON rm.UserGroupID = u.UserID
		INNER JOIN (SELECT RoleId, Id AS rank FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 0) AS rr ON rr.RoleId = rm.RoleId
	WHERE rm.TreeId = @TREEID AND rm.IsReal = 1 AND rm.hid <> '/'
	GROUP BY rm.hid, u.rank
	UNION ALL
	SELECT rm.hid, u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
	FROM tblRoleMembership as rm 
		INNER JOIN (SELECT UserId, Id AS rank FROM tblAppliedPermissionsUserMaps WHERE IsCurrent = 0) AS u ON rm.UserGroupID = u.UserID
		INNER JOIN (SELECT RoleId, Id AS rank FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 0) AS rr ON rr.RoleId = rm.RoleId
	WHERE rm.IsReal = 0 and rm.TreeId = @TREEID
	GROUP BY rm.hid, u.rank

	DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
    
	SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
    FROM (SELECT RoleId, Id AS rank FROM tblAppliedPermissionsRoleMaps WHERE IsCurrent = 0) AS rr
		INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
    ORDER BY rr.rank

END