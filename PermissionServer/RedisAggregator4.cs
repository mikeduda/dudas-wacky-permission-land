﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StackExchange.Redis;

namespace ResolverRisk.TreeServer
{
    public class ByteType
    {
        public ByteType(int key, byte[] data)
        {
            this.Key = key;
            this.Data = data;
        }

        public int Key { get; set; }

        public byte[] Data { get; set; }
    }

    /// <summary>
    /// TODO
    /// Dependent tables
    ///  might need extra summary for lst iteration of this loop
    ///  remember : workflowinstance.status = 0 assumed do not exist / items table state
    /// </summary>
    public static class RedisAggregator4
    {
        private static Guid Tree = new Guid("2E3D1C02-C154-47EE-A391-2C5480C5148A");//new Guid("3E172692-83C7-442A-BCA2-0476DE7C1F55");//
        public static Dictionary<string, ulong> Roles = new Dictionary<string, ulong>();
        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> RealMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        public static Dictionary<SqlHierarchyId, Dictionary<int, ulong>> AncestorMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        public static Dictionary<SqlHierarchyId, List<UserKey>> AncestorMembershipsByHid = new Dictionary<SqlHierarchyId, List<UserKey>>();

        public static bool IsGenerationRequired(string dbName)
        {
            string connStr = String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbName);

            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsIsGenerationRequired"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    int value = (int)cmd.ExecuteScalar();

                    if (value != 0) return true;
                }
            }

            return false;
        }

        public static void FullRebuild(string dbName)
        {
            string connStr = String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbName);

            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();

                DateTime now = DateTime.Now;

                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsSetGenerationParameters"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }

                using (SqlCommand cmd = new SqlCommand(String.Format(@"spGenerateGlobalPermissions"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQueryAsync();
                }

                RedisAggregator4.GenerateRoles(connection);
                RedisAggregator4.InsertBulk(connection, dbName);

                var time = DateTime.Now.Subtract(now);
                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsSetGenerationTime"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("duration", SqlDbType.NVarChar).Value = time.ToString();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void GenerateRoles(SqlConnection conn)
        {
            Roles.Clear();
            AncestorMemberships.Clear();
            AncestorMembershipsByHid.Clear();
            RealMemberships.Clear();

            var realMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();

            #region Get memberships

            using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
                        SELECT rm.hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 1 AS isreal
                        FROM tblRoleMembership AS rm 
	                        INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                        INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                        WHERE rm.TreeId = '{0}' AND rm.IsReal = 1 AND rm.hid <> '/'
                        GROUP BY rm.hid, u.rank
	                    UNION ALL
                        SELECT rm.hid, u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
                        FROM tblRoleMembership as rm 
	                        INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                        INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                        WHERE rm.IsReal = 0 and rm.TreeId = '{0}'
                        GROUP BY rm.hid, u.rank", Tree), conn))
            {
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var userid = (int)reader.GetInt64(1);
                        ulong rank = (ulong)reader.GetInt64(2);
                        int isReal = reader.GetInt32(3);

                        if (isReal == 1)
                        {
                            if (RealMemberships.ContainsKey(hid))
                            {
                                if (RealMemberships[hid].ContainsKey(userid))
                                {
                                    RealMemberships[hid][userid] |= rank;
                                }
                                else
                                {
                                    RealMemberships[hid].Add(userid, rank);
                                }
                            }
                            else
                            {
                                var member = new Dictionary<int, ulong>();
                                member.Add(userid, rank);

                                RealMemberships.Add(hid, member);
                            }
                        }
                        else
                        {
                            Dictionary<int, ulong> rolesAt = null;
                            if (AncestorMemberships.TryGetValue(hid, out rolesAt))
                            {
                                rolesAt.Add(userid, rank);
                            }
                            else
                            {
                                rolesAt = new Dictionary<int, ulong>();
                                rolesAt.Add(userid, rank);
                                AncestorMemberships.Add(hid, rolesAt);
                            }

                            List<UserKey> usersAt = null;
                            if (AncestorMembershipsByHid.TryGetValue(hid, out usersAt))
                            {
                                usersAt.Add(new UserKey(userid, rank));
                            }
                            else
                            {
                                AncestorMembershipsByHid.Add(hid, new List<UserKey>() { new UserKey(userid, rank) });
                            }

                        }
                    }
                }
            }

            #endregion

            #region Load roles

            using (SqlCommand cmd = new SqlCommand(String.Format(@"
                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
                    SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
                    FROM (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr
	                INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
                    ORDER BY rr.rank"), conn))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var rank = (ulong)reader.GetInt64(0);
                        var itemtype = reader.GetInt32(1);
                        var state = reader.GetGuid(2);

                        var key = itemtype.ToString() + state.ToString();

                        if (Roles.ContainsKey(key))
                        {
                            Roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
                        }
                        else
                        {
                            Roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
                        }
                    }
                }
            }

            #endregion
        }

        public static IEnumerable<NodeRoles> GenerateTree(SqlConnection conn)
        {
            SqlHierarchyId currentHid = SqlHierarchyId.Null;
            bool firstRun = true;
            Guid currentState = Guid.Empty;
            int currentSid = 0;

            using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT li.hid, wi.state, ist.ItemTypeId, i.itemsid
                        FROM tblLinkedItems4_5 AS li 
                              INNER JOIN tblItems AS i ON li.SubItemID = i.ItemId
			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
                        WHERE li.TreeID = '{0}'
                        ORDER BY li.hid", Tree), conn))
            {
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    var typeList = new List<int>();
                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                        var type = reader.GetInt32(2);
                        var sid = reader.GetInt32(3);

                        if (firstRun)
                        {
                            currentHid = hid;
                            currentSid = sid;
                            firstRun = false;
                        }

                        //Summarize hid
                        if (currentHid != hid)
                        {
                            //summarize currentHid
                            yield return Summarize(currentHid, currentSid, currentState, typeList);

                            currentHid = hid;
                            currentSid = sid;
                            currentState = state;
                            typeList.Clear();
                            typeList.Add(type);
                        }
                        else
                        {
                            currentState = state;
                            typeList.Add(type);
                        }
                    }

                    yield return Summarize(currentHid, currentSid, currentState, typeList);
                }
            }
        }

        public static IEnumerable<PermissionRow> YieldAggregate(SqlConnection conn)
        {
            var NodeMemberships = new Stack<MembershipSet2>();

            //Initialize empty root memberships
            NodeMemberships.Push(new MembershipSet2(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

            //Traverse the tree depth first
            foreach (var node in GenerateTree(conn))
            {
                //Pop until we hit the parent of current node
                while (NodeMemberships.Peek().Hid != node.Hid.GetAncestor(1)) NodeMemberships.Pop();

                //Get parent permissions
                var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

                //Combine current node applicable permission with parent applicable permissions for each user
                Combine(node.Hid, nodeMemberships);

                //Cache for descendants
                NodeMemberships.Push(new MembershipSet2(node.Hid, nodeMemberships));

                //roleMask contains applicable roles at this node, roleMembers the user roles
                foreach (var y in YieldApply(node.Hid, node.Sid, node.Mask, nodeMemberships))
                {
                    yield return y;
                }
            }

            AncestorMemberships.Clear();
            AncestorMembershipsByHid.Clear();
            RealMemberships.Clear();
            Roles.Clear();
        }

        public static void InsertBulk(SqlConnection conn, string dbName)
        {
            string connStr = String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbName);

            using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsClearStaging"), conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }

            using (GenericListDataReader reader = new GenericListDataReader(YieldAggregate(conn)))
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connStr, SqlBulkCopyOptions.TableLock))
                {
                    bulkCopy.DestinationTableName = "[dbo].[tblAppliedPermissionsStaging]";
                    bulkCopy.BatchSize = 250000;
                    bulkCopy.BulkCopyTimeout = 300;
                    bulkCopy.EnableStreaming = true;

                    bulkCopy.ColumnMappings.Add("userid", "userid");
                    bulkCopy.ColumnMappings.Add("treeid", "treeid");                   
                    bulkCopy.ColumnMappings.Add("hid", "hid");
                    bulkCopy.ColumnMappings.Add("rank", "rank");

                    bulkCopy.WriteToServer(reader);
                    bulkCopy.Close();
                }
            }

            using (SqlCommand cmd = new SqlCommand(String.Format(@"spMergeHierarchicalPermissions"), conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        private static NodeRoles Summarize(SqlHierarchyId hid, int sid, Guid state, List<int> types)
        {
            ulong nodeRoles = 0;
            foreach (var type in types)
            {
                var key = type.ToString() + state.ToString();
                ulong rolesForType = 0;

                if (Roles.TryGetValue(key, out rolesForType))
                {
                    nodeRoles |= rolesForType;
                }
            }

            return new NodeRoles(hid, sid, nodeRoles);
        }

        private static void Combine(SqlHierarchyId hid, Dictionary<int, ulong> parentMemberships)
        {
            Dictionary<int, ulong> members = null;
            if (RealMemberships.TryGetValue(hid, out members))
            {
                foreach (var member in members)
                {
                    if (parentMemberships.ContainsKey(member.Key))
                    {
                        parentMemberships[member.Key] |= member.Value;
                    }
                    else
                    {
                        parentMemberships.Add(member.Key, member.Value);
                    }
                }
            }
        }

        private static IEnumerable<PermissionRow> YieldApply(SqlHierarchyId hid, int itemSid, ulong roleMask, Dictionary<int, ulong> parentMemberships)
        {
            bool anyRealMembers = false;

            Dictionary<int, ulong> ancestorRoles = null;
            bool hasAncestors = AncestorMemberships.TryGetValue(hid, out ancestorRoles);

            foreach (var keyValue in parentMemberships)
            {
                anyRealMembers = true;

                //Combine with any ancestor permissions
                ulong ancestorPermission = 0;
                if (hasAncestors) ancestorRoles.TryGetValue(keyValue.Key, out ancestorPermission);

                ulong appliedRoles = (keyValue.Value | ancestorPermission) & roleMask;

                if (appliedRoles > 0)
                {
                    yield return new PermissionRow(keyValue.Key, hid, appliedRoles);
                }
            }

            //Add any ancestor permissions
            if (!anyRealMembers)
            {
                List<UserKey> userKeys = null;

                if (AncestorMembershipsByHid.TryGetValue(hid, out userKeys))
                {
                    foreach (var keyValue in userKeys)
                    {
                        ulong appliedRoles = keyValue.Rank & roleMask;
                        if (appliedRoles > 0)
                        {
                            yield return new PermissionRow(keyValue.User, hid, appliedRoles);
                        }
                    }
                }
            }
        }
    }
}
