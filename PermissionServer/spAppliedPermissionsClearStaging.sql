﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsClearStaging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsClearStaging]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsClearStaging
AS
BEGIN
	TRUNCATE TABLE tblAppliedPermissionsStaging
END

GO