﻿/****** Object:  Table [dbo].[tblAppliedPermissionsStatus]    Script Date: 2/26/2015 11:14:06 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsStatus]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsStatus]    Script Date: 2/26/2015 11:14:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsStatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[stateid] [int] NOT NULL,
	[state]  AS (case when [stateid]=(0) then 'idle' else 'generating' end),
	[lastran] [datetime] NOT NULL,
	[rmdate] [datetime] NOT NULL,
	[rpdate] [datetime] NOT NULL,
	[lidate] [datetime] NOT NULL,
	[istdate] [datetime] NOT NULL,
	[widate] [datetime] NOT NULL,
	[rmcount] [int] NOT NULL,
	[rpcount] [int] NOT NULL,
	[licount] [int] NOT NULL,
	[istcount] [int] NOT NULL,
	[wicount] [int] NOT NULL,
 CONSTRAINT [PK_AppliedPermissionsStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[tblAppliedPermissionsGlobal]    Script Date: 2/26/2015 11:13:53 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsGlobal]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsGlobal]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsGlobal]    Script Date: 2/26/2015 11:13:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsGlobal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [uniqueidentifier] NOT NULL,
	[treeid] [int] NOT NULL,
	[itemtypeid] [int] NOT NULL,
	[stateid] [uniqueidentifier] NOT NULL,
	[rank] [bigint] NOT NULL,
 CONSTRAINT [PK_AppliedPermissionsGlobal] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsGlobal]') AND name = N'IX_User')
BEGIN
	DROP INDEX [IX_User] ON [dbo].[tblAppliedPermissionsGlobal]
END
GO

/****** Object:  Index [IX_Hierarchical]    Script Date: 3/3/2015 1:50:27 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_User] ON [dbo].[tblAppliedPermissionsGlobal]
(
	[userid] ASC,
	[treeid] ASC,
	[itemtypeid] ASC,
	[stateid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppliedPermissionsDurations]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppliedPermissionsDurations]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsDurations]    Script Date: 2/26/2015 11:13:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsDurations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[duration] [nvarchar](250) NULL,
	[lastran] [datetime] NOT NULL
 CONSTRAINT [PK_AppliedPermissionsDurations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsIsGenerationRequired]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsIsGenerationRequired]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsIsGenerationRequired
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM [dbo].[tblAppliedPermissionsStatus] WHERE stateid = 1)SELECT 0
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblRoleMembership) AS t
							INNER JOIN (SELECT rmdate AS date, rmcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblRolePermissions) AS t
							INNER JOIN (SELECT rpdate AS date, rpcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblLinkedItems4_5) AS t
							INNER JOIN (SELECT lidate AS date, licount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1				
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblItemSubTypes) AS t
							INNER JOIN (SELECT istdate AS date, istcount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1				
	ELSE IF EXISTS(SELECT 1 FROM (SELECT MAX(updatedon) AS date, COUNT(*) AS count FROM tblWorkflowInstances) AS t
							INNER JOIN (SELECT widate AS date, wicount AS count FROM [dbo].[tblAppliedPermissionsStatus]) AS s ON t.date > s.date OR t.count != s.count) SELECT 1	
	ELSE SELECT 0

END

GO

/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsSetGenerationParameters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsSetGenerationParameters]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsSetGenerationParameters
AS
BEGIN
	
	UPDATE tblAppliedPermissionsStatus
	SET stateid = 1,
		rmdate =  (SELECT MAX(updatedon) AS date FROM tblRoleMembership),
		rmcount = (SELECT COUNT(*) FROM tblRoleMembership),
		rpdate =  (SELECT MAX(updatedon) AS date FROM tblRolePermissions),
		rpcount = (SELECT COUNT(*) FROM tblRolePermissions),
		lidate =  (SELECT MAX(updatedon) AS date FROM tblLinkedItems4_5),
		licount = (SELECT COUNT(*) FROM tblLinkedItems4_5),
		istdate = (SELECT MAX(updatedon) AS date FROM tblItemSubTypes),
		istcount = (SELECT COUNT(*) FROM tblItemSubTypes),
		widate =  (SELECT MAX(updatedon) AS date FROM tblWorkflowInstances),
		wicount = (SELECT COUNT(*) FROM tblWorkflowInstances)

END

GO

/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsSetGenerationTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsSetGenerationTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsSetGenerationTime
(
	@duration NVARCHAR(250)
)
AS
BEGIN

	UPDATE tblAppliedPermissionsStatus SET stateid = 0, lastran = GETDATE()

	INSERT INTO tblAppliedPermissionsDurations SELECT @duration, GETDATE()
END

GO

/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateGlobalPermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateGlobalPermissions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spGenerateGlobalPermissions
AS
BEGIN

TRUNCATE TABLE tblAppliedPermissionsGlobal

INSERT INTO tblAppliedPermissionsGlobal
	SELECT userGroupId, treeid, ItemTypeId, StateId, [dbo].[BinaryOR](rank) AS rank
	FROM	(SELECT ugo.UserGroupId, CASE WHEN(rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, rp.ItemTypeId, rp.StateId, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank
			FROM tblRoleMembership AS rm
						INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rm.RoleId = rr.RoleId
						INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupId = rm.UserGroupId
						INNER JOIN (SELECT DISTINCT RoleId, ItemTypeId, ISNULL(StateId, '00000000-0000-0000-0000-000000000000') AS StateId FROM tblRolePermissions) AS rp ON rm.RoleId = rp.RoleId
			WHERE rm.IsReal = 1 AND rm.hid = '/' AND ugo.Type = 1

			GROUP BY ugo.UserGroupId, rm.TreeId, rp.ItemTypeId, rp.StateId
			UNION
			SELECT  ugm.MemberId AS UserGroupId, CASE WHEN(rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, rp.ItemTypeId, rp.StateId, [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank
			FROM tblRoleMembership AS rm
						INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rm.RoleId = rr.RoleId
						INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupId = rm.UserGroupId
						INNER JOIN tblUserGroupMembership AS ugm ON ugo.UserGroupId = ugm.UserGroupId
						INNER JOIN (SELECT DISTINCT RoleId, ItemTypeId, ISNULL(StateId, '00000000-0000-0000-0000-000000000000') AS StateId FROM tblRolePermissions) AS rp ON rm.RoleId = rp.RoleId
			WHERE rm.IsReal = 1 AND rm.hid = '/' AND ugo.Type = 2
			GROUP BY ugm.MemberId, rm.TreeId, rp.ItemTypeId, rp.StateId) AS perm
	GROUP BY userGroupId, treeid, ItemTypeId, StateId
END

GO

IF NOT EXISTS(SELECT 1 FROM [tblAppliedPermissionsStatus])
BEGIN
	INSERT INTO [tblAppliedPermissionsStatus]
		SELECT 0, GETDATE(), GETDATE(), GETDATE(), GETDATE(), GETDATE(), GETDATE(), 0, 0, 0, 0, 0
END

/****** Object:  Table [dbo].[tblAppliedPermissionsRoleMaps]    Script Date: 3/13/2015 3:39:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsRoleMaps](
	[Id] [int] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
 CONSTRAINT [PK_tblAppliedPermissionsRoleMaps] PRIMARY KEY NONCLUSTERED 
(
	[IsCurrent] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[tblAppliedPermissionsUserMaps]    Script Date: 3/13/2015 3:39:19 PM ******/
DROP TABLE [dbo].[tblAppliedPermissionsUserMaps]
GO

/****** Object:  Table [dbo].[tblAppliedPermissionsUserMaps]    Script Date: 3/13/2015 3:39:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedPermissionsUserMaps](
	[Id] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
 CONSTRAINT [PK_tblAppliedPermissionsUserMaps] PRIMARY KEY NONCLUSTERED 
(
	[IsCurrent] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO