﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAppliedPermissionsSetGenerationTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAppliedPermissionsSetGenerationTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spAppliedPermissionsSetGenerationTime
(
	@duration NVARCHAR(250)
)
AS
BEGIN

	UPDATE tblAppliedPermissionsStatus SET stateid = 0, lastran = GETDATE()

	INSERT INTO tblAppliedPermissionsDurations SELECT @duration, GETDATE()
END

GO