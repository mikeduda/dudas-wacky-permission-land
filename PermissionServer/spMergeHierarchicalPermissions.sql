﻿/****** Object:  StoredProcedure [Cloud.Item].[spGetItemNodes]    Script Date: 2/20/2015 2:23:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMergeHierarchicalPermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMergeHierarchicalPermissions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC spMergeHierarchicalPermissions
AS
BEGIN

	MERGE [dbo].[tblAppliedPermissionsHierarchical] AS aph
	USING [dbo].[tblAppliedPermissionsStaging] AS aps
		ON aph.userid = aps.userid AND aph.treeid = aps.treeid AND aph.hid = aps.hid

	WHEN MATCHED AND aps.[rank] != aph.[rank] THEN
	  UPDATE SET aph.[rank] = aps.[rank]

	WHEN NOT MATCHED BY TARGET THEN
	  INSERT ([userid],[treeid],[hid],[rank])
	  VALUES (aps.[userid], aps.[treeid], aps.[hid], aps.[rank])

	WHEN NOT MATCHED BY SOURCE THEN
	  DELETE;

	TRUNCATE TABLE tblAppliedPermissionsStaging

	DELETE FROM [dbo].[tblAppliedPermissionsRoleMaps] WHERE IsCurrent = 1
	UPDATE [dbo].[tblAppliedPermissionsRoleMaps] SET IsCurrent = 1 WHERE IsCurrent = 0

	DELETE FROM [dbo].[tblAppliedPermissionsUserMaps] WHERE IsCurrent = 1
	UPDATE [dbo].[tblAppliedPermissionsUserMaps] SET IsCurrent = 1 WHERE IsCurrent = 0

END

GO