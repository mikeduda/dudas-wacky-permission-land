using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;

namespace PermissionAggregator.Logic
{
    public struct Role
    {
        public Role(ulong rank, int itemtype, Guid stateId)
        {
            this.Rank = rank;
            this.ItemTypeId = itemtype;
            this.StateId = stateId;
        }

        public ulong Rank;
        public int ItemTypeId;
        public Guid StateId;
      
    }
}
