//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;

namespace PermissionAggregator.Logic
{
    public struct ApplicableRole
    {
        public ApplicableRole(SqlHierarchyId hid, ulong rank, Guid userId)
        {
            this.Rank = rank;
            this.Hid = hid;
            this.UserId = userId;
        }

        public ulong Rank;
        public SqlHierarchyId Hid;
        public Guid UserId;
    }
}
