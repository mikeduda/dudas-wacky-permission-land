using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;

namespace PermissionAggregator.Logic
{
    public struct Membership
    {
        public Membership(ulong rank, Guid user)
        {
            Rank = rank;
            User = user;
        }

        public ulong Rank;
        public Guid User;
    }
}
