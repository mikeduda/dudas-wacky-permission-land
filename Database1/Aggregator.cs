//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using PermissionAggregator.Logic;

namespace PermissionAggregator.Logic2
{
    public struct ApplicableRole
    {
        public ApplicableRole(SqlHierarchyId hid, ulong rank, int userId)
        {
            this.Rank = rank;
            this.Hid = hid;
            this.UserId = userId;
            this.Treeid = 0;
        }

        public ulong Rank;
        public SqlHierarchyId Hid;
        public int UserId;
        public int Treeid;
    }

    public struct Membership
    {
        public Membership(ulong rank, int user)
        {
            this.Rank = rank;
            this.User = user;
        }

        public ulong Rank;
        public int User;
    }

    public struct Role
    {
        public Role(ulong rank, int itemtype, Guid stateId, int lookupType)
        {
            this.Rank = rank;
            this.LookupType = lookupType;
            this.ItemTypeId = itemtype;
            this.StateId = stateId;
        }

        public ulong Rank;
        public int LookupType;
        public int ItemTypeId;
        public Guid StateId;
    }

    public struct MembershipSet
    {
        public MembershipSet(SqlHierarchyId hid, List<Membership> memberships)
        {
            this.Hid = hid;
            this.Memberships = memberships;
        }

        public SqlHierarchyId Hid;
        public List<Membership> Memberships;
    }

    public static class Aggregator
    {
//        public static void Aggregate(string connString)
//        {
//            string connStr = connString;

//            Dictionary<ulong, Dictionary<int, Dictionary<int, int>>> Roles = new Dictionary<ulong, Dictionary<int, Dictionary<int, int>>>();
//            Stack<MembershipSet> Memberships = new Stack<MembershipSet>();
//            List<ApplicableRole> applied = new List<ApplicableRole>();
//            SqlConnection conn = new SqlConnection(connStr);

//            using (conn)
//            {
//                conn.Open();
//                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
//                    SELECT DISTINCT rt.rank, l.rank AS lookupType, CASE WHEN (it.ParentItemTypeID IS NULL) THEN it.ItemTypeId ELSE it.ParentItemTypeID END AS maintype
//				    FROM tblRolePermissions AS rp
//                        INNER JOIN tblItemTypes AS it ON rp.ItemTypeId = it.ItemTypeId
//					    INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rp.RoleId 
//					    INNER JOIN (SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
//								(SELECT ItemTypeId FROM tblItemTypes) AS it 
//								CROSS JOIN (SELECT Id FROM tblWorkflowStates UNION SELECT cast(cast(0 as binary) as uniqueidentifier)) AS w) AS l 
//										ON l.ItemTypeID = rp.ItemTypeId AND (l.Id = rp.StateId OR rp.StateId IS NULL AND l.Id = @EMPTY)
//                    WHERE rp.Status = 1
//                    ORDER BY rank "), conn))
//                {
//                    using (SqlDataReader reader = cmd2.ExecuteReader())
//                    {
//                        while (reader.Read())
//                        {
//                            ulong rank = (ulong)reader.GetInt64(0);
//                            int lookupType = (int)reader.GetInt64(1);
//                            int mainType = (int)reader.GetInt32(2);

//                            if (Roles.ContainsKey(rank))
//                            {
//                                Dictionary<int, Dictionary<int, int>> rankTypes = Roles[rank];
//                                if (rankTypes.ContainsKey(mainType))
//                                {
//                                    Dictionary<int, int> lookups = rankTypes[mainType];
//                                    lookups.Add(lookupType, 0);
//                                }
//                                else
//                                {
//                                    Dictionary<int, int> lookups = new Dictionary<int, int>();
//                                    lookups.Add(lookupType, 0);

//                                    rankTypes.Add(mainType, lookups);
//                                }
//                            }
//                            else
//                            {
//                                Dictionary<int, int> lookups = new Dictionary<int, int>();
//                                lookups.Add(lookupType, 0);

//                                Dictionary<int, Dictionary<int, int>> rankTypes = new Dictionary<int, Dictionary<int, int>>();
//                                rankTypes.Add(mainType, lookups);

//                                Roles.Add(rank, rankTypes);
//                            }
//                        }
//                    }
//                }

//                using (SqlCommand cmd = new SqlCommand(String.Format(@"
//                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
//
//                    IF object_id('tempdb..#TYPELOOKUP') IS NOT NULL BEGIN DROP TABLE #TYPELOOKUP END	
//                    CREATE TABLE #TYPELOOKUP (type int, itemType int, state UNIQUEIDENTIFIER PRIMARY KEY(type))
//                    
//                    CREATE INDEX IX_1 ON #TYPELOOKUP (itemType, state)
//
//                    IF object_id('tempdb..#ITEMSTATE') IS NOT NULL BEGIN DROP TABLE #ITEMSTATE END	
//                    CREATE TABLE #ITEMSTATE (itemid UNIQUEIDENTIFIER, state UNIQUEIDENTIFIER)
//
//                    INSERT #ITEMSTATE
//                    SELECT i.itemId, CASE WHEN(wi.CurrentState IS NULL) THEN @EMPTY ELSE wi.CurrentState END
//                    FROM tblItems AS i LEFT JOIN tblWorkflowInstances AS wi ON i.ItemID = wi.ItemId  
//                    WHERE i.Status = 1 AND (wi.Status = 1 OR wi.Id IS NULL)
//
//                    INSERT #TYPELOOKUP
//                    SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank,* FROM
//                    (SELECT ItemTypeId FROM tblItemTypes) AS it
//                    CROSS JOIN
//                    (SELECT Id FROM tblWorkflowStates UNION SELECT cast(cast(0 as binary) as UNIQUEIDENTIFIER)) AS w
//				
//                    SELECT  li.hid.ToString() AS hid, CASE WHEN(ist.ItemTypeID < 100) THEN ist.ItemTypeID ELSE NULL END AS itemtypeid, wi.state AS state, rr.rank AS rolerank, rm.UserGroupID AS userid ,tl.type, li.Hid.GetAncestor(1).ToString()
//                    FROM	tblLinkedItems4_5 AS li 
//			                    INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
//			                    LEFT JOIN #ITEMSTATE wi ON (wi.ItemID = li.SubItemID)
//				                    LEFT JOIN tblRoleMembership AS rm ON rm.TreeId = li.TreeID AND rm.Hid = li.HID AND rm.IsReal = 1 --AND rm.Hid <> '/'
//				                    LEFT JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
//				                    LEFT JOIN #TYPELOOKUP AS tl ON tl.itemType = ist.ItemTypeID AND (tl.state = wi.state)
//                    WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' --AND (rm.UserGroupID IS NULL OR rm.UserGroupID = 'FA3A8675-D235-4F57-ABA0-8FC5FD075A16')
//                    ORDER BY li.TreeID, li.hid"), conn))
//                {
//                    using (SqlDataReader reader = cmd.ExecuteReader())
//                    {
//                        string currentHid = "/", currentParentHid = "";
//                        string hid, parentHid;

//                        int itemType = 0;
//                        ulong rank = 0;
//                        int typeLookup = 0;
//                        Guid state = Guid.Empty;
//                        Guid user = Guid.Empty;
//                        Dictionary<int, int> itemTypes = new Dictionary<int, int>();
//                        List<Membership> rolesAtNode = new List<Membership>();
//                        Dictionary<int, int> lookUpTypes = new Dictionary<int, int>();

//                        //Initialize empty root memberships
//                        Memberships.Push(new MembershipSet("/", new List<Membership>()));

//                        while (reader.Read())
//                        {
//                            hid = reader.GetString(0);
//                            parentHid = reader.IsDBNull(6) ? "" : reader.GetString(6);

//                            if (!reader.IsDBNull(1)) itemType = reader.GetInt32(1);

//                            rank = reader.IsDBNull(3) ? 0 : (ulong)reader.GetInt64(3);
//                            typeLookup = reader.GetInt32(5);
//                            user = reader.IsDBNull(4) ? Guid.Empty : reader.GetGuid(4);

//                            //First row of new node - gather the node information - itemtype/state
//                            if (hid != currentHid)
//                            {
//                                #region Aggregate last node

//                                //If not initial root node
//                                if (currentHid != "/")
//                                {
//                                    //Pop until we hit the parent of current node
//                                    while (Memberships.Peek().Hid != currentParentHid) Memberships.Pop();

//                                    //Get parent permissions
//                                    List<Membership> combinedMemberships = new List<Membership>(Memberships.Peek().Memberships);

//                                    //Combine current node applicable permission with parent applicable permissions for each user
//                                    combinedMemberships.AddRange(rolesAtNode);

//                                    //Aggregate at this node and save
//                                    Combine(currentHid, itemType, lookUpTypes, combinedMemberships, Roles, applied, conn);

//                                    //Cache for descendants
//                                    Memberships.Push(new MembershipSet(currentHid, combinedMemberships));
//                                }

//                                #endregion

//                                #region Generate details for next node

//                                rolesAtNode.Clear();
//                                itemTypes.Clear();
//                                lookUpTypes.Clear();
//                                currentHid = hid;
//                                currentParentHid = parentHid;

//                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));

//                                #endregion
//                            }
//                            else
//                            {
//                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));
//                            }
//                        }

//                        //  InsertBulk(conn, applied);
//                    }
//                }
//            };
//        }

        //private static void Combine(string hid, int mainType, Dictionary<int, int> itemStructure, List<Membership> combinedMemberships, Dictionary<ulong, Dictionary<int, Dictionary<int, int>>> Roles, List<ApplicableRole> applied, SqlConnection conn)
        //{
        //    Dictionary<Guid, ApplicableRole> appliedLocal = new Dictionary<Guid, ApplicableRole>();
        //    foreach (Membership membership in combinedMemberships)
        //    {
        //        Dictionary<int, Dictionary<int, int>> lookup;
        //        if (Roles.TryGetValue(membership.Rank, out lookup))
        //        {
        //            //Check main type first
        //            Dictionary<int, int> rankTypes;
        //            if (lookup.TryGetValue(mainType, out rankTypes))
        //            {
        //                foreach (int k in itemStructure.Keys)
        //                {
        //                    if (rankTypes.ContainsKey(k))
        //                    {
        //                        ApplicableRole applicableRole;
        //                        if (appliedLocal.TryGetValue(membership.User, out applicableRole))
        //                        {
        //                            applicableRole.Rank &= (ulong)membership.Rank;
        //                        }
        //                        else
        //                        {
        //                            applicableRole = new ApplicableRole(hid, membership.Rank, membership.User);
        //                            appliedLocal.Add(membership.User, applicableRole);
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //    }
        //}

        public static IEnumerable StreamingAggregate(string connString)
        {
            string connStr = connString;
            Dictionary<ulong, Dictionary<int, Dictionary<int, int>>> Roles = new Dictionary<ulong, Dictionary<int, Dictionary<int, int>>>();
            Stack<MembershipSet> Memberships = new Stack<MembershipSet>();
            SqlConnection conn = new SqlConnection(connStr);

            using (conn)
            {
                #region Setup Roles cache

                conn.Open();
                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
                    SELECT DISTINCT rt.rank, l.rank AS lookupType, CASE WHEN (it.ParentItemTypeID IS NULL) THEN it.ItemTypeId ELSE it.ParentItemTypeID END AS maintype
                    FROM tblRolePermissions AS rp
                        INNER JOIN tblItemTypes AS it ON rp.ItemTypeId = it.ItemTypeId
	                    INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rp.RoleId 
	                    INNER JOIN (SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
			                    (SELECT ItemTypeId FROM tblItemTypes WHERE Status = 1) AS it 
			                    CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as uniqueidentifier)) AS w) AS l 
					                    ON l.ItemTypeID = rp.ItemTypeId AND (l.Id = rp.StateId OR rp.StateId IS NULL AND l.Id = @EMPTY)
                    WHERE rp.Status = 1 AND it.Status = 1
                    ORDER BY rank"), conn))
                {
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ulong rankId = (ulong)reader.GetInt64(0);
                            ulong rank = Convert.ToUInt64(Math.Pow(2, (double)rankId));

                            int lookupType = (int)reader.GetInt64(1);
                            int mainType = (int)reader.GetInt32(2);

                            if (Roles.ContainsKey(rank))
                            {
                                Dictionary<int, Dictionary<int, int>> rankTypes = Roles[rank];
                                if (rankTypes.ContainsKey(mainType))
                                {
                                    Dictionary<int, int> lookups = rankTypes[mainType];
                                    lookups.Add(lookupType, 0);
                                }
                                else
                                {
                                    Dictionary<int, int> lookups = new Dictionary<int, int>();
                                    lookups.Add(lookupType, 0);

                                    rankTypes.Add(mainType, lookups);
                                }
                            }
                            else
                            {
                                Dictionary<int, int> lookups = new Dictionary<int, int>();
                                lookups.Add(lookupType, 0);

                                Dictionary<int, Dictionary<int, int>> rankTypes = new Dictionary<int, Dictionary<int, int>>();
                                rankTypes.Add(mainType, lookups);

                                Roles.Add(rank, rankTypes);
                            }
                        }
                    }
                }

                #endregion

                #region Aggregate permissions

                using (SqlCommand cmd = new SqlCommand(String.Format(@"
                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))

                    IF object_id('tempdb..#TYPELOOKUP') IS NOT NULL BEGIN DROP TABLE #TYPELOOKUP END	
                    CREATE TABLE #TYPELOOKUP (type int, itemType int, state UNIQUEIDENTIFIER)

                    INSERT #TYPELOOKUP
                    SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
                    (SELECT ItemTypeId FROM tblItemTypes WHERE Status = 1) AS it 
                    CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as UNIQUEIDENTIFIER)) AS w

                    IF object_id('tempdb..#ITEMSTATE') IS NOT NULL BEGIN DROP TABLE #ITEMSTATE END	
                    CREATE TABLE #ITEMSTATE (itemid UNIQUEIDENTIFIER, maintype int, state UNIQUEIDENTIFIER)

                    INSERT #ITEMSTATE
                    SELECT i.itemId, i.ItemTypeId, CASE WHEN(wi.CurrentState IS NULL) THEN @EMPTY ELSE wi.CurrentState END
                    FROM tblItems AS i LEFT JOIN tblWorkflowInstances AS wi ON i.ItemID = wi.ItemId AND wi.Status = 1

                    IF object_id('tempdb..#ROLES') IS NOT NULL BEGIN DROP TABLE #ROLES END	
                    CREATE TABLE #ROLES (roleid UNIQUEIDENTIFIER, rank bigint)
                    INSERT #ROLES
                    SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles

                    IF object_id('tempdb..#MEMBERS') IS NOT NULL BEGIN DROP TABLE #MEMBERS END	
                    CREATE TABLE #MEMBERS (hid HIERARCHYID, userid INT, roleid UNIQUEIDENTIFIER)

                    INSERT #MEMBERS
                    SELECT rm.hid, u.rank AS userid, rm.RoleId 
                    FROM tblRoleMembership AS rm 
							INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
					WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND (rm.isHidden = 1 OR (rm.isHidden = 0 AND rm.Hid <> '/'))
				
                    SELECT  li.hid AS hid, wi.maintype AS maintype, rr.rank AS rolerank, rm.userid AS userid, tl.type
                    FROM	tblLinkedItems4_5 AS li WITH(NOLOCK)
			                    INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
			                        LEFT JOIN #ITEMSTATE AS wi ON (wi.ItemID = li.SubItemID)
				                    LEFT JOIN #MEMBERS AS rm ON rm.Hid = li.HID
				                    LEFT JOIN #ROLES AS rr ON rr.RoleId = rm.RoleId
				                    LEFT JOIN #TYPELOOKUP AS tl ON tl.itemType = ist.ItemTypeID AND (tl.state = wi.state)
                    WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 
                    ORDER BY li.hid"), conn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        SqlHierarchyId currentHid = SqlHierarchyId.Null, currentParentHid = SqlHierarchyId.Null;
                        SqlHierarchyId hid, parentHid;

                        int mainType = 0, currentItemType = 0;
                        ulong rankId = 0;
                        ulong rank = 0;
                        int typeLookup = 0;
                        Guid state = Guid.Empty;
                        int user = 0;
                        Dictionary<int, int> itemTypes = new Dictionary<int, int>();
                        List<Membership> rolesAtNode = new List<Membership>();
                        Dictionary<int, int> lookUpTypes = new Dictionary<int, int>();

                        //Initialize empty root memberships
                        Memberships.Push(new MembershipSet(SqlHierarchyId.Null, new List<Membership>()));

                        while (reader.Read())
                        {
                            hid = (SqlHierarchyId)reader.GetValue(0);
                            parentHid = hid.GetAncestor(1);

                            mainType = reader.GetInt32(1);

                            rankId = reader.IsDBNull(2) ? 0 : (ulong)reader.GetInt64(2);
                            rank = Convert.ToUInt64(Math.Pow(2, (double)rankId));
                            user = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                            typeLookup = reader.GetInt32(4);

                            //First row of new node - gather the node information - itemtype/state
                            if (currentHid.IsNull || hid != currentHid)
                            {
                                #region Aggregate last node

                                //If not initial root node
                                if (!currentHid.IsNull)
                                {
                                    //Pop until we hit the parent of current node
                                    while (Memberships.Peek().Hid != currentParentHid) Memberships.Pop();

                                    //Get parent permissions
                                    List<Membership> savedMemberships = new List<Membership>(Memberships.Peek().Memberships);

                                    //Combine current node applicable permission with parent applicable permissions for each user
                                    savedMemberships.AddRange(rolesAtNode);

                                    //Cache for descendants
                                    Memberships.Push(new MembershipSet(currentHid, savedMemberships));

                                    foreach (ApplicableRole ar in StreamingCombine(currentHid, currentItemType, lookUpTypes, new List<Membership>(savedMemberships), Roles))
                                    {
                                        yield return ar;
                                    }
                                }

                                #endregion

                                #region Generate details for next node

                                rolesAtNode.Clear();
                                itemTypes.Clear();
                                lookUpTypes.Clear();

                                currentHid = hid;
                                currentParentHid = parentHid;
                                currentItemType = mainType;

                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
                                if (user != 0) rolesAtNode.Add(new Membership(rank, user));

                                #endregion
                            }
                            else
                            {
                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
                                if (user != 0) rolesAtNode.Add(new Membership(rank, user));
                            }
                        }
                    }
                }

                #endregion
            };
        }

        private static List<ApplicableRole> StreamingCombine(SqlHierarchyId hid, int mainType, Dictionary<int, int> itemStructure, List<Membership> combinedMemberships, Dictionary<ulong, Dictionary<int, Dictionary<int, int>>> Roles)
        {
            Dictionary<int, ApplicableRole> applied = new Dictionary<int, ApplicableRole>();
            foreach (Membership membership in combinedMemberships)
            {
                Dictionary<int, Dictionary<int, int>> lookup;
                if (Roles.TryGetValue(membership.Rank, out lookup))
                {
                    //Check main type first
                    Dictionary<int, int> rankTypes;
                    if (lookup.TryGetValue(mainType, out rankTypes))
                    {
                        foreach (int k in itemStructure.Keys)
                        {
                            if (rankTypes.ContainsKey(k))
                            {
                                ApplicableRole applicableRole;
                                if (applied.TryGetValue(membership.User, out applicableRole))
                                {
                                    applicableRole.Rank |= (ulong)membership.Rank;
                                }
                                else
                                {
                                    applicableRole = new ApplicableRole(hid, membership.Rank, membership.User);
                                    applied.Add(membership.User, applicableRole);
                                }
                                break;
                            }
                        }
                    }
                }
            }

            return new List<ApplicableRole>(applied.Values);
        }
    }
}
