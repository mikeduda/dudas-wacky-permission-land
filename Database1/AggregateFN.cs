//------------------------------------------------------------------------------
// <copyright file="CSSqlFunction.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using PermissionAggregator.Logic2;

public partial class PermissionFunctions
{
    [SqlFunction(FillRowMethodName = "FillRow",
                SystemDataAccess = SystemDataAccessKind.Read,
                DataAccess = DataAccessKind.None,
                TableDefinition = "[hid] nvarchar(max), [user] int, [rank] [bigint]")]

    public static IEnumerable Aggregate()
    {
        string connStr = "data source=.;initial catalog=DUDADB5;integrated security=SSPI;enlist=false";

        return Aggregator.StreamingAggregate(connStr);
    }

    public static void FillRow(Object obj, out SqlString hid, out SqlInt32 user, out SqlInt64 rank)
    {
        ApplicableRole role = (ApplicableRole)obj;

        hid = (SqlString)role.Hid.ToString();
        user = (SqlInt32)role.UserId;
        rank = (SqlInt64)System.Convert.ToInt64(role.Rank);
    }
}
