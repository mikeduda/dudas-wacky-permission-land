﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using System.Data.SqlClient;
using PermissionScheduler.Aggregation;

namespace PermissionScheduler
{
    public class QueueJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var map = context.JobDetail.JobDataMap;
            var dbname = map.GetString("dbname");

            try
            {
                var aggregator = new Aggregator(String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbname), dbname);

                if (aggregator.IsGenerationRequired())
                {
                    aggregator.Aggregate();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(String.Format("DB({0}) encountered errror: {1}", dbname, exc.ToString()));
            }
        }
    }
}
