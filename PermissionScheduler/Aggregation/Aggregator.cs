﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionScheduler.Aggregation
{
    public class Aggregator
    {
        private string _ConnectionString { get; set; }
        private string _ConnectionStringPermissionsDB
        {
            get
            {
                return _ConnectionString.Replace(this._Database, this._Database + "_PERMISSIONS");
            }
        }
        private string _Database;

        private Guid Tree = new Guid("2E3D1C02-C154-47EE-A391-2C5480C5148A");//new Guid("3E172692-83C7-442A-BCA2-0476DE7C1F55");//
        private Dictionary<string, ulong> _Roles = new Dictionary<string, ulong>();
        private Dictionary<SqlHierarchyId, Dictionary<int, ulong>> _RealMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        private Dictionary<SqlHierarchyId, Dictionary<int, ulong>> _AncestorMemberships = new Dictionary<SqlHierarchyId, Dictionary<int, ulong>>();
        private Dictionary<SqlHierarchyId, List<UserKey>> _AncestorMembershipsByHid = new Dictionary<SqlHierarchyId, List<UserKey>>();

        /// <summary>
        /// Creates a instance of Aggregator
        /// </summary>
        /// <param name="connectionString"></param>
        public Aggregator(string connectionString, string db)
        {
            _ConnectionString = connectionString;
            _Database = db;
        }

        private void SetupAggregationData(SqlConnection conn)
        {
            _Roles.Clear();
            _AncestorMemberships.Clear();
            _AncestorMembershipsByHid.Clear();
            _RealMemberships.Clear();

            #region Get memberships

            using (SqlCommand cmd3 = new SqlCommand("spAppliedPermissionsGetAggregationData", conn))
            {
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Parameters.Add("@TREEID", SqlDbType.UniqueIdentifier).Value = this.Tree;

                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    #region Get Memberships

                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var userid = (int)reader.GetInt32(1);
                        ulong rank = (ulong)reader.GetInt64(2);
                        int isReal = reader.GetInt32(3);

                        if (isReal == 1)
                        {
                            if (_RealMemberships.ContainsKey(hid))
                            {
                                if (_RealMemberships[hid].ContainsKey(userid))
                                {
                                    _RealMemberships[hid][userid] |= rank;
                                }
                                else
                                {
                                    _RealMemberships[hid].Add(userid, rank);
                                }
                            }
                            else
                            {
                                var member = new Dictionary<int, ulong>();
                                member.Add(userid, rank);

                                _RealMemberships.Add(hid, member);
                            }
                        }
                        else
                        {
                            Dictionary<int, ulong> rolesAt = null;
                            if (_AncestorMemberships.TryGetValue(hid, out rolesAt))
                            {
                                rolesAt.Add(userid, rank);
                            }
                            else
                            {
                                rolesAt = new Dictionary<int, ulong>();
                                rolesAt.Add(userid, rank);
                                _AncestorMemberships.Add(hid, rolesAt);
                            }

                            List<UserKey> usersAt = null;
                            if (_AncestorMembershipsByHid.TryGetValue(hid, out usersAt))
                            {
                                usersAt.Add(new UserKey(userid, rank));
                            }
                            else
                            {
                                _AncestorMembershipsByHid.Add(hid, new List<UserKey>() { new UserKey(userid, rank) });
                            }
                        }
                    }

                    #endregion

                    reader.NextResult();

                    #region Get Roles

                    while (reader.Read())
                    {
                        var rank = (ulong)reader.GetInt32(0);
                        var itemtype = reader.GetInt32(1);
                        var state = reader.GetGuid(2);

                        var key = itemtype.ToString() + state.ToString();

                        if (_Roles.ContainsKey(key))
                        {
                            _Roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
                        }
                        else
                        {
                            _Roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
                        }
                    }

                    #endregion
                }
            }

            #endregion
        }

        private void TearDownAggregationData()
        {
            _AncestorMemberships.Clear();
            _AncestorMembershipsByHid.Clear();
            _RealMemberships.Clear();
            _Roles.Clear();
        }

        private IEnumerable<NodeSummary> EnumerateTree(SqlConnection conn)
        {
            SqlHierarchyId currentHid = SqlHierarchyId.Null;
            bool firstRun = true;
            Guid currentState = Guid.Empty;

            using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT li.hid, wi.state, ist.ItemTypeId
                        FROM tblLinkedItems4_5 AS li 
			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
                        WHERE li.TreeID = '{0}'
                        ORDER BY li.hid", Tree), conn))
            {
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    var typeList = new List<int>();
                    while (reader.Read())
                    {
                        var hid = (SqlHierarchyId)reader.GetValue(0);
                        var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                        var type = reader.GetInt32(2);

                        if (firstRun)
                        {
                            currentHid = hid;
                            firstRun = false;
                        }

                        if (currentHid != hid)
                        {
                            yield return SummarizeNode(currentHid, currentState, typeList);

                            currentHid = hid;
                            currentState = state;
                            typeList.Clear();
                            typeList.Add(type);
                        }
                        else
                        {
                            currentState = state;
                            typeList.Add(type);
                        }
                    }

                    yield return SummarizeNode(currentHid, currentState, typeList);
                }
            }
        }

        private IEnumerable<PermissionRow> EnumeratePermissions(SqlConnection conn)
        {
            var NodeMemberships = new Stack<MembershipSet>();

            //Initialize empty root memberships
            NodeMemberships.Push(new MembershipSet(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

            //Traverse the tree depth first
            foreach (var node in EnumerateTree(conn))
            {
                //Pop until we hit the parent of current node
                while (NodeMemberships.Peek().Hid != node.Hid.GetAncestor(1)) NodeMemberships.Pop();

                //Get parent permissions
                var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

                //Combine current node applicable permission with parent applicable permissions for each user
                CombineNodePermissions(node.Hid, nodeMemberships);

                //Cache for descendants
                NodeMemberships.Push(new MembershipSet(node.Hid, nodeMemberships));

                //roleMask contains applicable roles at this node, roleMembers the user roles
                foreach (var y in EnumerateAppliedPermissions(node.Hid, node.Mask, nodeMemberships))
                {
                    yield return y;
                }
            }
        }

        private void AggregateAndPersistPermissions(SqlConnection conn, SqlConnection connPermissions)
        {
            using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsClearStaging"), connPermissions))
            {
                cmd.CommandTimeout = 120;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }

            using (PermissionDataReader reader = new PermissionDataReader(this.EnumeratePermissions(conn)))
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this._ConnectionStringPermissionsDB, SqlBulkCopyOptions.TableLock))
                {
                    bulkCopy.DestinationTableName = "[dbo].[tblAppliedPermissionsStaging]";
                    //bulkCopy.BatchSize = 250000;
                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.EnableStreaming = true;

                    bulkCopy.ColumnMappings.Add("userid", "userid");
                    bulkCopy.ColumnMappings.Add("treeid", "treeid");
                    bulkCopy.ColumnMappings.Add("hid", "hid");
                    bulkCopy.ColumnMappings.Add("rank", "rank");

                    bulkCopy.WriteToServer(reader);
                    bulkCopy.Close();
                }
            }

            using (SqlCommand cmd = new SqlCommand(String.Format(@"spMergeHierarchicalPermissions"), connPermissions))
            {
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        private NodeSummary SummarizeNode(SqlHierarchyId hid, Guid state, List<int> types)
        {
            ulong nodeRoles = 0;
            foreach (var type in types)
            {
                var key = type.ToString() + state.ToString();
                ulong rolesForType = 0;

                if (_Roles.TryGetValue(key, out rolesForType))
                {
                    nodeRoles |= rolesForType;
                }
            }

            return new NodeSummary(hid, nodeRoles);
        }

        private void CombineNodePermissions(SqlHierarchyId hid, Dictionary<int, ulong> parentMemberships)
        {
            Dictionary<int, ulong> members = null;
            if (_RealMemberships.TryGetValue(hid, out members))
            {
                foreach (var member in members)
                {
                    if (parentMemberships.ContainsKey(member.Key))
                    {
                        parentMemberships[member.Key] |= member.Value;
                    }
                    else
                    {
                        parentMemberships.Add(member.Key, member.Value);
                    }
                }
            }
        }

        private IEnumerable<PermissionRow> EnumerateAppliedPermissions(SqlHierarchyId hid, ulong roleMask, Dictionary<int, ulong> parentMemberships)
        {
            Dictionary<int, ulong> ancestorRoles = null;
            Dictionary<int, ulong> combinedRoles = new Dictionary<int, ulong>();

            if (_AncestorMemberships.TryGetValue(hid, out ancestorRoles))
            {
                //Comine dictionaries
                foreach (var keyVal in ancestorRoles.Concat(parentMemberships))
                {
                    if (combinedRoles.ContainsKey(keyVal.Key)) combinedRoles[keyVal.Key] |= keyVal.Value;
                    else
                    {
                        combinedRoles.Add(keyVal.Key, keyVal.Value);
                    }
                }
            }
            else
            {
                combinedRoles = parentMemberships; 
            }

            foreach (var keyValue in combinedRoles)
            {
                ulong appliedRoles = keyValue.Value & roleMask;

                if (appliedRoles > 0)
                {
                    yield return new PermissionRow(keyValue.Key, hid, appliedRoles);
                }
            }
        }

        /// <summary>
        /// Determines if generation is required
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public bool IsGenerationRequired()
        {
            using (var connection = new SqlConnection(this._ConnectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsIsGenerationRequired"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    int value = (int)cmd.ExecuteScalar();

                    if (value != 0) return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Aggregate permissions
        /// </summary>
        public void Aggregate()
        {
            using (SqlConnection connection = new SqlConnection(this._ConnectionString), connection2 = new SqlConnection(this._ConnectionStringPermissionsDB))
            {
                connection.Open();

                DateTime now = DateTime.Now;

                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsSetGenerationParameters"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }

                this.SetupAggregationData(connection);

                using (SqlCommand cmd = new SqlCommand(String.Format(@"spGenerateGlobalPermissions"), connection))
                {
                    cmd.CommandTimeout = 120;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQueryAsync();
                }


                connection2.Open();
                this.AggregateAndPersistPermissions(connection, connection2);
                this.TearDownAggregationData();

                using (SqlCommand cmd = new SqlCommand(String.Format(@"spAppliedPermissionsSetGenerationTime"), connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("duration", SqlDbType.NVarChar).Value = DateTime.Now.Subtract(now).ToString();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
