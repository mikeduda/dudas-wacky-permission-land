﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionScheduler.Aggregation
{
    public class MembershipSet
    {
        public MembershipSet(SqlHierarchyId hid, Dictionary<int, ulong> memberships)
        {
            this.Hid = hid;
            this.Memberships = memberships;
        }

        public readonly SqlHierarchyId Hid;
        public readonly Dictionary<int, ulong> Memberships;
    }

}
