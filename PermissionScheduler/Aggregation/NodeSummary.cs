﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionScheduler.Aggregation
{
    public struct NodeSummary : IComparable, IComparable<NodeSummary>
    {
        public NodeSummary(SqlHierarchyId hid, ulong mask)
        {
            this.Hid = hid;
            this.Mask = mask;
        }

        public readonly SqlHierarchyId Hid;
        public readonly ulong Mask;

        public int CompareTo(object obj)
        {
            return this.CompareTo((NodeSummary)obj);
        }

        public int CompareTo(NodeSummary other)
        {
            return this.Hid.CompareTo(other.Hid);
        }
    }
}
