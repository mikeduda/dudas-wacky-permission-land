﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionScheduler.Aggregation
{
    public struct PermissionRow
    {
        public PermissionRow(int userId, SqlHierarchyId hid, ulong mask)
        {
            this.UserId = userId;
            this.Hid = hid;
            this.Mask = mask;
        }

        public readonly int UserId;
        public readonly SqlHierarchyId Hid;
        public readonly ulong Mask;
    }
}
