﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionScheduler.Aggregation
{
    public struct UserKey
    {
        public readonly int User;
        public readonly ulong Rank;

        public UserKey(int user, ulong rank)
        {
            this.User = user;
            this.Rank = rank;
        }
    }
}
