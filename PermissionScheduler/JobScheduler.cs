﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Quartz;
using Quartz.Impl;
using System.IO;

namespace PermissionScheduler
{
    public static class JobScheduler
    {
        public static IScheduler Scheduler;
        public static IJobDetail Job;
        private static List<string> Databases = new List<string>();

        public static void Init()
        {
            using (var file = File.OpenText("databases.txt"))
            {
                while (!file.EndOfStream)
                {
                    Databases.Add(file.ReadLine());
                }
            }

            ScheduleGenerateJobs();
        }

        public static void Stop()
        {
            if (Scheduler != null)
                Scheduler.Shutdown(true);
        }

        public static void ScheduleGenerateJobs()
        {
            var properties = new NameValueCollection { { "quartz.threadPool.threadCount", "20" } };
            var schedulerFactory = new StdSchedulerFactory(properties);

            Scheduler = schedulerFactory.GetScheduler();
            Scheduler.Start();

            foreach (var dbName in Databases)
            {
                Job = JobBuilder.Create<QueueJob>()
                        .WithIdentity(dbName, "permissionGenerationSchedulerJob")
                        .UsingJobData("dbname", dbName)
                        .StoreDurably().Build();

                ITrigger trigger = TriggerBuilder.Create()
                        .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(60)
                        .RepeatForever())
                    .Build();

                Scheduler.ScheduleJob(Job, trigger);
            }
        }
    }
}
