﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;

namespace PermissionScheduler
{
    class Program
    {
        private static List<string> Databases = new List<string>();
        private static Dictionary<string, DateTime> Schedules = new Dictionary<string, DateTime>();
        private static Random random = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine("Starting Permission Tester.");
            Console.WriteLine("Press q to quit.");

            //Read databases in
            using (var file = File.OpenText("databases.txt"))
            {
                while (!file.EndOfStream)
                {
                    Databases.Add(file.ReadLine());
                }
            }

            //Setup initial schedule
            foreach (var item in Databases)
            {
                Schedules.Add(item, DateTime.Now.Add(TimeSpan.FromSeconds(300 - random.Next(240)))); //Anywhere from 1 to 2 minutes 
            }

            var key = new ConsoleKeyInfo();

            do
            {
                while (!Console.KeyAvailable)
                {
                    foreach (var itemKey in new Dictionary<string, DateTime>(Schedules))
                    {
                        if (DateTime.Now > itemKey.Value) ScheduleJob(itemKey.Key);
                    }

                    Thread.Sleep(100);
                }
                key = Console.ReadKey(true);
            } while (key.Key != ConsoleKey.Q);
        }

        private static void ScheduleJob(string dbName)
        {
            Schedules[dbName] = DateTime.Now.Add(TimeSpan.FromSeconds(300 - random.Next(240))); //Anywhere from 1 to 2 minutes 

            var connectionString = String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbName);

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(String.Format(@"UPDATE tblRoleMembership SET UpdatedOn = GETDATE() FROM tblRoleMembership AS rm INNER JOIN (SELECT TOP(1) RoleMemberSID FROM tblRoleMembership) AS rm2 ON rm.RoleMemberSID = rm2.RoleMemberSID"), connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
