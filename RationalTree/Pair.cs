﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RadixSolutionsUtilities
{
    public class Pair<P1, P2>
    {
        public P1 First { get; set; }
        public P2 Second { get; set; }

        public Pair(P1 first, P2 second)
        {
            First = first;
            Second = second;
        }
    }
}
