﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RationalTree
{
    public class Node
    {
        public int Left { get; set; }
        public int Right { get; set; }
        public string Data { get; set; }

        public Node(int left, int right, string data)
        {
            this.Left = left;
            this.Right = right;
            this.Data = data;
        }

    }
}
