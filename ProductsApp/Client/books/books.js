﻿angular.module('BooksApp', []).

controller('booksController', function ($scope) {
    $scope.booksList = [
      {
          Driver: {
              givenName: 'Sebastian',
              familyName: 'Vettel'
          },
          points: 322,
          nationality: "German",
          Constructors: [
              { name: "Red Bull" }
          ]
      },
      {
          Driver: {
              givenName: 'Fernando',
              familyName: 'Alonso'
          },
          points: 207,
          nationality: "Spanish",
          Constructors: [
              { name: "Ferrari" }
          ]
      }
    ];

    $scope.addItem = function () {
        $scope.booksList.push({
            Driver: { givenName: $scope.newItem.name },
            points: 122,
            nationality: "Spanish"
        });
    };
});