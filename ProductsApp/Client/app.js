﻿/// <reference path="lib/angular.js" />

var myApp = angular.module('myApp', ['ui.tree']);

myApp.factory('GetString', function () {
    return { message: "I'm data from a service" }
});

myApp.directive("datadirective", function () {
    return {
        restrict: "E",
        template: "<div> My directive!</div>"
    }
});

function FirstCtrl($scope, GetString) {
    $scope.data = GetString;
}

function SecondCtrl($scope, GetString) {
    $scope.data = GetString;
    $scope.reverse = function reverse(msg) {
        return msg.split("").reverse().join("");
    }
}

myApp.controller('treeCtrl', function ($scope, $http) {

    $scope.remove = function (scope) {
        scope.remove();
    };

    $scope.toggle = function (scope) {
        scope.toggle();
    };

    $scope.moveLastToTheBegginig = function () {
        var a = $scope.data.pop();
        $scope.data.splice(0, 0, a);
    };

    $scope.newSubItem = function (scope) {
        var nodeData = scope.$modelValue;
        nodeData.nodes.push({
            id: nodeData.id * 10 + nodeData.nodes.length,
            title: nodeData.title + '.' + (nodeData.nodes.length + 1),
            nodes: []
        });
    };

    var getRootNodesScope = function () {
        return angular.element(document.getElementById("tree-root")).scope();
    };

    $scope.collapseAll = function () {
        var scope = getRootNodesScope();
        scope.collapseAll();
    };

    $scope.expandAll = function () {
        var scope = getRootNodesScope();
        scope.expandAll();
    };

    $scope.treeServerCount = function () {
        $http({
            url: "../../api/treeclient/count",
            method: "GET"//,
            //data: {"foo":"bar"}
        }).success(function (data, status, headers, config) {
            $scope.treeTestData = data;
        }).error(function (data, status, headers, config) {
            $scope.treeTestData = "Error making call";
        });
    };

    $scope.treeServerRebuildRoles = function () {
        $http({
            url: "../../api/treeclient/rebuildroles",
            method: "GET"//,
            //data: {"foo":"bar"}
        }).success(function (data, status, headers, config) {
            $scope.treeTestData = data;
        }).error(function (data, status, headers, config) {
            $scope.treeTestData = "Error making call";
        });
    };

    $scope.treeServerRebuildTree = function () {
        $http({
            url: "../../api/treeclient/rebuildtree",
            method: "GET"//,
            //data: {"foo":"bar"}
        }).success(function (data, status, headers, config) {
            $scope.treeTestData = data;
        }).error(function (data, status, headers, config) {
            $scope.treeTestData = "Error making call";
        });
    };

    $scope.treeServerGenerate = function () {
        $http({
            url: "../../api/treeclient/generate",
            method: "GET"//,
            //data: {"foo":"bar"}
        }).success(function (data, status, headers, config) {
            $scope.treeTestData = data;
        }).error(function (data, status, headers, config) {
            $scope.treeTestData = "Error making call";
        });
    }; 

    $scope.treeServerGetDescendantPermission = function () {
        $http({
            url: "../../api/treeclient/getpermissions",
            method: "GET"//,
            //data: {"foo":"bar"}
        }).success(function (data, status, headers, config) {
            $scope.treeTestData = data;
        }).error(function (data, status, headers, config) {
            $scope.treeTestData = "Error making call";
        });
    };


    $scope.data = [{
        "id": 1,
        "title": "node1",
        "nodes": [
          {
              "id": 11,
              "title": "node1.1",
              "nodes": [
                {
                    "id": 111,
                    "title": "node1.1.1",
                    "nodes": []
                }
              ]
          },
          {
              "id": 12,
              "title": "node1.2",
              "nodes": []
          }
        ],
    }, {
        "id": 2,
        "title": "node2",
        "nodes": [
          {
              "id": 21,
              "title": "node2.1",
              "nodes": []
          },
          {
              "id": 22,
              "title": "node2.2",
              "nodes": []
          }
        ],
    }, {
        "id": 3,
        "title": "node3",
        "nodes": [
          {
              "id": 31,
              "title": "node3.1",
              "nodes": []
          }
        ],
    }, {
        "id": 4,
        "title": "node4",
        "nodes": [
          {
              "id": 41,
              "title": "node4.1",
              "nodes": []
          }
        ],
    }];
}
    );
