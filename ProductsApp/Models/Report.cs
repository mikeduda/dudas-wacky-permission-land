﻿
namespace ReportsApp.Models
{
    public class ReportElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ReportContent
    {
        public string GenerateTime { get; set; }
        public string Html { get; set; }
    }
}