﻿using ReportsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace ReportsApp.Controllers
{
    public class ReportsController : ApiController
    {
        public ReportContent GetAllReports()
        {
            return RedisClient.GetCompleteHtml(2);
        }

        public ReportContent GetReport(int id)
        {
            return RedisClient.GetCompleteHtml(id);
        }
    }
}
