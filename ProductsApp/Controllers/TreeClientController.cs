﻿using PermissionAggregator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace BooksAPI.Controllers
{
    [RoutePrefix("api/treeclient")]
    public class TreeClientController : ApiController
    {
        private HttpClient httpClient = null;

        private HttpClient Client
        {
            get
            {
                if (httpClient == null)
                {
                    httpClient = new HttpClient();
                    httpClient.BaseAddress = new Uri("http://localhost:9001/");
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/bson"));
                }

                return httpClient;
            }
        }

        [Route("count")]
        [HttpGet]
        public string Count()
        {
            DateTime now = DateTime.Now;
            string permissions = String.Empty;

            var response = Client.GetAsync("api/treeserver/count").Result;

            if (response.IsSuccessStatusCode)
            {
                permissions = response.Content.ReadAsAsync<string>().Result;
            }

            var time = DateTime.Now.Subtract(now).TotalSeconds;

            return String.Format("Count: {0} in {1} seconds. ", permissions, time);
        }

        [Route("rebuildroles")]
        [HttpGet]
        public string RebuildRoles()
        {
            DateTime now = DateTime.Now;
            string permissions = String.Empty;

            var response = Client.GetAsync("api/treeserver/rebuildroles").Result;

            if (response.IsSuccessStatusCode)
            {
                permissions = response.Content.ReadAsAsync<string>().Result;
            }

            var time = DateTime.Now.Subtract(now).TotalSeconds;

            return String.Format("RebuildRoles in {0} seconds. ", time);
        }

        [Route("rebuildtree")]
        [HttpGet]
        public string RebuildTree()
        {
            DateTime now = DateTime.Now;
            string permissions = String.Empty;

            var response = Client.GetAsync("api/treeserver/rebuildtree").Result;

            if (response.IsSuccessStatusCode)
            {
                permissions = response.Content.ReadAsAsync<string>().Result;
            }

            var time = DateTime.Now.Subtract(now).TotalSeconds;

            return String.Format("RebuildTree in {0} seconds. ", time);
        }

        [Route("generate")]
        [HttpGet]
        public string Generate()
        {
            DateTime now = DateTime.Now;
            string permissions = String.Empty;

            var response = Client.GetAsync("api/treeserver/generate").Result;

            if (response.IsSuccessStatusCode)
            {
                permissions = response.Content.ReadAsAsync<string>().Result;
            }

            var time = DateTime.Now.Subtract(now).TotalSeconds;

            return String.Format("Generated in {0} seconds. ", time);
        }

        [Route("getpermissions")]
        [HttpGet]
        public string GetPermissions()
        {
            DateTime now = DateTime.Now;
            List<NodePayload> permissions = new List<NodePayload>();

            var response = Client.GetAsync("api/treeserver/getpermissions").Result;

            MediaTypeFormatter[] formatters = new MediaTypeFormatter[] { new BsonMediaTypeFormatter() };

            if (response.IsSuccessStatusCode) permissions = response.Content.ReadAsAsync<List<NodePayload>>(formatters).Result;

            var time = DateTime.Now.Subtract(now).TotalSeconds;

            return String.Format("Generate {0} permissions in {1} seconds. ", permissions.ToString(), time);
        }

        // POST api/treeclient
        public void Post([FromBody]string value)
        {
        }

        // PUT api/treeclient/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/treeclient/5
        public void Delete(int id)
        {
        }

        public static async Task<string> SendTreeClientRequest()
        {
            string permissions = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync("api/treeserver/count").Result;

                if (response.IsSuccessStatusCode)
                {
                    permissions = await response.Content.ReadAsAsync<string>();
                }
            }

            return permissions;
        }

    }
}
