﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Description;
//using BooksAPI.Models;
//using BooksAPI.DTO;
//using System.Linq.Expressions;
//using System.Threading.Tasks;

//namespace BooksAPI.Controllers
//{
//    [RoutePrefix("api/books")]
//    public class BooksController : ApiController
//    {
//        private BooksAPIContext db = new BooksAPIContext();

//        private static readonly Expression<Func<Book, BookDetailDTO>> AsBookDto =
//           x => new BookDetailDTO
//           {
//               Title = x.Title,
//               Author = x.Author.Name,
//               Genre = x.Genre
//           };

//        [Route("")]
//        public IQueryable<BookDetailDTO> GetBooks()
//        {
//            return db.Books.Include(b => b.Author).Select(AsBookDto);
//        }

//        [Route("{id:int}")]
//        [ResponseType(typeof(BookDetailDTO))]
//        public async Task<IHttpActionResult> GetBook(int id)
//        {
//            BookDetailDTO book = await db.Books.Include(b => b.Author)
//                .Where(b => b.BookId == id)
//                .Select(AsBookDto)
//                .FirstOrDefaultAsync();
//            if (book == null)
//            {
//                return NotFound();
//            }

//            return Ok(book);
//        }

//        [Route("{id:int}/details")]
//        [ResponseType(typeof(BookDetailDTO))]
//        public async Task<IHttpActionResult> GetBookDetail(int id)
//        {
//            var book = await (from b in db.Books.Include(b => b.Author)
//                              where b.AuthorId == id
//                              select new BookDetailDTO
//                              {
//                                  Title = b.Title,
//                                  Genre = b.Genre,
//                                  PublishDate = b.PublishDate,
//                                  Price = b.Price,
//                                  Description = b.Description,
//                                  Author = b.Author.Name
//                              }).FirstOrDefaultAsync();

//            if (book == null)
//            {
//                return NotFound();
//            }
//            return Ok(book);
//        }

//        [Route("{genre}")]
//        public IQueryable<BookDetailDTO> GetBooksByGenre(string genre)
//        {
//            return db.Books.Include(b => b.Author)
//                .Where(b => b.Genre.Equals(genre, StringComparison.OrdinalIgnoreCase))
//                .Select(AsBookDto);
//        }

//        [Route("~api/authors/{authorId}/books")]
//        public IQueryable<BookDetailDTO> GetBooksByAuthor(int authorId)
//        {
//            return db.Books.Include(b => b.Author)
//                .Where(b => b.AuthorId == authorId)
//                .Select(AsBookDto);
//        }

//        [Route("date/{pubdate:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
//        [Route("date/{*pubdate:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
//        public IQueryable<BookDetailDTO> GetBooks(DateTime pubdate)
//        {
//            return db.Books.Include(b => b.Author)
//                .Where(b => DbFunctions.TruncateTime(b.PublishDate)
//                    == DbFunctions.TruncateTime(pubdate))
//                .Select(AsBookDto);
//        }

//        // PUT api/Books/5
//        public IHttpActionResult PutBook(int id, Book book)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }

//            if (id != book.BookId)
//            {
//                return BadRequest();
//            }

//            db.Entry(book).State = EntityState.Modified;

//            try
//            {
//                db.SaveChanges();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!BookExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return StatusCode(HttpStatusCode.NoContent);
//        }

//        // POST api/Books
//        [ResponseType(typeof(Book))]
//        public IHttpActionResult PostBook(Book book)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }

//            db.Books.Add(book);
//            db.SaveChanges();

//            return CreatedAtRoute("DefaultApi", new { id = book.BookId }, book);
//        }

//        // DELETE api/Books/5
//        [ResponseType(typeof(Book))]
//        public IHttpActionResult DeleteBook(int id)
//        {
//            Book book = db.Books.Find(id);
//            if (book == null)
//            {
//                return NotFound();
//            }

//            db.Books.Remove(book);
//            db.SaveChanges();

//            return Ok(book);
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        private bool BookExists(int id)
//        {
//            return db.Books.Count(e => e.BookId == id) > 0;
//        }
//    }
//}