﻿////------------------------------------------------------------------------------
//// <copyright file="CSSqlClassFile.cs" company="Microsoft">
////     Copyright (c) Microsoft Corporation.  All rights reserved.
//// </copyright>
////------------------------------------------------------------------------------
//using System;
//using System.Data;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Data.SqlTypes;
//using System.Collections;
//using Microsoft.SqlServer.Server;
//using Microsoft.SqlServer.Types;

//namespace PermissionAggregator
//{
//    public struct ApplicableRole
//    {
//        public ApplicableRole(SqlHierarchyId hid, ulong rank, Guid userId)
//        {
//            this.Rank = rank;
//            this.Hid = hid;
//            this.UserId = userId;
//        }

//        public ulong Rank;
//        public SqlHierarchyId Hid;
//        public Guid UserId;
//    }

//    public struct Membership
//    {
//        public Membership(ulong rank, Guid user)
//        {
//            this.Rank = rank;
//            this.User = user;
//        }

//        public ulong Rank;
//        public Guid User;
//    }

//    public struct Role
//    {
//        public Role(ulong rank, int itemtype, Guid stateId)
//        {
//            this.Rank = rank;
//            this.ItemTypeId = itemtype;
//            this.StateId = stateId;
//        }

//        public ulong Rank;
//        public int ItemTypeId;
//        public Guid StateId;

//    }
//    public static class Aggregator
//    {
//        private static Dictionary<ulong, List<Role>> Roles = new Dictionary<ulong, List<Role>>();
//        private static Dictionary<SqlHierarchyId, List<Membership>> Memberships = new Dictionary<SqlHierarchyId, List<Membership>>();
//        private static List<ApplicableRole> Results = new List<ApplicableRole>();

//        public static void Aggregate(string connString)
//        {
//            string connStr = connString;

//            using (SqlConnection conn = new SqlConnection(connStr))
//            {
//                conn.Open();
//                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                    SELECT DISTINCT rt.rank, rp.ItemTypeId, rp.StateId
//                    FROM tblRolePermissions AS rp 
//		                INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rp.RoleId 
//                    WHERE rp.Status = 1
//                    ORDER BY rt.rank"), conn))
//                {
//                    using (SqlDataReader reader = cmd2.ExecuteReader())
//                    {
//                        while (reader.Read())
//                        {
//                            Role role = new Role();
//                            role.Rank = (ulong)reader.GetInt64(0);
//                            role.ItemTypeId = reader.GetInt32(1);
//                            role.StateId = reader.IsDBNull(2) ? Guid.Empty : reader.GetGuid(2);

//                            if (Roles.ContainsKey(role.Rank)) Roles[role.Rank].Add(role);
//                            else
//                            {
//                                List<Role> roleTypes = new List<Role>();
//                                roleTypes.Add(role);
//                                Roles.Add(role.Rank, roleTypes);
//                            }
//                        }
//                    }
//                }

//                using (SqlCommand cmd = new SqlCommand(String.Format(@"
//                    SELECT  li.hid AS hid, ist.ItemTypeID AS itemtypeid, wi.CurrentState AS currentstate, rr.rank AS rolerank, rm.UserGroupID AS userid
//                    FROM	tblLinkedItems4_5 AS li 
//			            INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
//			            LEFT JOIN tblWorkflowInstances wi ON (wi.Status = 1 AND wi.ItemID = li.SubItemID)
//			            LEFT JOIN tblRoleMembership AS rm ON rm.TreeId = li.TreeID AND rm.Hid = li.HID AND rm.IsReal = 1 AND rm.Hid <> '/'
//			            LEFT JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
//                    WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND li.Hid <> '/'
//                    ORDER BY li.hid"), conn))
//                {
//                    using (SqlDataReader reader = cmd.ExecuteReader())
//                    {
//                        SqlHierarchyId currentHid = SqlHierarchyId.GetRoot();
//                        SqlHierarchyId hid;

//                        int itemType = 0;
//                        ulong rank = 0;
//                        Guid state = Guid.Empty;
//                        Guid user = Guid.Empty;
//                        Dictionary<int, int> itemTypes = new Dictionary<int, int>();
//                        List<Membership> rolesAtNode = new List<Membership>();
//                        // List<ApplicableRole> applicableRolesAtNode = new List<ApplicableRole>();

//                        //Initialize empty root memberships
//                        Memberships.Add(currentHid, new List<Membership>());

//                        while (reader.Read())
//                        {
//                            hid = (SqlHierarchyId)reader.GetValue(0);
//                            itemType = reader.GetInt32(1);
//                            rank = reader.IsDBNull(3) ? 0 : (ulong)reader.GetInt64(3);
//                            user = reader.IsDBNull(4) ? Guid.Empty : reader.GetGuid(4);

//                            //First row of new node - gather the node information - itemtype/state
//                            if (hid != currentHid)
//                            {
//                                #region Aggregate last node

//                                //If not initial root node
//                                if (currentHid != SqlHierarchyId.GetRoot())
//                                {
//                                    //Get parent permissions
//                                    List<Membership> combinedMemberships = new List<Membership>(Memberships[currentHid.GetAncestor(1)]);

//                                    //Combine current node applicable permission with parent applicable permissions for each user
//                                    combinedMemberships.AddRange(rolesAtNode);

//                                    //Aggregate at this node and save
//                                    Results.AddRange(Combine(hid, itemTypes, state, combinedMemberships));

//                                    //Cache for descendants
//                                    Memberships.Add(currentHid, combinedMemberships);
//                                }

//                                #endregion

//                                #region Generate details for next node

//                                rolesAtNode.Clear();
//                                //   applicableRolesAtNode.Clear();
//                                itemTypes.Clear();
//                                currentHid = hid;

//                                state = reader.IsDBNull(2) ? Guid.Empty : reader.GetGuid(2);
//                                if (!itemTypes.ContainsKey(itemType)) itemTypes.Add(itemType, 0);

//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));

//                                //  if (Roles[rank].Contains(new Role(rank, itemType, state)))
//                                //  {
//                                //       applicableRolesAtNode.Add(new ApplicableRole(hid, rank, user));
//                                //   }

//                                #endregion
//                            }
//                            else
//                            {
//                                if (!itemTypes.ContainsKey(itemType)) itemTypes.Add(itemType, 0);
//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));

//                                //  if (Roles[rank].Contains(new Role(rank, itemType, state)))
//                                ///  {
//                                //      applicableRolesAtNode.Add(new ApplicableRole(hid, rank, user));
//                                //  }
//                            }
//                        }
//                    }
//                }
//            };
//        }
//        private static IEnumerable<ApplicableRole> Combine(SqlHierarchyId hid, Dictionary<int, int> itemTypes, Guid state, List<Membership> combinedMemberships)
//        {
//            Dictionary<Guid, ApplicableRole> applied = new Dictionary<Guid, ApplicableRole>();
//            foreach (Membership membership in combinedMemberships)
//            {
//                foreach (Role role in Roles[membership.Rank])
//                {
//                    if (role.StateId == state && itemTypes.ContainsKey(role.ItemTypeId))
//                    {
//                        ApplicableRole applicableRole;
//                        if (applied.TryGetValue(membership.User, out applicableRole))
//                        {
//                            applicableRole.Rank &= (ulong)role.Rank;
//                        }
//                        else
//                        {
//                            applicableRole = new ApplicableRole(hid, role.Rank, membership.User);
//                            applied.Add(membership.User, applicableRole);
//                        }
//                    }
//                }
//            }

//            return applied.Values;
//        }
//    }
//}
