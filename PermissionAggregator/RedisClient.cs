﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;

namespace PermissionAggregator
{
    public class Args
    {
        public string Hid;
        public Guid ItemID;
        public long Rank;
        public string Values;
    }

    public class RedisClient
    {
        private static string NameKey = new Guid("BE31F120-C565-4AED-A9A9-7C7E657C5D1F").ToString();
        private static string TypeKey = new Guid("CFAA370A-4E10-4D5C-8474-E2F27A13AC52").ToString();
        private static string DescriptionKey = new Guid("10147C85-698F-4515-BB05-0AFFDDF4F20E").ToString();
        private static long Count;
        public static StringBuilder ReportString = new StringBuilder();

//        internal static ConnectionMultiplexer GetConnection(string host, int port, bool open = true, bool allowAdmin = false, bool waitForOpen = false, int syncTimeout = 5000, int ioTimeout = 5000)
//        {
//            var conn = new RedisConnection(host, port, syncTimeout: syncTimeout, ioTimeout: ioTimeout, allowAdmin: allowAdmin);
//            conn.Error += (s, args) =>
//            {
//                Trace.WriteLine(args.Exception.Message, args.Cause);
//            };
//            if (open)
//            {
//                var openAsync = conn.Open();
//                if (waitForOpen) conn.Wait(openAsync);
//            }
//            return conn;
//        }

//        internal static void Test()
//        {
//            using (var conn = GetConnection("127.0.0.1", 6379))
//            {
//                const double value = 634614442154715;
//                conn.SortedSets.Add(3, "zset", "abc", value);
//                var range = conn.SortedSets.Range(3, "zset", 0, -1);
//            }
//        }

//        internal static void LoadDataIntoRedis()
//        {
//            string connStr = "data source=QA-DB1;initial catalog=DUDADB5;integrated security=SSPI;MultipleActiveResultSets=true;";
//            using (SqlConnection dbConn = new SqlConnection(connStr))
//            {
//                dbConn.Open();

//                using (var redis = GetConnection("127.0.0.1", 6379, true, true))
//                {
//                    redis.Server.FlushAll();

//                    using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                        SELECT ice.itemid, ice.itemtypeid, ice.criteriaid, ice.ValueText
//                        FROM [dbo].[tblItemCriteriaEvaluations] AS ice
//		                    INNER JOIN tblCriteria AS c ON ice.CriteriaID = c.CriteriaID
//                        WHERE c.DisplayStyle <> 5 AND c.DisplayStyle <> 9"), dbConn))
//                    {
//                        using (SqlDataReader reader = cmd2.ExecuteReader())
//                        {
//                            while (reader.Read())
//                            {
//                                var itemId = reader.GetGuid(0);
//                                var typeId = reader.GetInt32(1);
//                                var propertyid = reader.GetGuid(2);
//                                var value = reader.GetString(3);

//                                redis.Hashes.Set(0, itemId.ToString(), typeId.ToString() + propertyid.ToString(), value);
//                            }
//                        }
//                    }

//                    using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                       SELECT i.itemid, i.Name, i.Description FROM tblItems AS i WHERE i.Status = 1"), dbConn))
//                    {
//                        using (SqlDataReader reader = cmd2.ExecuteReader())
//                        {
//                            while (reader.Read())
//                            {
//                                var itemId = reader.GetGuid(0);
//                                var name = reader.GetString(1);
//                                var description = reader.GetString(2);

//                                if (!String.IsNullOrEmpty(name)) redis.Hashes.Set(0, itemId.ToString(), NameKey, name);
//                                if (!String.IsNullOrEmpty(description)) redis.Hashes.Set(0, itemId.ToString(), DescriptionKey, description);
//                            }
//                        }
//                    }

//                    redis.Close(true);
//                }
//                dbConn.Close();
//            }
//        }

//        internal static void ReportIteration()
//        {
//            var startingHid = "/";
//            string connStr = "data source=QA-DB1;initial catalog=DUDADB5;integrated security=SSPI;MultipleActiveResultSets=true;";

//            using (SqlConnection dbConn = new SqlConnection(connStr))
//            {
//                dbConn.Open();

//                using (var redis = GetConnection("127.0.0.1", 6379))
//                {
//                    //  var info = redis.Server.GetInfo().Result;

//                    Queue<Args> links = new Queue<Args>();

//                    using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
//                      SELECT li.[HID].ToString(), li.SubItemID, ap.rank
//                      FROM [dbo].[tblLinkedItems4_5] AS li 
//		                    LEFT JOIN tblAppliedCachedPermissions AS ap ON ap.hid = li.HID
//                      WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND ap.userid = 'D0AB7B7E-8848-4264-B560-E3A347562E5A' AND ap.treeid = 0"), dbConn))
//                    {
//                        using (SqlDataReader reader = cmd3.ExecuteReader())
//                        {
//                            while (reader.Read())
//                            {
//                                var hid = reader.GetString(0);
//                                var itemId = reader.GetGuid(1);
//                                var rank = reader.GetInt64(2);

//                                var args = new Args() { ItemID = itemId, Hid = hid, Rank = rank };

//                                links.Enqueue(args);

//                                //     ReportString.Append(hid).Append(" - ").Append(itemId).Append(" Rank: ").Append(rank).Append(Environment.NewLine);

//                                var vals = redis.Hashes.GetValues(0, itemId.ToString());

//                                vals.ContinueWith((t, o) =>
//                                {
//                                    //Check filters and set up item
//                                    var arg = (Args)o;
//                                    //ReportString.Append(arg.Hid).Append(" - ").Append(arg.ItemID).Append(Environment.NewLine);

//                                    foreach (var val in t.Result)
//                                    {
//                                        arg.Values += Encoding.UTF8.GetString(val);
//                                        // ReportString.Append(Encoding.UTF8.GetString(val)).Append(Environment.NewLine);
//                                    }

//                                }, args);
//                            }
//                        }
//                    }

//                    //Pass second time over tree, applying tree rules etc


//                    redis.Close(true);
//                }
//                dbConn.Close();
//            }
//        }
    }
}
