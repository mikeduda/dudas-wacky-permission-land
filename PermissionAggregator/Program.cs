﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

//using DataBaseFunctions;

namespace PermissionAggregator
{
    class Program
    {
        static void Main(string[] args)
        {
            string db = "DUDADB5";
            if (args.Length > 0)
            {
                db = args[0];
            }

            string connStr = "data source=QA-DB1;initial catalog=" + db + ";integrated security=SSPI;MultipleActiveResultSets=true;";

           // RedisAggregator.Init();

            Dictionary<string, string> times = new Dictionary<string, string>();

            //DateTime now = DateTime.Now;
            ////depends on links/subtypes/workflowstates/tblRolePermissions
            //RedisAggregator.LoadTree(connStr);
            //var time = DateTime.Now.Subtract(now).TotalSeconds;
            //times.Add("LoadTree", time.ToString());

            //now = DateTime.Now;
            //RedisAggregator.LoadRawTree(connStr);
            //time = DateTime.Now.Subtract(now).TotalSeconds;
            //times.Add("LoadRaw", time.ToString());

            //now = DateTime.Now;
            //RedisAggregator.TraverseRawTree();
            //time = DateTime.Now.Subtract(now).TotalSeconds;
            //times.Add("Traverse", time.ToString());


            //now = DateTime.Now;
            ////depends on rolememberships/rolepermissions
            //RedisAggregator.LoadMemberships(connStr);
            //time = DateTime.Now.Subtract(now).TotalSeconds;
            //times.Add("LoadMemberships", time.ToString());

            //now = DateTime.Now;
            //RedisAggregator.Aggregate();
            //time = DateTime.Now.Subtract(now).TotalSeconds;
            //times.Add("Aggregate", time.ToString());

            DateTime now = DateTime.Now;
           // RedisAggregator.FlushRedis();
            RedisAggregator.AggregateAllInOne(connStr);
            var time = DateTime.Now.Subtract(now).TotalSeconds;
            times.Add("Aggregate", time.ToString());

            //  now = DateTime.Now;
            ////  var c =  RedisAggregator.PermissionCheck();
            //  time = DateTime.Now.Subtract(now).TotalSeconds;
            //  times.Add("Check", time.ToString());


            Console.Write(time);
            Console.ReadLine();
        }
    }
}
