﻿
USE [DUDADB5]
GO

/****** Object:  Table [dbo].[tblAppliedRootPermissions]    Script Date: 12/6/2013 1:39:50 PM ******/
DROP TABLE [dbo].[tblAppliedRootPermissions]
GO

/****** Object:  Table [dbo].[tblAppliedRootPermissions]    Script Date: 12/6/2013 1:39:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedRootPermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[treeid] [int] NOT NULL,
	[hid] [hierarchyid] NOT NULL,
	[userid] [uniqueidentifier] NOT NULL,
	[rank] [int] NOT NULL,
	[isroot] [bit] NOT NULL,
 CONSTRAINT [PK_AppliedRootPermissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [DUDADB5]
GO

/****** Object:  Table [dbo].[tblAppliedCachedPermissions]    Script Date: 12/6/2013 4:17:15 PM ******/
DROP TABLE [dbo].[tblAppliedCachedPermissions]
GO

/****** Object:  Table [dbo].[tblAppliedCachedPermissions]    Script Date: 12/6/2013 4:17:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedCachedPermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [uniqueidentifier] NOT NULL,
	[treeid] [int] NOT NULL,
	[hid] [hierarchyid] NOT NULL,
	[rank] [bigint] NOT NULL,
 CONSTRAINT [PK_AppliedCachedPermissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO





USE [DUDADB5]
GO

/****** Object:  Table [dbo].[tblAppliedProjectedPermissions]    Script Date: 12/6/2013 1:40:13 PM ******/
DROP TABLE [dbo].[tblAppliedProjectedPermissions]
GO

/****** Object:  Table [dbo].[tblAppliedProjectedPermissions]    Script Date: 12/6/2013 1:40:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblAppliedProjectedPermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [uniqueidentifier] NOT NULL,
	[hid] [hierarchyid] NOT NULL,
	[rank] [int] NOT NULL,
 CONSTRAINT [PK_AppliedPermissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


