﻿DROP PROCEDURE AggregateSP
GO

DROP FUNCTION [dbo].[Aggregate]
GO

DROP ASSEMBLY [UserDefinedFunctions] 
GO

CREATE ASSEMBLY [UserDefinedFunctions] from 'D:\ResolverDBDll\DatabaseFunctions.dll' WITH PERMISSION_SET = EXTERNAL_ACCESS 
GO

CREATE PROCEDURE AggregateSP AS EXTERNAL NAME UserDefinedFunctions.StoredProcedures.AggregatorSP 
GO

CREATE FUNCTION [dbo].[Aggregate] ()
RETURNS TABLE ([hid] nvarchar(250), [user] uniqueidentifier)
AS EXTERNAL NAME [UserDefinedFunctions].[PermissionFunctions].[Aggregate];

GO


--use master;
--create asymmetric key CLRExtensionKey
--from file = 'D:\Sample CLR\SampleCLRKey.snk'