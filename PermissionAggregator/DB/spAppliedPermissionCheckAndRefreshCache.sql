﻿USE [DUDADB5]
GO
/****** Object:  StoredProcedure [dbo].[spAppliedPermissionsCheckAndRefreshCache]    Script Date: 12/13/2013 5:22:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE  [dbo].[spAppliedPermissionsCheckAndRefreshCache] 
(
	@userId UNIQUEIDENTIFIER
)
AS
BEGIN

IF EXISTS(SELECT 1 FROM [dbo].[tblAppliedPermissionsDirtyState] WHERE userid = @userId AND isdirty = 1) 
BEGIN

	DECLARE @userrank INT = (SELECT r.rank FROM tblUsers As u 
							INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS r ON r.UserGroupID = u.UserGroupID 
							WHERE u.UserGroupID = @userId)

	DELETE FROM [dbo].[tblAppliedCachedPermissions] WHERE userid = @userid

	INSERT[dbo].[tblAppliedCachedPermissions]
		SELECT @userid, treeid, hid, [dbo].[BinaryOR](rank)
		FROM
		(SELECT treeid, hid, rank 
		FROM tblAppliedMaterializedPermissions WHERE userid = @userrank AND treeid = 0
		UNION ALL
		SELECT treeid, hid, rank 
		FROM tblAppliedProjectedPermissions WHERE userid = @userrank
		) AS j	
		GROUP BY treeid, hid

		UPDATE [dbo].[tblAppliedPermissionsDirtyState] SET isdirty = 0
		WHERE userid = @userId

RETURN 1
END

	RETURN 0 
END
