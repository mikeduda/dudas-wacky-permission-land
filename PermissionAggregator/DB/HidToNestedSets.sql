﻿DECLARE @Tree TABLE (name varchar(100), path varchar(100))
INSERT @Tree SELECT i.Name, li.hid.ToString() FROM tblLinkedItems4_5 AS li INNER JOIN tblItems AS i ON i.ItemID = li.SubItemID
			 WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND li.HID.IsDescendantOf('/7/') = 1

--SELECT (SELECT COUNT(*) FROM @Tree b WHERE SUBSTRING(a.path, 1, LEN(b.path)) = b.path)  +
--	   (SELECT COUNT(*) FROM @Tree b WHERE SUBSTRING(a.path, 1, LEN(b.path)) <> b.path AND b.path < a.path) * 2 AS lft,
--	   (SELECT COUNT(*) FROM @Tree) * 2 - 
--	   (SELECT COUNT(*) FROM @Tree b WHERE SUBSTRING(a.path, 1, LEN(b.path)) = b.path) - 
--	   (SELECT COUNT(*) FROM @Tree b WHERE SUBSTRING(b.path, 1, LEN(a.path)) > a.path) * 2 + 1 AS rgt , a.path 
--FROM @Tree a

IF object_id('tempdb..#Tree2') IS NULL 
BEGIN 
CREATE TABLE #Tree2 (name varchar(100), path hierarchyid primary key(path))
INSERT #Tree2 SELECT i.Name, li.hid FROM tblLinkedItems4_5 AS li INNER JOIN tblItems AS i ON i.ItemID = li.SubItemID
			 WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND li.HID.IsDescendantOf('/1/') = 1
END

DECLARE @TotalSize int = (SELECT COUNT(*) FROM #Tree2) * 2;
SELECT (SELECT COUNT(*) FROM #Tree2 b WHERE a.path.IsDescendantOf(b.path) = 1) +
	   (SELECT COUNT(*) FROM #Tree2 b WHERE a.path.IsDescendantOf(b.path) = 0 AND b.path < a.path) * 2 AS lft,		
	   @TotalSize  - 
	   (SELECT COUNT(*) FROM #Tree2 b WHERE a.path.IsDescendantOf(b.path) = 1) - 
	   (SELECT COUNT(*) FROM #Tree2 b WHERE b.path.GetLevel() = a.path.GetLevel() AND b.path > a.path) * 2 + 1 AS rgt , a.path.ToString()
FROM #Tree2 a

--DROP TABLE #Tree2
SELECT count(*) FROM tblLinkedItems4_5 as li where li.hid.IsDescendantOf('/1/1/1/2/') = 1 AND li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' -- 661
SELECT count(*) FROM tblLinkedItems4_5 as li where li.hid.IsDescendantOf('/1/1/1/') = 1 AND li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' -- 7815
SELECT count(*) FROM tblLinkedItems4_5 as li where li.hid.IsDescendantOf('/1/1/') = 1 AND li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' -- 49265

SELECT li.NodeLevel, rm.hid.ToString(), rm.RoleId AS roleid, rm.UserGroupID AS userid FROM tblRoleMembership AS rm 
inner join tblLinkedItems4_5 as li on rm.hid = li.hid and rm.TreeId = li.TreeID
WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1


select hid.ToString(),NodeLevel from tblLinkedItems4_5 as li where li.HID.IsDescendantOf('/1/1/4/') = 1 AND li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 