﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spPermissionAggregationInitialLoad]
AS
BEGIN
		
SET NOCOUNT ON;

IF object_id('tempdb..#ROLETYPES') IS NOT NULL BEGIN DROP TABLE #ROLETYPES END	
CREATE TABLE #ROLETYPES (roleid uniqueidentifier,itemtype int, ismaintype bit, [rank] int primary key(roleid, itemtype, ismaintype))

IF object_id('tempdb..#membership') IS NOT NULL BEGIN DROP TABLE #membership END
CREATE TABLE #membership (treeId uniqueidentifier not null, hid hierarchyid not null, roleId uniqueidentifier not null,
			itemtypeid int not null, ismaintype bit, userId uniqueidentifier not null,rank int primary key(treeid, hid, roleId,itemtypeid,ismaintype, userId))	
	
--Select Links
--CASE WHEN(li.treeid = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid,

SELECT  0 AS TreeID, li.HID.ToString() AS hid, li.parentHID.ToString() AS phid,
		li.SubItemID AS itemid, ist.ItemTypeID AS itemtypeid, wi.CurrentState AS workflowstate 
FROM	tblLinkedItems4_5 AS li 
			INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
			LEFT JOIN tblWorkflowInstances wi ON (wi.Status = 1 AND wi.ItemID = li.SubItemID)
WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
ORDER BY treeid, NodeLevel, hid, itemtypeid

--Select membership
INSERT INTO #ROLETYPES
	SELECT DISTINCT r.RoleId, CASE WHEN (it.ParentItemTypeID IS NULL) THEN it.ItemTypeId ELSE it.ParentItemTypeID END, 
					CASE WHEN (it.ParentItemTypeID IS NULL) THEN 1 ELSE 0 END, rolerank.rank
	FROM tblRoles AS r 
						INNER JOIN tblRolePermissions AS rp ON rp.RoleId = r.RoleId
						INNER JOIN tblItemTypes AS it ON it.ItemTypeID = rp.ItemTypeId
						INNER JOIN(SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rolerank 
										ON rolerank.RoleId = r.RoleId
						
INSERT INTO #membership SELECT DISTINCT rm.TreeId, rm.hid, rm.RoleId, rt.itemtype, rt.ismaintype, rm.UserGroupID, rt.rank
							FROM tblRoleMembership AS rm INNER JOIN #ROLETYPES AS rt ON rm.RoleId = rt.roleid
							WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1-- AND rm.Hid <> '/'

--Select 'IsReal' memberships
SELECT CASE WHEN(rm.treeid = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, rm.hid.ToString(), 
		rm.RoleId AS roleid, rt.itemtype,rt.ismaintype, rm.UserGroupID AS userid, rt.rank
							FROM tblRoleMembership AS rm INNER JOIN #ROLETYPES AS rt ON rm.RoleId = rt.roleid
							WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1-- AND rm.Hid <> '/'
							
--Select ancestor memberships				                                    
SELECT CASE WHEN(rm.treeid = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END AS treeid, a.ancestor.ToString() as hid,
		rm.RoleId AS roleid, rm.itemtypeid, rm.ismaintype, rm.userId AS userid, rm.rank
						FROM #membership AS rm CROSS APPLY dbo.fnGetAncestors(rm.hid) as a
						INNER JOIN tblLinkedItems4_5 as li ON li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND li.HID = a.ancestor AND li.itemTypeID = rm.itemtypeid
						WHERE rm.hid <> a.ancestor

--Select dictionary of role mappings to itemtypes/states
SELECT DISTINCT rp.RoleId,rp.ItemTypeId,rp.StateId FROM tblRolePermissions AS rp
WHERE rp.Status = 1

--Select role permissions
--SELECT rp.RoleId,rp.ItemTypeId,rp.PropertyId,rp.PropertyType,rp.PermissionFlags,rp.StateId 
--FROM tblRolePermissions AS rp WHERE rp.Status = 1

END

