﻿SELECT DISTINCT rp.RoleId AS roleid, rt.rank, rp.ItemTypeId, rp.StateId
				FROM tblRolePermissions AS rp 
					INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rp.RoleId 
WHERE rp.Status = 1
ORDER BY roleid						
					
SELECT  0 AS TreeID, li.hid AS hid, li.SubItemID AS itemid, ist.ItemTypeID AS itemtypeid, wi.CurrentState AS currentstate, rr.rank AS rolerank, rm.UserGroupID AS userid
FROM	tblLinkedItems4_5 AS li 
			INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
			LEFT JOIN tblWorkflowInstances wi ON (wi.Status = 1 AND wi.ItemID = li.SubItemID)
				LEFT JOIN tblRoleMembership AS rm ON rm.TreeId = li.TreeID AND rm.Hid = li.HID AND rm.IsReal = 1 AND rm.Hid <> '/'
				LEFT JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
ORDER BY li.TreeID, li.hid	

INSERT AppliedRootPermissions
SELECT CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, rm.UserGroupID, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank)), 1
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rm.RoleId 
WHERE rm.IsReal = 1 AND rm.hid = '/'
GROUP BY rm.TreeId, rm.Hid, rm.UserGroupID

SELECT * FROM tblRoleMembership as rm 
INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rm.RoleId 
where rm.hid = '/' AND rm.IsReal = 1


--Generate isreal = 0 permissions
INSERT tblAppliedRootPermissions
SELECT CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, rm.UserGroupID, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank)), 0
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rm.RoleId 
				INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = rm.ItemID 
				INNER JOIN tblRolePermissions AS rp ON rp.RoleId = rm.RoleId AND ist.ItemTypeID = rp.ItemTypeId	
				LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemId = rm.ItemID AND wi.CurrentState = rp.StateId
WHERE rm.IsReal = 0 AND ((wi.CurrentState = rp.StateId) OR (wi.CurrentState IS NULL AND  rp.StateId IS NULL)) 
GROUP BY rm.TreeId, rm.Hid, rm.UserGroupID



SELECT * FROM	tblAppliedRootPermissions AS arp
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON arp.rank & POWER(CONVERT(BIGINT, 2), rt.rank) = POWER(CONVERT(BIGINT, 2), rt.rank) 
WHERE arp.isroot = 1 AND arp.userid = '5ECD662F-2060-4621-8C74-00B96698CF74'


DECLARE @t1 DATETIME;
DECLARE @t2 DATETIME;

DECLARE @userid UNIQUEIDENTIFIER = 'FF74065B-332A-491B-8778-02670937AEC9'
DECLARE users cursor for (SELECT TOP(100) usergroupid FROM tblUsers WHERE Status = 1)
OPEN users
FETCH NEXT FROM users INTO @userid
WHILE @@FETCH_STATUS = 0
begin	
	--	SET @t1 = GETDATE();
				
		INSERT [dbo].[tblAppliedCachedPermissions]
		SELECT @userid, treeid, hid, [dbo].[BinaryOR](rank)
		FROM
		(SELECT treeid, hid, rank 
		FROM tblAppliedRootPermissions WHERE isroot = 0 AND userid = @userid AND treeid = 0

		UNION ALL

		SELECT treeid, hid, rank 
		FROM tblAppliedProjectedPermissions WHERE userid = @userid

		UNION ALL

		SELECT 0 AS treeid, hid, ap.rank
		FROM tblLinkedItems4_5 AS li
			INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = li.SubItemID
			LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemId = li.SubItemID
			INNER JOIN (SELECT DISTINCT arp.treeid, rp.ItemTypeId, rp.StateId, rt.rank
						FROM tblAppliedRootPermissions AS arp
							INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON arp.rank & POWER(CONVERT(BIGINT, 2), rt.rank) = POWER(CONVERT(BIGINT, 2), rt.rank) 
							INNER JOIN tblRolePermissions AS rp ON rp.RoleId = rt.RoleId			
						WHERE arp.isroot = 1 AND arp.userid = @userid AND arp.treeid = 0) AS ap 
								ON ist.ItemTypeID = ap.ItemTypeId AND (wi.CurrentState = ap.StateId OR (wi.CurrentState IS NULL AND ap.StateId IS NULL))) AS j
		GROUP BY treeid, hid

		--SET @t2 = GETDATE();
		--SELECT DATEDIFF(millisecond,@t1,@t2) AS elapsed_ms;		

fetch NEXT from users into @userid
end
close users
deallocate users


DBCC dropcleanbuffers
DBCC freeproccache
SET STATISTICS IO ON
SET STATISTICS TIME ON

SET STATISTICS IO OFF
SET STATISTICS TIME OFF

DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
SELECT DISTINCT rt.rank, l.rank AS lookupType, CASE WHEN (it.ParentItemTypeID IS NULL) THEN it.ItemTypeId ELSE it.ParentItemTypeID END AS maintype
FROM tblRolePermissions AS rp
    INNER JOIN tblItemTypes AS it ON rp.ItemTypeId = it.ItemTypeId
	INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rp.RoleId 
	INNER JOIN (SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
			(SELECT ItemTypeId FROM tblItemTypes) AS it 
			CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as uniqueidentifier)) AS w) AS l 
					ON l.ItemTypeID = rp.ItemTypeId AND (l.Id = rp.StateId OR rp.StateId IS NULL AND l.Id = @EMPTY)
WHERE rp.Status = 1 AND it.Status = 1
ORDER BY rank 

--SELECT count(*) FROM tblAppliedProjectedPermissions
--Combine all permissions

--Generate root permissions
INSERT tblAppliedRootPermissions
SELECT 1 AS isroot, CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank))
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rm.RoleId
				INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON rm.UserGroupID = u.UserGroupID
WHERE rm.IsReal = 1 AND rm.hid = '/'
GROUP BY rm.TreeId, rm.Hid, u.rank

INSERT tblAppliedRootPermissions
SELECT 1 AS isroot, CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank))
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles) AS rt ON rt.RoleId = rm.RoleId
				INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupID = rm.UserGroupID
				INNER JOIN tblGroups AS g ON ugo.UserGroupID = g.UserGroupID
				INNER JOIN tblUserGroupMembership AS ugm ON ugm.UserGroupID = g.UserGroupID
				INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON ugm.MemberID = u.UserGroupID		
WHERE rm.IsReal = 1 AND rm.hid = '/'
GROUP BY rm.TreeId, rm.Hid, u.rank


--Generate isreal = 0 permissions
INSERT tblAppliedRootPermissions
SELECT CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank))
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rm.RoleId 
				INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = rm.ItemID 
				INNER JOIN tblRolePermissions AS rp ON rp.RoleId = rm.RoleId AND ist.ItemTypeID = rp.ItemTypeId	
				INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON rm.UserGroupID = u.UserGroupID
				LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemId = rm.ItemID AND wi.CurrentState = rp.StateId
WHERE rm.IsReal = 0 AND ((wi.CurrentState = rp.StateId) OR (wi.CurrentState IS NULL AND  rp.StateId IS NULL)) 
GROUP BY rm.TreeId, rm.Hid, u.rank

--Isreal 0 for groups
INSERT tblAppliedRootPermissions
SELECT  CASE WHEN (rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A') THEN 0 ELSE 1 END, rm.Hid, u.rank AS userid, [dbo].[BinaryOR](POWER(CONVERT(BIGINT, 2), rt.rank))
FROM tblRoleMembership AS rm 
				INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rm.RoleId 
				INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = rm.ItemID 
				INNER JOIN tblRolePermissions AS rp ON rp.RoleId = rm.RoleId AND ist.ItemTypeID = rp.ItemTypeId	
				INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupID = rm.UserGroupID
				INNER JOIN tblGroups AS g ON ugo.UserGroupID = g.UserGroupID
				INNER JOIN tblUserGroupMembership AS ugm ON ugm.UserGroupID = g.UserGroupID
				INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON ugm.MemberID = u.UserGroupID				
				LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemId = rm.ItemID AND wi.CurrentState = rp.StateId
WHERE rm.IsReal = 0 AND ((wi.CurrentState = rp.StateId) OR (wi.CurrentState IS NULL AND  rp.StateId IS NULL)) 
GROUP BY rm.TreeId, rm.Hid, u.rank


--Combine per user

SELECT treeid, hid, [dbo].[BinaryOR](rank)
FROM
(SELECT treeid, hid, rank 
FROM tblAppliedRootPermissions WHERE isroot = 0 AND userid = '5ECD662F-2060-4621-8C74-00B96698CF74'  
UNION ALL
SELECT treeid, hid, rank 
FROM tblAppliedProjectedPermissions WHERE userid = '5ECD662F-2060-4621-8C74-00B96698CF74' 
UNION ALL
SELECT 0 AS treeid, hid, ap.rank
FROM tblLinkedItems4_5 AS li
	INNER JOIN tblItemSubTypes AS ist ON ist.ItemID = li.SubItemID
	LEFT JOIN tblWorkflowInstances AS wi ON wi.ItemId = li.SubItemID
	INNER JOIN (SELECT DISTINCT arp.treeid, rp.ItemTypeId, rp.StateId, rt.rank
				FROM tblAppliedRootPermissions AS arp
					INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON arp.rank & POWER(CONVERT(BIGINT, 2), rt.rank) = POWER(CONVERT(BIGINT, 2), rt.rank) 
					INNER JOIN tblRolePermissions AS rp ON rp.RoleId = rt.RoleId			
				WHERE arp.isroot = 1 AND arp.userid = '5ECD662F-2060-4621-8C74-00B96698CF74') AS ap 
						ON ist.ItemTypeID = ap.ItemTypeId AND (wi.CurrentState = ap.StateId OR (wi.CurrentState IS NULL AND ap.StateId IS NULL))) AS j
GROUP BY treeid, hid

SELECT distinct userid FROM tblAppliedCachedPermissions
select count(*) from tblAppliedProjectedPermissions


--FF74065B-332A-491B-8778-02670937AEC9
SELECT userid, COUNT(userid)  FROM tblAppliedCachedPermissions
GROUP BY userid 
ORDER BY COUNT(userid) DESC
	

DELETE FROM tblAppliedCachedPermissions
DELETE FROM [dbo].[tblAppliedProjectedPermissions]
DELETE FROM [dbo].[tblAppliedRootPermissions]


SELECT COUNT(*) FROM [dbo].[tblAppliedProjectedPermissions]
SELECT TOP (1000) * FROM [dbo].[tblAppliedProjectedPermissions]


DECLARE @t1 DATETIME;
DECLARE @t2 DATETIME;




DECLARE @userrank INT = (SELECT r.rank FROM tblUsers As u INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS r ON r.UserGroupID = u.UserGroupID WHERE u.UserGroupID = 'D0AB7B7E-8848-4264-B560-E3A347562E5A')
DECLARE @userid UNIQUEIDENTIFIER = (SELECT u.UserGroupID FROM tblUsers As u INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS r ON r.UserGroupID = u.UserGroupID WHERE u.UserGroupID = 'D0AB7B7E-8848-4264-B560-E3A347562E5A')

DELETE FROM [dbo].[tblAppliedCachedPermissions] WHERE userid = @userid

INSERT[dbo].[tblAppliedCachedPermissions]
SELECT @userid, treeid, hid, [dbo].[BinaryOR](rank)
FROM
(SELECT treeid, hid, rank 
FROM tblAppliedRootPermissions WHERE userid = @userrank AND treeid = 0
UNION ALL
SELECT treeid, hid, rank 
FROM tblAppliedProjectedPermissions WHERE userid = @userrank
) AS j	
GROUP BY treeid, hid


---------------

DECLARE @userid INT 
DECLARE users cursor for (SELECT usergroupid FROM tblUsers WHERE Status = 1)
OPEN users
FETCH NEXT FROM users INTO @userid
WHILE @@FETCH_STATUS = 0
begin	
		INSERT [dbo].[tblAppliedCachedPermissions]
		SELECT @userid, treeid, hid, [dbo].[BinaryOR](rank)
		FROM
		(SELECT treeid, hid, rank 
		FROM tblAppliedRootPermissions WHERE isroot = 0 AND userid = @userid AND treeid = 0
		UNION ALL
		SELECT treeid, hid, rank 
		FROM tblAppliedProjectedPermissions WHERE userid = @userid
		) AS j	
		GROUP BY treeid, hid
fetch NEXT from users into @userid
end
close users
deallocate users


ALTER DATABASE DUDADB5 SET RECOVERY BULK_LOGGED;



DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))

                    IF object_id('tempdb..#TYPELOOKUP') IS NOT NULL BEGIN DROP TABLE #TYPELOOKUP END	
                    CREATE TABLE #TYPELOOKUP (type int, itemType int, state UNIQUEIDENTIFIER)

                    INSERT #TYPELOOKUP
                    SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
                    (SELECT ItemTypeId FROM tblItemTypes WHERE Status = 1) AS it 
                    CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as UNIQUEIDENTIFIER)) AS w

                    IF object_id('tempdb..#ITEMSTATE') IS NOT NULL BEGIN DROP TABLE #ITEMSTATE END	
                    CREATE TABLE #ITEMSTATE (itemid UNIQUEIDENTIFIER, maintype int, state UNIQUEIDENTIFIER)

                    INSERT #ITEMSTATE
                    SELECT i.itemId, i.ItemTypeId, CASE WHEN(wi.CurrentState IS NULL) THEN @EMPTY ELSE wi.CurrentState END
                    FROM tblItems AS i LEFT JOIN tblWorkflowInstances AS wi ON i.ItemID = wi.ItemId AND wi.Status = 1

                    IF object_id('tempdb..#ROLES') IS NOT NULL BEGIN DROP TABLE #ROLES END	
                    CREATE TABLE #ROLES (roleid UNIQUEIDENTIFIER, rank bigint)
                    INSERT #ROLES
                    SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles

                    IF object_id('tempdb..#MEMBERS') IS NOT NULL BEGIN DROP TABLE #MEMBERS END	
                    CREATE TABLE #MEMBERS (hid HIERARCHYID, userid INT, roleid UNIQUEIDENTIFIER)

                    INSERT #MEMBERS
                    SELECT rm.hid, u.rank AS userid, rm.RoleId 
                    FROM tblRoleMembership AS rm 
							INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON rm.UserGroupID = u.UserGroupID
					WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 -- AND rm.hid <> '/'

					INSERT #MEMBERS
                    SELECT DISTINCT rm.hid, u.rank AS userid, rm.RoleId
					FROM tblRoleMembership AS rm 
								INNER JOIN tblUserGroupObjects AS ugo ON ugo.UserGroupID = rm.UserGroupID
								INNER JOIN tblGroups AS g ON ugo.UserGroupID = g.UserGroupID
								INNER JOIN tblUserGroupMembership AS ugm ON ugm.UserGroupID = g.UserGroupID
								INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblusers) AS u ON ugm.MemberID = u.UserGroupID
					WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 -- AND rm.hid <> '/'

                    SELECT  li.hid AS hid, wi.maintype AS maintype, rr.rank AS rolerank, rm.userid AS userid, tl.type
                    FROM	tblLinkedItems4_5 AS li WITH(NOLOCK)
			                    INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
			                        LEFT JOIN #ITEMSTATE AS wi ON (wi.ItemID = li.SubItemID)
				                    LEFT JOIN #MEMBERS AS rm ON rm.Hid = li.HID
				                    LEFT JOIN #ROLES AS rr ON rr.RoleId = rm.RoleId
				                    LEFT JOIN #TYPELOOKUP AS tl ON tl.itemType = ist.ItemTypeID AND (tl.state = wi.state)
                    WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 
                    ORDER BY li.hid





 IF object_id('tempdb..#DISTINCTROOTNODES') IS NOT NULL BEGIN DROP TABLE #DISTINCTROOTNODES END	
 CREATE TABLE #DISTINCTROOTNODES (groupid UNIQUEIDENTIFIER)

 INSERT #DISTINCTROOTNODES
  select distinct rm.RoleId from tblRoleMembership as rm
  where rm.IsReal = 1 and rm.Hid = '/' and rm.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 

  --Hidden groups
  INSERT [dbo].[tblUserGroupObjects]
  SELECT  groupid, 'Hidden' + CONVERT(varchar(256), newid()), 2, GETDATE(), 1,0,0,'en-US',GETDATE(),GETDATE(), 0, 0, 1
  FROM #DISTINCTROOTNODES
  
  INSERT [dbo].[tblGroups]
  SELECT  groupid, '', 6, GETDATE(),GETDATE()
  FROM #DISTINCTROOTNODES

  --Hidden members
  INSERT [dbo].[tblUserGroupMembership]

	select distinct u.groupid, UserGroupID, GETDATE(),GETDATE()
	from tblRoleMembership as rm 
		inner join #DISTINCTROOTNODES as u on u.groupid = rm.RoleId
	where rm.IsReal = 1 and rm.Hid = '/' and rm.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 


  --Hidden memberships
 INSERT [dbo].[tblRoleMembership]
   select distinct groupid, groupid, '2E3D1C02-C154-47EE-A391-2C5480C5148A' ,'/', 1, GETDATE(),GETDATE(), 1, '2E3D1C02-C154-47EE-A391-2C5480C5148A', 1
   FROM #DISTINCTROOTNODES

