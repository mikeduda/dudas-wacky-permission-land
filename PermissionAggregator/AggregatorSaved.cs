﻿////------------------------------------------------------------------------------
//// <copyright file="CSSqlClassFile.cs" company="Microsoft">
////     Copyright (c) Microsoft Corporation.  All rights reserved.
//// </copyright>
////------------------------------------------------------------------------------
//using System;
//using System.Linq;
//using System.Data;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Data.SqlTypes;
//using System.Collections;
//using Microsoft.SqlServer.Server;
//using Microsoft.SqlServer.Types;
//using System.Threading.Tasks;
//using System.Collections.Concurrent;
//using System.Numerics;

//namespace PermissionAggregator
//{
//    public struct ApplicableRole
//    {
//        public ApplicableRole(string hid, ulong rank, Guid userId)
//        {
//            this.Rank = rank;
//            this.Hid = hid;
//            this.UserId = userId;
//        }

//        public ulong Rank;
//        public string Hid;
//        public Guid UserId;
//    }

//    public struct Membership
//    {
//        public Membership(ulong rank, Guid user)
//        {
//            this.Rank = rank;
//            this.User = user;
//        }

//        public ulong Rank;
//        public Guid User;
//    }

//    public struct Role
//    {
//        public Role(ulong rank, int itemtype, Guid stateId, int lookupType)
//        {
//            this.Rank = rank;
//            this.LookupType = lookupType;
//            this.ItemTypeId = itemtype;
//            this.StateId = stateId;
//        }

//        public ulong Rank;
//        public int LookupType;
//        public int ItemTypeId;
//        public Guid StateId;

//    }

//    public struct MembershipSet
//    {
//        public MembershipSet(string hid, List<Membership> memberships)
//        {
//            this.Hid = hid;
//            this.Memberships = memberships;
//        }

//        public string Hid;
//        public List<Membership> Memberships;
//    }

//    public static class Aggregator
//    {
//        private static object _ThreadCountLock = new object();
//        private static int _ThreadCount;
//        private static int ThreadCount
//        {
//            get { return _ThreadCount; }
//            set
//            {
//                lock (_ThreadCountLock)
//                {
//                    _ThreadCount = value;
//                }
//            }
//        }

//        private static Dictionary<ulong, Dictionary<int, Dictionary<int, int>>> Roles = new Dictionary<ulong, Dictionary<int, Dictionary<int, int>>>();
//        private static Stack<MembershipSet> Memberships = new Stack<MembershipSet>();
//        private static List<ApplicableRole> ResultsSync = new List<ApplicableRole>();
//        private static ConcurrentBag<ApplicableRole> ResultsParallel = new ConcurrentBag<ApplicableRole>();
//        private static List<Task> Tasks = new List<Task>();
//        private static SqlConnection Connection;
//        private static SqlConnection Connection2;
//        private static DataTable TableResults = Aggregator.CreateTable();

//        public static void Aggregate(string connString)
//        {
//            string connStr = connString;

//            //    using (Connection2 = new SqlConnection(connStr))
//            //   {
//            //        Connection2.Open();

//            using (Connection = new SqlConnection(connStr))
//            {

//                #region Setup Roles cache

//                Connection.Open();
//                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
//                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
//                    SELECT DISTINCT rt.rank, l.rank AS lookupType, CASE WHEN (it.ParentItemTypeID IS NULL) THEN it.ItemTypeId ELSE it.ParentItemTypeID END AS maintype
//                    FROM tblRolePermissions AS rp
//                        INNER JOIN tblItemTypes AS it ON rp.ItemTypeId = it.ItemTypeId
//	                    INNER JOIN (SELECT RANK() OVER (ORDER BY RoleId) AS rank, RoleId FROM tblRoles WHERE Status = 1) AS rt ON rt.RoleId = rp.RoleId 
//	                    INNER JOIN (SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
//			                    (SELECT ItemTypeId FROM tblItemTypes WHERE Status = 1) AS it 
//			                    CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as uniqueidentifier)) AS w) AS l 
//					                    ON l.ItemTypeID = rp.ItemTypeId AND (l.Id = rp.StateId OR rp.StateId IS NULL AND l.Id = @EMPTY)
//                    WHERE rp.Status = 1 AND it.Status = 1
//                    ORDER BY rank  "), Connection))
//                {
//                    using (SqlDataReader reader = cmd2.ExecuteReader())
//                    {
//                        while (reader.Read())
//                        {
//                            var rankId = (ulong)reader.GetInt64(0);
//                            ulong rank = Convert.ToUInt64(Math.Pow(2, (double)rankId));

//                            var lookupType = (int)reader.GetInt64(1);
//                            var mainType = (int)reader.GetInt32(2);

//                            if (Roles.ContainsKey(rank))
//                            {
//                                var rankTypes = Roles[rank];
//                                if (rankTypes.ContainsKey(mainType))
//                                {
//                                    var lookups = rankTypes[mainType];
//                                    lookups.Add(lookupType, 0);
//                                }
//                                else
//                                {
//                                    var lookups = new Dictionary<int, int>();
//                                    lookups.Add(lookupType, 0);

//                                    rankTypes.Add(mainType, lookups);
//                                }
//                            }
//                            else
//                            {
//                                var lookups = new Dictionary<int, int>();
//                                lookups.Add(lookupType, 0);

//                                var rankTypes = new Dictionary<int, Dictionary<int, int>>();
//                                rankTypes.Add(mainType, lookups);

//                                Roles.Add(rank, rankTypes);
//                            }
//                        }
//                    }
//                }

//                #endregion

//                #region Aggregate permissions

//                using (SqlCommand cmd = new SqlCommand(String.Format(@"
//                   DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
//
//                    IF object_id('tempdb..#TYPELOOKUP') IS NOT NULL BEGIN DROP TABLE #TYPELOOKUP END	
//                    CREATE TABLE #TYPELOOKUP (type int, itemType int, state UNIQUEIDENTIFIER)
//
//                    INSERT #TYPELOOKUP
//                    SELECT RANK() OVER (ORDER BY itemTypeId, Id) AS rank, * FROM
//                    (SELECT ItemTypeId FROM tblItemTypes WHERE Status = 1) AS it 
//                    CROSS JOIN (SELECT Id FROM tblWorkflowStates WHERE Status = 1 UNION SELECT cast(cast(0 as binary) as UNIQUEIDENTIFIER)) AS w
//
//                    IF object_id('tempdb..#ITEMSTATE') IS NOT NULL BEGIN DROP TABLE #ITEMSTATE END	
//                    CREATE TABLE #ITEMSTATE (itemid UNIQUEIDENTIFIER, maintype int, state UNIQUEIDENTIFIER)
//
//                    INSERT #ITEMSTATE
//                    SELECT i.itemId, i.ItemTypeId, CASE WHEN(wi.CurrentState IS NULL) THEN @EMPTY ELSE wi.CurrentState END
//                    FROM tblItems AS i LEFT JOIN tblWorkflowInstances AS wi ON i.ItemID = wi.ItemId AND wi.Status = 1
//
//                    IF object_id('tempdb..#ROLES') IS NOT NULL BEGIN DROP TABLE #ROLES END	
//                    CREATE TABLE #ROLES (roleid UNIQUEIDENTIFIER, rank bigint)
//                    INSERT #ROLES
//                    SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles
//
//                    IF object_id('tempdb..#MEMBERS') IS NOT NULL BEGIN DROP TABLE #MEMBERS END	
//                    CREATE TABLE #MEMBERS (hid HIERARCHYID, userid UNIQUEIDENTIFIER, roleid UNIQUEIDENTIFIER)
//
//                    INSERT #MEMBERS
//                    SELECT rm.hid, rm.UserGroupID, rm.RoleId 
//                    FROM tblRoleMembership AS rm 
//                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid <> '/'
//
//                    SELECT  li.hid AS hid, wi.maintype AS maintype, rr.rank AS rolerank, rm.userid AS userid, tl.type--, li.Hid.GetAncestor(1).ToString()
//                    FROM	tblLinkedItems4_5 AS li WITH(NOLOCK)
//			                    INNER JOIN tblItemSubTypes ist ON ist.ItemID=li.SubItemID
//			                        LEFT JOIN #ITEMSTATE AS wi ON (wi.ItemID = li.SubItemID)
//				                    LEFT JOIN #MEMBERS AS rm ON rm.Hid = li.HID
//				                    LEFT JOIN #ROLES AS rr ON rr.RoleId = rm.RoleId
//				                    LEFT JOIN #TYPELOOKUP AS tl ON tl.itemType = ist.ItemTypeID AND (tl.state = wi.state)
//                    WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A' 
//                    ORDER BY li.hid"), Connection))
//                {
//                    using (SqlDataReader reader = cmd.ExecuteReader())
//                    {
//                        string currentHid = "/", currentParentHid = "";
//                        string hid, parentHid;

//                        int mainType = 0, currentItemType = 0;
//                        ulong rankId = 0;
//                        ulong rank = 0;
//                        int typeLookup = 0;
//                        Guid state = Guid.Empty;
//                        Guid user = Guid.Empty;
//                        Dictionary<int, int> itemTypes = new Dictionary<int, int>();
//                        List<Membership> rolesAtNode = new List<Membership>();
//                        Dictionary<int, int> lookUpTypes = new Dictionary<int, int>();

//                        //Initialize empty root memberships
//                        Memberships.Push(new MembershipSet("/", new List<Membership>()));

//                        while (reader.Read())
//                        {
//                            var hidValue = (SqlHierarchyId)reader.GetValue(0);
//                            hid = hidValue.ToString();
//                            parentHid = hidValue.GetAncestor(1).ToString();
//                            mainType = reader.GetInt32(1);

//                            rankId = reader.IsDBNull(2) ? 0 : (ulong)reader.GetInt64(2);
//                            rank = Convert.ToUInt64(Math.Pow(2, (double)rankId));
//                            user = reader.IsDBNull(3) ? Guid.Empty : reader.GetGuid(3);
//                            typeLookup = reader.GetInt32(4);

//                            //First row of new node - gather the node information - itemtype/state
//                            if (hid != currentHid)
//                            {
//                                #region Aggregate last node

//                                //If not initial root node
//                                if (currentHid != "/")
//                                {
//                                    //Pop until we hit the parent of current node
//                                    while (Memberships.Peek().Hid != currentParentHid) Memberships.Pop();

//                                    //Get parent permissions
//                                    List<Membership> savedMemberships = new List<Membership>(Memberships.Peek().Memberships);

//                                    //Combine current node applicable permission with parent applicable permissions for each user
//                                    savedMemberships.AddRange(rolesAtNode);

//                                    //Cache for descendants
//                                    Memberships.Push(new MembershipSet(currentHid, savedMemberships));

//                                    var taskMemberships = new List<Membership>(savedMemberships);

//                                    //if (ThreadCount < 0)
//                                    //{
//                                    //    ThreadCount++;
//                                    //    CombineParallel(currentHid, currentItemType, lookUpTypes, taskMemberships);
//                                    //}
//                                    //else
//                                    //{
//                                    Combine(currentHid, currentItemType, lookUpTypes, taskMemberships);
//                                    //  }
//                                }

//                                #endregion

//                                #region Generate details for next node

//                                rolesAtNode.Clear();
//                                itemTypes.Clear();
//                                lookUpTypes.Clear();

//                                currentHid = hid;
//                                currentParentHid = parentHid;
//                                currentItemType = mainType;

//                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));

//                                #endregion
//                            }
//                            else
//                            {
//                                if (!lookUpTypes.ContainsKey(typeLookup)) lookUpTypes.Add(typeLookup, 0);
//                                if (user != Guid.Empty) rolesAtNode.Add(new Membership(rank, user));
//                            }
//                        }
//                    }
//                }

//                #endregion

//                #region Insert data

//                InsertBulk(ResultsSync);

//                #endregion
//            };

//            #region Insert remaining

//            //    InsertBulkParallel(ResultsSync);

//            #endregion
//            //  }
//        }

//        //IEnumerable<ApplicableRole>
//        private static void Combine(string hid, int mainType, Dictionary<int, int> itemStructure, List<Membership> combinedMemberships)
//        {
//            Dictionary<Guid, ApplicableRole> applied = new Dictionary<Guid, ApplicableRole>();
//            foreach (Membership membership in combinedMemberships)
//            {
//                Dictionary<int, Dictionary<int, int>> lookup;
//                if (Roles.TryGetValue(membership.Rank, out lookup))
//                {
//                    //Check main type first
//                    Dictionary<int, int> rankTypes;
//                    if (lookup.TryGetValue(mainType, out rankTypes))
//                    {
//                        if (itemStructure.Keys.FirstOrDefault(k => rankTypes.ContainsKey(k)) != 0)
//                        {
//                            ApplicableRole applicableRole;
//                            if (applied.TryGetValue(membership.User, out applicableRole))
//                            {
//                                applicableRole.Rank |= (ulong)membership.Rank;
//                            }
//                            else
//                            {
//                                applicableRole = new ApplicableRole(hid, membership.Rank, membership.User);
//                                applied.Add(membership.User, applicableRole);
//                            }
//                        }
//                    }
//                }
//            }

//            // ResultsSync.AddRange(applied.Values);
//            AddResultRows(applied.Values);

//            //#region Parallel saving

//            //if (ResultsSync.Count > 10000)
//            //{
//            //    List<ApplicableRole> resultBatch = new List<ApplicableRole>();

//            //    resultBatch = ResultsSync;

//            //    ResultsSync = new List<ApplicableRole>();

//            //    InsertBulkParallel(resultBatch);
//            //}

//            //#endregion

//        }

//        private static void CombineParallel(string hid, int mainType, Dictionary<int, int> itemStructure, List<Membership> combinedMemberships)
//        {
//            Dictionary<Guid, ApplicableRole> applied = new Dictionary<Guid, ApplicableRole>();

//            var byUser = combinedMemberships.GroupBy(m => m.User);

//            Parallel.ForEach(byUser, a => CombineParallelByUser(hid, mainType, itemStructure, a));
//            ThreadCount--;
//        }

//        private static void CombineParallelByUser(string hid, int mainType, Dictionary<int, int> itemStructure, IGrouping<Guid, Membership> memberships)
//        {
//            if (memberships.Any())
//            {
//                ulong rank = 0;
//                foreach (var membership in memberships)
//                {
//                    Dictionary<int, Dictionary<int, int>> lookup;
//                    if (Roles.TryGetValue(membership.Rank, out lookup))
//                    {
//                        Dictionary<int, int> rankTypes;
//                        if (lookup.TryGetValue(mainType, out rankTypes))
//                        {
//                            if (itemStructure.Keys.FirstOrDefault(k => rankTypes.ContainsKey(k)) != 0)
//                            {
//                                rank |= (ulong)membership.Rank;
//                            }
//                        }
//                    }
//                }

//                if (rank != (ulong)0)
//                    ResultsParallel.Add(new ApplicableRole(hid, rank, memberships.Key));
//            }
//        }
//        private static DataTable CreateTable()
//        {
//            DataTable appliedTable = new DataTable("AppliedPermissions");

//            appliedTable.Columns.Add("userid", typeof(Guid));
//            appliedTable.Columns.Add("hid", typeof(SqlHierarchyId));
//            appliedTable.Columns.Add("rank", typeof(ulong));
//            return appliedTable;
//        }

//        private static void AddResultRows(IEnumerable<ApplicableRole> applied)
//        {
//            foreach (ApplicableRole role in applied)
//            {
//                DataRow row = TableResults.NewRow();

//                row["userid"] = role.UserId;
//                row["hid"] = SqlHierarchyId.Parse(role.Hid);
//                row["rank"] = role.Rank;

//                TableResults.Rows.Add(row);
//            }
//        }

//        private static DataTable MakeTable(List<ApplicableRole> applied)
//        // Create a new DataTable named NewProducts. 
//        {
//            DataTable appliedTable = new DataTable("AppliedPermissions");

//            appliedTable.Columns.Add("userid", typeof(Guid));
//            appliedTable.Columns.Add("hid", typeof(SqlHierarchyId));
//            appliedTable.Columns.Add("rank", typeof(int));

//            foreach (ApplicableRole role in applied)
//            {
//                DataRow row = appliedTable.NewRow();

//                row["userid"] = role.UserId;
//                row["hid"] = SqlHierarchyId.Parse(role.Hid);
//                row["rank"] = role.Rank;
//                appliedTable.Rows.Add(row);
//            }

//            appliedTable.AcceptChanges();
//            return appliedTable;
//        }

//        private static void InsertBulk(List<ApplicableRole> applied)
//        {
//            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Connection, SqlBulkCopyOptions.TableLock, null))
//            {
//                bulkCopy.DestinationTableName = "dbo.tblAppliedProjectedPermissions";
//                bulkCopy.BatchSize = 20000;
//                bulkCopy.ColumnMappings.Add("userid", "userid");
//                bulkCopy.ColumnMappings.Add("hid", "hid");
//                bulkCopy.ColumnMappings.Add("rank", "rank");
//                bulkCopy.EnableStreaming = true;

//                bulkCopy.WriteToServer(TableResults);
//                bulkCopy.Close();
//            }
//        }

//        private static void InsertBulkParallel(List<ApplicableRole> applied)
//        {
//            Parallel.Invoke(() => InsertBulk(applied));
//        }
//    }
//}
