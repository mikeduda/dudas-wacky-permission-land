﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Numerics;
using StackExchange.Redis;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PermissionAggregator
{
    /// <summary>
    /// TODO
    /// 4. groups
    /// 5. updates / data structures that are easier to update
    /// 
    ///
    /// Dependent tables
    /// tree structure/roles/rolemembers/rolepermissions/itemsubtypes/itemstate
    /// 
    /// 
    /// remember : workflowinstance.status = 0 assumed do not exist / items table state
    /// </summary>
    public static class RedisAggregator
    {
        private static List<Membership> Memberships = new List<Membership>();
        private static SqlConnection Connection;
        private static ConnectionMultiplexer RedisConnection;
        private static IDatabase Redis;

        public static Dictionary<string, Dictionary<int, ulong>> GlobalMemberships = new Dictionary<string, Dictionary<int, ulong>>();
        public static Dictionary<string, Dictionary<int, ulong>> AncestorMemberships = new Dictionary<string, Dictionary<int, ulong>>();
        public static List<Membership> HierarchicalMemberships = new List<Membership>();

        public static void Init()
        {
            RedisConnection = GetRedisConnection();
            Redis = RedisConnection.GetDatabase();
        }

        public static void FlushRedis()
        {
            RedisConnection.GetServer("localhost", 6379).FlushDatabase();
        }

        public static bool Compare(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
                if (a1[i] != a2[i])
                    return false;

            return true;
        }

        private static void Summarize(Dictionary<string, ulong> roles, List<byte[]> tree, byte[] hid, Guid state, List<int> types)
        {
            ulong nodeRoles = 0;

            //Generate correct role applicability
            //have state, list<types>
            foreach (var type in types)
            {
                var key = type.ToString() + state.ToString();
                ulong rolesForType = 0;

                if (roles.TryGetValue(key, out rolesForType))
                {
                    nodeRoles |= rolesForType;
                }
            }

            byte[] bits = BitConverter.GetBytes(nodeRoles);

            byte[] result = new byte[hid.Length + 8];

            bits.CopyTo(result, 0);
            hid.CopyTo(result, 8);

            tree.Add(result);
        }

        //        public static void LoadTree(string connString)
        //        {
        //            //Setup Redis
        //            RedisConnection.GetServer("localhost", 6379).FlushDatabase();

        //            using (Connection = new SqlConnection(connString))
        //            {
        //                Connection.Open();

        //                Dictionary<string, ulong> roles = new Dictionary<string, ulong>();

        //                #region Load roles

        //                using (SqlCommand cmd = new SqlCommand(String.Format(@"
        //                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
        //                    SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
        //                    FROM (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr
        //	                INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
        //                    ORDER BY rr.rank"), Connection))
        //                {
        //                    using (SqlDataReader reader = cmd.ExecuteReader())
        //                    {
        //                        while (reader.Read())
        //                        {
        //                            var rank = (ulong)reader.GetInt64(0);
        //                            var itemtype = reader.GetInt32(1);
        //                            var state = reader.GetGuid(2);

        //                            var key = itemtype.ToString() + state.ToString();

        //                            if (roles.ContainsKey(key))
        //                            {
        //                                roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
        //                            }
        //                            else
        //                            {
        //                                roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
        //                            }
        //                        }
        //                    }
        //                }

        //                #endregion

        //                #region Load tree

        //                byte[] currentHid = new byte[8];
        //                bool firstRun = true;
        //                Guid currentState = Guid.Empty;
        //                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
        //                    	SELECT CAST(li.hid AS VARBINARY(MAX)) AS hid, wi.state, ist.ItemTypeId
        //                        FROM tblLinkedItems4_5 AS li 
        //			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
        //			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
        //                        WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
        //                        ORDER BY li.hid"), Connection))
        //                {
        //                    using (SqlDataReader reader = cmd2.ExecuteReader())
        //                    {
        //                        List<RedisValue> tree = new List<RedisValue>();
        //                        var typeList = new List<int>();

        //                        while (reader.Read())
        //                        {
        //                            long length = reader.GetBytes(0, 0, null, 0, 0);
        //                            byte[] hid = new byte[length];
        //                            reader.GetBytes(0, 0, hid, 0, (int)length);

        //                            var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
        //                            var type = reader.GetInt32(2);

        //                            if (firstRun)
        //                            {
        //                                currentHid = hid;
        //                                firstRun = false;
        //                            }

        //                            //Summarize hid
        //                            if (!Compare(currentHid, hid))
        //                            {
        //                                //summarize currentHid
        //                                Summarize(roles, tree, currentHid, currentState, typeList);

        //                                currentHid = hid;
        //                                currentState = state;
        //                                typeList.Clear();
        //                                typeList.Add(type);
        //                            }
        //                            else
        //                            {
        //                                currentState = state;
        //                                typeList.Add(type);
        //                            }
        //                        }

        //                        Summarize(roles, tree, currentHid, currentState, typeList);

        //                        Redis.ListRightPush("Tree", tree.ToArray());

        //                        tree.Clear();
        //                    }
        //                }

        //                #endregion
        //            }
        //        }

        public static void LoadRawTree(string connString)
        {
            //Setup Redis
            using (Connection = new SqlConnection(connString))
            {
                Connection.Open();

                byte[] currentHid = new byte[8];

                Guid currentState = Guid.Empty;
                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT CAST(li.hid AS VARBINARY(MAX)) AS hid, wi.state, ist.ItemTypeId
                        FROM tblLinkedItems4_5 AS li 
			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
                        WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                        ORDER BY li.hid"), Connection))
                {
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        List<RedisValue> tree = new List<RedisValue>();
                        var typeList = new List<int>();

                        while (reader.Read())
                        {
                            long length = reader.GetBytes(0, 0, null, 0, 0);
                            byte[] hid = new byte[length];
                            reader.GetBytes(0, 0, hid, 0, (int)length);

                            var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                            var type = reader.GetInt32(2);

                            ulong nodeRoles = 0;

                            byte[] bits = BitConverter.GetBytes(nodeRoles);
                            byte[] typebits = BitConverter.GetBytes(type);
                            byte[] statebits = state.ToByteArray();

                            byte[] result = new byte[hid.Length + 28];

                            bits.CopyTo(result, 0);
                            typebits.CopyTo(result, 8);
                            statebits.CopyTo(result, 12);
                            hid.CopyTo(result, 28);

                            tree.Add(result);
                        }

                        Redis.ListRightPush("RawTree", tree.ToArray());

                        tree.Clear();
                    }
                }
            };
        }

        public static void TraverseRawTree()
        {
            int count = 0;
            foreach (var node in Redis.ListRange("RawTree"))
            {
                count++;
            }
        }

        public static void LoadMemberships(string connString)
        {
            using (Connection = new SqlConnection(connString))
            {
                Connection.Open();

                var realMemberships = new Dictionary<string, Dictionary<int, ulong>>();
                var ancestorMemberships = new Dictionary<string, Dictionary<int, ulong>>();

                using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
                    SELECT rm.hid.ToString(), u.rank AS userid, rr.rank, 1 AS isreal
                    FROM tblRoleMembership AS rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1	
                    UNION ALL
                    SELECT rm.hid.ToString(), u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
                    FROM tblRoleMembership as rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.IsReal = 0 and rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                    GROUP BY rm.hid, u.rank "), Connection))
                {
                    using (SqlDataReader reader = cmd3.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var hid = reader.GetString(0);
                            var userid = (int)reader.GetInt64(1);
                            ulong rank = (ulong)reader.GetInt64(2);
                            int isReal = reader.GetInt32(3);

                            if (isReal == 1)
                            {
                                if (realMemberships.ContainsKey(hid))
                                {
                                    if (realMemberships[hid].ContainsKey(userid))
                                    {
                                        realMemberships[hid][userid] |= rank;
                                    }
                                    else
                                    {
                                        realMemberships[hid].Add(userid, rank);
                                    }
                                }
                                else
                                {
                                    var member = new Dictionary<int, ulong>();
                                    member.Add(userid, rank);

                                    realMemberships.Add(hid, member);
                                }
                            }
                            else
                            {
                                if (ancestorMemberships.ContainsKey(hid))
                                {
                                    if (ancestorMemberships[hid].ContainsKey(userid))
                                    {
                                        ancestorMemberships[hid][userid] |= rank;
                                    }
                                    else
                                    {
                                        ancestorMemberships[hid].Add(userid, rank);
                                    }
                                }
                                else
                                {
                                    var member = new Dictionary<int, ulong>();
                                    member.Add(userid, rank);
                                    ancestorMemberships.Add(hid, member);
                                }
                            }
                        }

                        //Set as serialized dictionary
                        BinaryFormatter formatter = new BinaryFormatter();
                        var stream = new MemoryStream();

                        formatter.Serialize(stream, realMemberships);

                        Redis.StringSet("Memberships", stream.ToArray());

                        stream = new MemoryStream();
                        formatter.Serialize(stream, ancestorMemberships);

                        Redis.StringSet("AncestorMemberships", stream.ToArray());

                        realMemberships.Clear();
                        ancestorMemberships.Clear();
                    }
                }
            }
        }

        public static void Aggregate()
        {
            Stack<MembershipSet2> NodeMemberships = new Stack<MembershipSet2>();

            var mbytes = Redis.StringGet("Memberships");
            //var abytes = Redis.StringGet("AncestorMemberships");

            BinaryFormatter form = new BinaryFormatter();

            var memberships = (Dictionary<string, Dictionary<int, ulong>>)form.Deserialize(new MemoryStream(mbytes));
            //var ancestormemberships = (Dictionary<string, Dictionary<int, ulong>>)form.Deserialize(new MemoryStream(abytes));

            SqlHierarchyId hid, parentHid;

            //Initialize empty root memberships
            NodeMemberships.Push(new MembershipSet2(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

            //Traverse the tree depth first
            foreach (var node in Redis.ListRange("Tree"))
            {
                //Decode parameters
                var byt = (byte[])node;
                long len = byt.Length - 8;

                byte[] bhid = new byte[len];
                byte[] bMask = new byte[8];

                Array.Copy(byt, 8, bhid, 0, len);
                Array.Copy(byt, 0, bMask, 0, 8);

                ulong roleMask = BitConverter.ToUInt64(bMask, 0);

                using (BinaryReader reader = new BinaryReader(new MemoryStream(bhid)))
                {
                    hid = SqlHierarchyId.Null;
                    hid.Read(reader);
                }
                parentHid = hid.GetAncestor(1);

                //Pop until we hit the parent of current node
                while (NodeMemberships.Peek().Hid != parentHid) NodeMemberships.Pop();

                //Get parent permissions
                var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

                //Combine current node applicable permission with parent applicable permissions for each user
                Add(hid.ToString(), memberships, nodeMemberships);

                //Cache for descendants
                NodeMemberships.Push(new MembershipSet2(hid, nodeMemberships));

                //roleMask contains applicable roles at this node, roleMembers the user roles
                Apply(hid.ToString(), roleMask, nodeMemberships);//, ancestormemberships);
            }
        }

        public static void AggregateAllInOne(string connString)
        {
            var NodeMemberships = new Stack<MembershipSet2>();
            var realMemberships = new Dictionary<string, Dictionary<int, ulong>>();

            using (Connection = new SqlConnection(connString))
            {
                Connection.Open();

                #region Get memberships

                using (SqlCommand cmd3 = new SqlCommand(String.Format(@"
                    SELECT rm.hid.ToString(), u.rank AS userid, rr.rank, 2 AS isreal
                    FROM tblRoleMembership AS rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid = '/'
                    UNION ALL
                    SELECT rm.hid.ToString(), u.rank AS userid, rr.rank, 1 AS isreal
                    FROM tblRoleMembership AS rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A' AND rm.IsReal = 1 AND rm.hid <> '/'
                    UNION ALL
                    SELECT rm.hid.ToString(), u.rank as userid,  [dbo].[BinaryOR](POWER(CAST(2 AS BIGINT), rr.rank)) AS rank, 0 AS isreal
                    FROM tblRoleMembership as rm 
	                    INNER JOIN (SELECT UserGroupID, RANK() OVER (ORDER BY UserGroupID) AS rank FROM tblUserGroupObjects WHERE Status = 1) AS u ON rm.UserGroupID = u.UserGroupID
	                    INNER JOIN (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr ON rr.RoleId = rm.RoleId
                    WHERE rm.IsReal = 0 and rm.TreeId = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                    GROUP BY rm.hid, u.rank"), Connection))
                {
                    using (SqlDataReader reader = cmd3.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var hid = reader.GetString(0);
                            var userid = (int)reader.GetInt64(1);
                            ulong rank = (ulong)reader.GetInt64(2);
                            int isReal = reader.GetInt32(3);

                            if (isReal == 2)
                            {
                                if (GlobalMemberships.ContainsKey(hid))
                                {
                                    if (GlobalMemberships[hid].ContainsKey(userid))
                                    {
                                        GlobalMemberships[hid][userid] |= rank;
                                    }
                                    else
                                    {
                                        GlobalMemberships[hid].Add(userid, rank);
                                    }
                                }
                                else
                                {
                                    var member = new Dictionary<int, ulong>();
                                    member.Add(userid, rank);

                                    GlobalMemberships.Add(hid, member);
                                }
                            }
                            else if (isReal == 1)
                            {
                                if (realMemberships.ContainsKey(hid))
                                {
                                    if (realMemberships[hid].ContainsKey(userid))
                                    {
                                        realMemberships[hid][userid] |= rank;
                                    }
                                    else
                                    {
                                        realMemberships[hid].Add(userid, rank);
                                    }
                                }
                                else
                                {
                                    var member = new Dictionary<int, ulong>();
                                    member.Add(userid, rank);

                                    realMemberships.Add(hid, member);
                                }
                            }
                            else
                            {
                                if (AncestorMemberships.ContainsKey(hid))
                                {
                                    if (AncestorMemberships[hid].ContainsKey(userid))
                                    {
                                        AncestorMemberships[hid][userid] |= rank;
                                    }
                                    else
                                    {
                                        AncestorMemberships[hid].Add(userid, rank);
                                    }
                                }
                                else
                                {
                                    var member = new Dictionary<int, ulong>();
                                    member.Add(userid, rank);
                                    AncestorMemberships.Add(hid, member);
                                }
                            }
                        }
                    }
                }

                //Set as serialized dictionary
                //BinaryFormatter formatter = new BinaryFormatter();
                //var stream = new MemoryStream();

                //formatter.Serialize(stream, globalMemberships);

                //Redis.StringSet("GlobalMemberships", stream.ToArray());

                //stream = new MemoryStream();
                //formatter.Serialize(stream, ancestorMemberships);

                //Redis.StringSet("AncestorMemberships", stream.ToArray());

                //globalMemberships.Clear();
                //ancestorMemberships.Clear();

                #endregion

                Dictionary<string, ulong> roles = new Dictionary<string, ulong>();

                #region Load roles

                using (SqlCommand cmd = new SqlCommand(String.Format(@"
                    DECLARE @EMPTY UNIQUEIDENTIFIER = (select cast(cast(0 as binary) as UNIQUEIDENTIFIER))
                    SELECT DISTINCT rr.rank, ItemTypeId, CASE WHEN(rp.StateId IS NULL) THEN @EMPTY ELSE rp.StateId END 
                    FROM (SELECT RoleId, RANK() OVER (ORDER BY RoleId) AS rank FROM tblRoles) AS rr
	                INNER JOIN tblRolePermissions AS rp ON rr.RoleId = rp.RoleId
                    ORDER BY rr.rank"), Connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var rank = (ulong)reader.GetInt64(0);
                            var itemtype = reader.GetInt32(1);
                            var state = reader.GetGuid(2);

                            var key = itemtype.ToString() + state.ToString();

                            if (roles.ContainsKey(key))
                            {
                                roles[key] |= Convert.ToUInt64(Math.Pow(2, rank));
                            }
                            else
                            {
                                roles.Add(key, Convert.ToUInt64(Math.Pow(2, rank)));
                            }
                        }
                    }
                }

                #endregion

                #region Load tree

                byte[] currentHid = new byte[8];
                bool firstRun = true;
                Guid currentState = Guid.Empty;
                List<byte[]> tree = new List<byte[]>();

                using (SqlCommand cmd2 = new SqlCommand(String.Format(@"
                    	SELECT CAST(li.hid AS VARBINARY(MAX)) AS hid, wi.state, ist.ItemTypeId
                        FROM tblLinkedItems4_5 AS li 
			                  INNER JOIN tblItemSubTypes ist ON ist.ItemID = li.SubItemID
			                  LEFT JOIN (SELECT wi.ItemId, wi.CurrentState AS state FROM tblWorkflowInstances AS wi WHERE wi.Status = 1) AS wi ON (wi.ItemID = li.SubItemID)
                        WHERE li.TreeID = '2E3D1C02-C154-47EE-A391-2C5480C5148A'
                        ORDER BY li.hid"), Connection))
                {
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        var typeList = new List<int>();

                        while (reader.Read())
                        {
                            long length = reader.GetBytes(0, 0, null, 0, 0);
                            byte[] hid = new byte[length];
                            reader.GetBytes(0, 0, hid, 0, (int)length);

                            var state = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                            var type = reader.GetInt32(2);

                            if (firstRun)
                            {
                                currentHid = hid;
                                firstRun = false;
                            }

                            //Summarize hid
                            if (!Compare(currentHid, hid))
                            {
                                //summarize currentHid
                                Summarize(roles, tree, currentHid, currentState, typeList);

                                currentHid = hid;
                                currentState = state;
                                typeList.Clear();
                                typeList.Add(type);
                            }
                            else
                            {
                                currentState = state;
                                typeList.Add(type);
                            }
                        }

                        Summarize(roles, tree, currentHid, currentState, typeList);
                    }
                }

                #endregion

                SqlHierarchyId hid2, parentHid;

                //Initialize empty root memberships
                NodeMemberships.Push(new MembershipSet2(SqlHierarchyId.Null, new Dictionary<int, ulong>()));

                //Traverse the tree depth first - might need extra summary for lst iteration of this loop
                foreach (var node in tree)
                {
                    //Decode parameters
                    var byt = (byte[])node;
                    long len = byt.Length - 8;

                    byte[] bhid = new byte[len];
                    byte[] bMask = new byte[8];

                    Array.Copy(byt, 8, bhid, 0, len);
                    Array.Copy(byt, 0, bMask, 0, 8);

                    ulong roleMask = BitConverter.ToUInt64(bMask, 0);

                    using (BinaryReader reader = new BinaryReader(new MemoryStream(bhid)))
                    {
                        hid2 = SqlHierarchyId.Null;
                        hid2.Read(reader);
                    }
                    parentHid = hid2.GetAncestor(1);

                    //Pop until we hit the parent of current node
                    while (NodeMemberships.Peek().Hid != parentHid) NodeMemberships.Pop();

                    //Get parent permissions
                    var nodeMemberships = new Dictionary<int, ulong>(NodeMemberships.Peek().Memberships);

                    //Combine current node applicable permission with parent applicable permissions for each user
                    Add(hid2.ToString(), realMemberships, nodeMemberships);

                    //Cache for descendants
                    NodeMemberships.Push(new MembershipSet2(hid2, nodeMemberships));

                    //roleMask contains applicable roles at this node, roleMembers the user roles
                    Apply(hid2.ToString(), roleMask, nodeMemberships);//, ancestormemberships);
                }

                //Serialize hierarchicalMemberships
                //  stream = new MemoryStream();
                //  formatter.Serialize(stream, hierarchicalMemberships);

                //  Redis.StringSet("HierarchicalMemberships", stream.ToArray());
            }

            //   while (Console.Read() > 0)
            //   {
            //nothing
            //       var count = globalMemberships.Count + ancestorMemberships.Count + hierarchicalMemberships.Count;
            //     }




        }

        private static void Add(string hid, Dictionary<string, Dictionary<int, ulong>> memberships, Dictionary<int, ulong> parentMemberships)
        {
            if (memberships.ContainsKey(hid))
            {
                foreach (var member in memberships[hid])
                {
                    if (parentMemberships.ContainsKey(member.Key))
                    {
                        parentMemberships[member.Key] |= member.Value;
                    }
                    else
                    {
                        parentMemberships.Add(member.Key, member.Value);
                    }
                }
            }
        }

        private static void Apply(string hid, ulong roleMask, Dictionary<int, ulong> parentMemberships)//, Dictionary<string, Dictionary<int, ulong>> ancestormemberships)
        {
            //Apply isReal = 1 roles
            foreach (var keyValue in parentMemberships)
            {
                ulong appliedRoles = keyValue.Value & roleMask;
                if (appliedRoles > 0)
                {
                    HierarchicalMemberships.Add(new Membership(hid, appliedRoles, keyValue.Key));
                }
            }
        }

        public static int PermissionCheck()
        {
            var bglobal = Redis.StringGet("GlobalMemberships");
            var bancestor = Redis.StringGet("AncestorMemberships");
            var bhierarchical = Redis.StringGet("HierarchicalMemberships");

            BinaryFormatter form = new BinaryFormatter();

            var global = (Dictionary<string, Dictionary<int, ulong>>)form.Deserialize(new MemoryStream(bglobal));
            var ancestor = (Dictionary<string, Dictionary<int, ulong>>)form.Deserialize(new MemoryStream(bancestor));
            var hierarchical = (List<Membership>)form.Deserialize(new MemoryStream(bhierarchical));

            return global.Count + ancestor.Count + hierarchical.Count;
        }

        private static ConnectionMultiplexer GetRedisConnection()
        {
            var conn = ConnectionMultiplexer.Connect("localhost,allowAdmin=true,SyncTimeout=5000");
            return conn;
        }
    }
}
