﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PermissionAggregator;
using System.Net.Http.Formatting;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ResolverRisk.TreeServer;
using System.Data.SqlClient;

namespace TreeService
{
    [RoutePrefix("api/treeserver")]
    public class TreeController : ApiController
    {
        [Route("rebuild")]
        [HttpGet]
        public IHttpActionResult Rebuild()
        {

            return Ok("");
        }
    }
}

