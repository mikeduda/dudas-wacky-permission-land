﻿using Microsoft.Owin.Hosting;
using PermissionAggregator;
using ResolverRisk.TreeServer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace TreeService
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9001/";
            string dbName = "DUDADB5";

            if (args.Length > 0)
            {
                dbName = args[0];
            }

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine(String.Format("Using: {0}", dbName));

            using (WebApp.Start<Startup>(url: baseAddress))
            {
                JobScheduler.Init();

                Console.WriteLine("Listening on: " + baseAddress);
                string connStr = String.Format("data source=QA-DB1;initial catalog={0};integrated security=SSPI;MultipleActiveResultSets=true;", dbName);

                using (var connection = new SqlConnection(connStr))
                {
                    connection.Open();

                    while (true)
                    {
                        Console.WriteLine("Enter command (q to quit, w for All,  c for redis add, r for membership load, t for tree load, a for aggregate, s for redis count):");

                        string line = Console.ReadLine();
                        if (line == "q")
                        {
                            break;
                        }
                        else if (line == "w")
                        {
                            DateTime now = DateTime.Now;

                            RedisAggregator4.GenerateRoles(connection);
                           // RedisAggregator4.InsertBulk(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;

                            Console.WriteLine(String.Format("String size: {0}", time.ToString()));
                            Console.WriteLine();
                        }

                        else if (line == "c")
                        {
                            DateTime now = DateTime.Now;

                          //  RedisAggregator4.InsertBulk(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;

                            Console.WriteLine(String.Format("String size: {0}", time.ToString()));
                            Console.WriteLine();
                        }
                        else if (line == "s")
                        {
                            DateTime now = DateTime.Now;

                            int count = 0;// RedisAggregator4.Count();
                            //RedisAggregator4.InsertFromRedis(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;

                            Console.WriteLine(String.Format("Count: {0} in {1}", count, time.ToString()));
                            Console.WriteLine();
                        }
                        else if (line == "r")
                        {
                            DateTime now = DateTime.Now;

                            RedisAggregator4.GenerateRoles(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;

                            Console.WriteLine("Time to generate: " + time.ToString());
                            Console.WriteLine();
                        }
                        else if (line == "t")
                        {
                            DateTime now = DateTime.Now;

                            RedisAggregator4.GenerateTree(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;
                            Console.WriteLine("Time to generate: " + time.ToString());
                            Console.WriteLine();
                        }
                        else if (line == "a")
                        {
                            DateTime now = DateTime.Now;

                           // RedisAggregator4.Aggregate(connection);

                            var time = DateTime.Now.Subtract(now).TotalSeconds;
                            Console.WriteLine("Time to generate: " + time.ToString());
                            Console.WriteLine();
                        }
                    }
                }
            }
        }

        public static async void SendRequest()
        {
            DateTime now = DateTime.Now;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                for (int i = 0; i <= 1; i++)
                {
                    await client.GetAsync("api/treeserver/count").ContinueWith((r) =>
                    {
                        if (i == 1)
                        {
                            var time = DateTime.Now.Subtract(now).TotalSeconds;
                            Console.WriteLine("Time to call: " + time.ToString());
                        }
                    });
                }
            }
        }

    }
}
