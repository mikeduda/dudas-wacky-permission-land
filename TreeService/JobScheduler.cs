﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Quartz;
using Quartz.Impl;

namespace TreeService
{
    public static class JobScheduler
    {
        public static IScheduler Scheduler;
        public static IJobDetail Job;

        public static void Init()
        {
            ScheduleGenerateJob();
        }

        public static void Stop()
        {
            if (Scheduler != null)
                Scheduler.Shutdown(true);
        }

        public static void ScheduleGenerateJob()
        {
            var properties = new NameValueCollection { { "quartz.threadPool.threadCount", "20" } };
            var schedulerFactory = new StdSchedulerFactory(properties);

            // get a scheduler
            Scheduler = schedulerFactory.GetScheduler();
            Scheduler.Start();

            Job = JobBuilder.Create<QueueJob>()
                .WithIdentity("permissionGenerationSchedulerJob", "permissionGenerationSchedulerJob")
                .StoreDurably().Build();
            
            //Scheduler.AddJob(Job, true);

            // Trigger the job daily at the right time
            ITrigger trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(60)
                    .RepeatForever())
                .Build();

            Scheduler.ScheduleJob(Job, trigger);            
        }
    }
}
